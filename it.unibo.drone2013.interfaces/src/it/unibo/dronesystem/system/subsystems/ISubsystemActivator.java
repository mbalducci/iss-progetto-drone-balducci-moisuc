package it.unibo.dronesystem.system.subsystems;

/**
 * Project: dronesystem
 * <p/>
 * Represent an activator for a subsystem. It's used to start and stop the
 * underlying subsystem.
 */
public interface ISubsystemActivator {

  /**
   * Start the subsystem.
   */
  public abstract void start() throws Exception;

  /**
   * Stop the subsystem.
   */
  public abstract void stop() throws Exception;

  /**
   * @return true if the subsystem is started, otherwise false.
   */
  public abstract Boolean isStarted();
}
