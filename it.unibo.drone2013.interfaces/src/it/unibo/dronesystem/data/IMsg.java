package it.unibo.dronesystem.data;

import java.io.Serializable;

/**
 * Project: dronesystem
 * <p/>
 * A generic message.
 */
public interface IMsg extends Serializable {
  /* INF: Empty */
}
