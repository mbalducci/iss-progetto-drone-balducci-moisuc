package it.unibo.dronesystem.data.commands;

/**
 * Project: dronesystem
 * <p/>
 * Represent a message to request the drone stop.
 */
public interface IStopCmdMsg extends ICmdMsg {
  /* INF: Empty */
}
