package it.unibo.dronesystem.data.commands;

/**
 * Project: dronesystem
 * Project: dronesystem
 * <p/>
 * Represent a message to request a drone speed increment.
 */
public interface IIncSpeedCmdMsg extends IGaugeCmdMsg {
  /* INF: Empty */
}
