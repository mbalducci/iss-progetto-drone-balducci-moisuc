package it.unibo.dronesystem.data.commands;

import it.unibo.dronesystem.data.IMsg;

/**
 * Project: dronesystem
 * <p/>
 * Represent a generic command message.
 */
public interface ICmdMsg extends IMsg {
  /* INF: Empty */
}
