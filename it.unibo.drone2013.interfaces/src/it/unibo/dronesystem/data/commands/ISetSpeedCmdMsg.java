package it.unibo.dronesystem.data.commands;

/**
 * Project: dronesystem
 * <p/>
 * Represents a message to request the drone speed set.
 */
public interface ISetSpeedCmdMsg extends IGaugeCmdMsg {
  /* INF: Empty */
}
