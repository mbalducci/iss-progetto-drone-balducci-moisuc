package it.unibo.dronesystem.data.commands;

import it.unibo.dronesystem.data.gauge.IGaugeValue;

/**
 * Project: dronesystem
 * <p/>
 * Represent a generic command message related to the gauges.
 */
public interface IGaugeCmdMsg extends ICmdMsg {

  /**
   * @return The value passed in the underlying gauge command message.
   */
  public abstract IGaugeValue getValue();
}
