package it.unibo.dronesystem.data.commands;

/**
 * Project: dronesystem
 * <p/>
 * Represent a message to request a drone speed decrement.
 */
public interface IDecSpeedCmdMsg extends IGaugeCmdMsg {
  /* INF: Empty */
}
