package it.unibo.dronesystem.data.commands;

/**
 * Project: dronesystem
 * <p/>
 * Represents a message to request the drone start.
 */
public interface IStartCmdMsg extends ICmdMsg {
  /* INF: Empty */
}
