package it.unibo.dronesystem.data.dronedata.image;

import it.unibo.dronesystem.data.IMsg;

/**
 * Project: dronesystem
 * <p/>
 * Data type that represents an image.
 */
public interface IImage extends IMsg {

  /**
   * @return The raw image content.
   */
  public byte[] getBlob();

  /**
   * Check if the image is equal to the provided obj.
   * 
   * @param obj
   *          Object to be compared.
   * @return true if the underlying image is equal to obj, otherwise false.
   */
  @Override
  public abstract boolean equals(final Object obj);
}
