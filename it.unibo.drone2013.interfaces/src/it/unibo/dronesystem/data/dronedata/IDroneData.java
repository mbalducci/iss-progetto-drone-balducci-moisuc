package it.unibo.dronesystem.data.dronedata;

import it.unibo.dronesystem.data.IMsg;
import it.unibo.dronesystem.data.dronedata.droneid.IDroneId;
import it.unibo.dronesystem.data.dronedata.dronemissionid.IDroneMissionId;
import it.unibo.dronesystem.data.dronedata.image.IImage;
import it.unibo.dronesystem.data.gauge.distancetraveled.IDistanceTraveled;
import it.unibo.dronesystem.data.gauge.fuel.IFuel;
import it.unibo.dronesystem.data.gauge.geo.IGeo;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;

/**
 * Project: dronesystem
 * <p/>
 * The data sent by the drone.
 */
public interface IDroneData extends IMsg {

  /**
   * @return The fuel available in the drone when it sent the data.
   */
  public abstract IFuel getFuel();

  /**
   * @return The distance that the drone has traveled when it sent this data.
   */
  public abstract IDistanceTraveled getDistanceTraveled();

  /**
   * @return The speed that the drone had when it sent this data.
   */
  public abstract ISpeed getSpeed();

  /**
   * @return The geographic point where the drone was when it sent this data.
   */
  public abstract IGeo getGeo();

  /**
   * @return The image taken by the drone, associated with the other data
   *         of the current object.
   */
  public abstract IImage getImage();

  /**
   * @return The identifier for the drone that sent the data.
   */
  public abstract IDroneId getDroneId();

  /**
   * @return The identifier for the drone mission.
   */
  public abstract IDroneMissionId getDroneMissionId();

  /**
   * Check if the drone data is equal to the provided obj.
   * 
   * @param obj
   *          Object to be compared.
   * @return true if the underlying drone data is equal to obj,
   *         otherwise false.
   */
  @Override
  public abstract boolean equals(final Object obj);

}
