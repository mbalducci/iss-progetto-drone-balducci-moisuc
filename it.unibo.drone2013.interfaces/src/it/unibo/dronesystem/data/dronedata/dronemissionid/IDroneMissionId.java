package it.unibo.dronesystem.data.dronedata.dronemissionid;

import it.unibo.dronesystem.data.IMsg;

import java.math.BigInteger;

/**
 * Project: dronesystem
 * <p/>
 * A drone mission identifier
 */
public interface IDroneMissionId extends IMsg {

  /**
   * @return A {BigInteger} representation of the drone mission identifier.
   */
  public abstract BigInteger getIntRep();

  /**
   * @return true if the mission identifier represents any mission,
   *         otherwise false.
   */
  public abstract Boolean isAnyMission();

  /**
   * Check if the drone mission identifier is equal to the provided obj.
   * 
   * @param obj
   *          Object to be compared.
   * @return true if the underlying drone mission identifier is equal to obj,
   *         otherwise false.
   */
  @Override
  public abstract boolean equals(final Object obj);

}
