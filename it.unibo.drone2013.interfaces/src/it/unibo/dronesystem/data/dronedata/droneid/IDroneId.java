package it.unibo.dronesystem.data.dronedata.droneid;

import it.unibo.dronesystem.data.IMsg;

import java.math.BigInteger;

/**
 * Project: dronesystem
 * <p/>
 * A drone identifier
 */
public interface IDroneId extends IMsg {

  /**
   * @return A {BigInteger} representation of the drone identifier.
   */
  public abstract BigInteger getIntRep();

  /**
   * Check if the drone identifier is equal to the provided obj.
   * 
   * @param obj
   *          Object to be compared.
   * @return true if the underlying drone identifier is equal to obj,
   *         otherwise false.
   */
  @Override
  public abstract boolean equals(final Object obj);
}
