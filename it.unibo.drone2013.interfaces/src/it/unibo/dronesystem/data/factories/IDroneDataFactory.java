package it.unibo.dronesystem.data.factories;

import it.unibo.dronesystem.data.dronedata.IDroneData;
import it.unibo.dronesystem.data.dronedata.droneid.IDroneId;
import it.unibo.dronesystem.data.dronedata.dronemissionid.IDroneMissionId;
import it.unibo.dronesystem.data.dronedata.image.IImage;
import it.unibo.dronesystem.data.gauge.distancetraveled.IDistanceTraveled;
import it.unibo.dronesystem.data.gauge.fuel.IFuel;
import it.unibo.dronesystem.data.gauge.geo.IGeo;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;

import java.math.BigInteger;

/**
 * Project: dronesystem
 * <p/>
 * Factory to create data types used in a {IDroneData}.
 */
public interface IDroneDataFactory extends IGaugeValueFactory {

  /**
   * Create a new {IImage}.
   * 
   * @param rawData
   *          The image raw data (also known as blob).
   * @return The created {IImage}.
   */
  public abstract IImage createImage(final byte[] rawData);

  /**
   * Create a new {IDroneId}.
   * 
   * @param rep
   *          The {BigInteger} representation of the drone identifier.
   * @return The created {IDroneId}.
   */
  public abstract IDroneId createDroneId(final BigInteger rep);

  /**
   * Create a new {IDroneMissionId}.
   * 
   * @param rep
   *          The {BigInteger} representation of the drone mission identifier.
   * @return The created {IDroneMissionId}.
   */
  public abstract IDroneMissionId createDroneMissionId(final BigInteger rep);

  /**
   * Create a drone mission id that represents any mission.
   * 
   * @return The created {IDroneMissionId}
   */
  public abstract IDroneMissionId createDroneMissionIdForAnyMission();

  /**
   * Create a new {IDroneData} with the provided arguments.
   * 
   * @param fuel
   *          The fuel value.
   * @param distanceTraveled
   *          The distance traveled value.
   * @param speed
   *          The speed value.
   * @param geo
   *          The geo value.
   * @param image
   *          The image.
   * @param droneId
   *          The drone identifier.
   * @param droneMissionId
   *          The drone mission identifier.
   * @return The created {IDroneData}.
   */
  public abstract IDroneData createDroneData(final IFuel fuel,
      final IDistanceTraveled distanceTraveled, final ISpeed speed,
      final IGeo geo, final IImage image, final IDroneId droneId,
      final IDroneMissionId droneMissionId);
}
