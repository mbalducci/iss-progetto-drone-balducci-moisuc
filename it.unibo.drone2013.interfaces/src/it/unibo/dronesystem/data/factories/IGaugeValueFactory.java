package it.unibo.dronesystem.data.factories;

import it.unibo.dronesystem.data.gauge.distancetraveled.IDistanceTraveled;
import it.unibo.dronesystem.data.gauge.fuel.IFuel;
import it.unibo.dronesystem.data.gauge.geo.IGeo;
import it.unibo.dronesystem.data.gauge.geo.IGeoCoordinate;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Project: dronesystem
 * <p/>
 * Factory to create an object of the {IGaugeValue} hierarchy
 */
public interface IGaugeValueFactory {

  /**
   * Create a new {ISpeed}
   * 
   * @param rep
   *          The {BigDecimal} representation for the initial value that
   *          assumes the speed value
   * @return The created {ISpeed}
   */
  public abstract ISpeed createSpeed(final BigDecimal rep);

  /**
   * Create a new {IFuel}
   * 
   * @param rep
   *          The {BigDecimal} representation for the initial value that
   *          assumes the fuel value
   * @return The created {IFuel}
   */
  public abstract IFuel createFuel(final BigDecimal rep);

  /**
   * Create a new {IDistanceTraveled}
   * 
   * @param rep
   *          The {BigDecimal} representation for the initial value that
   *          assumes the distance traveled value
   * @return The created {IDistanceTraveled}
   */
  public abstract IDistanceTraveled createDistanceTraveled(final BigDecimal rep);

  /**
   * Create a new {IGeo}
   * 
   * @param latitude
   *          The initial latitude for the geo value
   * @param longitude
   *          The initial longitude for the geo value
   * @return The created {IGeo}
   */
  public abstract IGeo createGeo(final IGeoCoordinate latitude,
      final IGeoCoordinate longitude);

  /**
   * Create a new {IGeoCoordinate}
   * 
   * @param degrees
   *          The {BigDecimal} representation of the degrees
   * @param minutes
   *          The {BigDecimal} representation of the minutes
   * @param seconds
   *          The {BigDecimal} representation of the seconds
   * @return The created {IGeoCoordinate}
   */
  public abstract IGeoCoordinate createGeoCoordinate(final BigInteger degrees,
      final BigInteger minutes, final BigInteger seconds);
}
