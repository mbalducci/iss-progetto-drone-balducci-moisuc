package it.unibo.dronesystem.data.gauge.fuel;

import it.unibo.dronesystem.data.gauge.IGaugeValue;

import java.math.BigDecimal;

/**
 * Project: dronesystem
 * <p/>
 * Data type that represents a fuel value.
 */
public interface IFuel extends IGaugeValue, Comparable<IFuel> {

  /**
   * @return A {BigDecimal} representation of the fuel value.
   */
  public abstract BigDecimal getFloatRep();
}
