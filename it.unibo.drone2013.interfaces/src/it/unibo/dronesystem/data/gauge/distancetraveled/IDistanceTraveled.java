package it.unibo.dronesystem.data.gauge.distancetraveled;

import it.unibo.dronesystem.data.gauge.IGaugeValue;

import java.math.BigDecimal;

/**
 * Project: dronesystem
 * <p/>
 * Data type that represents a distance traveled value.
 */
public interface IDistanceTraveled extends IGaugeValue,
    Comparable<IDistanceTraveled> {

  /**
   * @return A {BigDecimal} representation of the distance traveled value.
   */
  public abstract BigDecimal getFloatRep();
}
