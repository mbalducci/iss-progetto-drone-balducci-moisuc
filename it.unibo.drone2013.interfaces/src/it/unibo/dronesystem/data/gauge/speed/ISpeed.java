package it.unibo.dronesystem.data.gauge.speed;

import it.unibo.dronesystem.data.gauge.IGaugeValue;

import java.math.BigDecimal;

/**
 * Project: dronesystem
 * <p/>
 * Data type that represents a speed value.
 */
public interface ISpeed extends IGaugeValue, Comparable<ISpeed> {

  /**
   * @return A {BigDecimal} representation of the speed value.
   */
  public abstract BigDecimal getFloatRep();
}
