package it.unibo.dronesystem.data.gauge.geo;

import it.unibo.dronesystem.data.IMsg;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Project: dronesystem
 * <p/>
 * Data type that represents a geographic coordinate.
 */
public interface IGeoCoordinate extends Comparable<IGeoCoordinate>, IMsg {

  /**
   * @return The {String} representation of the coordinate in the
   *         degrees-minutes-seconds notation.
   */
  public abstract String getDefRepInDMS();

  /**
   * @return The {String} representation of the coordinate in the decimal
   *         notation.
   */
  public abstract String getDefRepInDecimal();

  /**
   * @return The {BigInteger} representation for the degrees.
   */
  public abstract BigInteger getDegreesIntRep();

  /**
   * @return The {BigInteger} representation for the minutes.
   */
  public abstract BigInteger getMinutesIntRep();

  /**
   * @return The {BigInteger} representation for the seconds.
   */
  public abstract BigInteger getSecondsIntRep();

  /**
   * @return The {BigDecimal} representation for the coordinate.
   */
  public abstract BigDecimal getDecimalFloatRep();

  /**
   * Validate the coordinate with the provided minimum and maximum allowed
   * values.
   * 
   * @param minValueAllowed
   *          The minimum value for the coordinate (in decimal
   *          notation).
   * @param maxValueAllowed
   *          The minimum value for the coordinate (in decimal
   *          notation).
   * @return true if the coordinate is valid, false otherwise.
   */
  public abstract Boolean validateCoordinate(final BigDecimal minValueAllowed,
      final BigDecimal maxValueAllowed);
}
