package it.unibo.dronesystem.data.gauge.geo;

import it.unibo.dronesystem.data.gauge.IGaugeValue;

/**
 * Project: dronesystem
 * <p/>
 * Data type that represents a geographic point, which is composed by the
 * following two {IGeoCoordinate}: a latitude and a longitude.
 * <p/>
 * This definition supposed that an {IGeo} doesn't track the elevation / depth
 * (that information isn't mentioned in the functional requirements).
 */
public interface IGeo extends IGaugeValue {

  /**
   * @return The current latitude
   */
  public abstract IGeoCoordinate getLatitude();

  /**
   * @return The current longitude
   */
  public abstract IGeoCoordinate getLongitude();

  /**
   * @return true if the latitude is valid, otherwise false
   */
  public abstract Boolean isLatitudeValid();

  /**
   * @return true if the latitude is valid, otherwise false
   */
  public abstract Boolean isLongitudeValid();
}
