package it.unibo.dronesystem.display.factories;

import it.unibo.dronesystem.data.gauge.fuel.IFuel;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;
import it.unibo.dronesystem.display.views.implementors.IFuelometerViewImpl;
import it.unibo.dronesystem.display.views.implementors.ILocTrackerViewImpl;
import it.unibo.dronesystem.display.views.implementors.IOdometerViewImpl;
import it.unibo.dronesystem.display.views.implementors.ISpeedometerViewImpl;

/**
 * Project: dronesystem
 * <p/>
 * A factory to create gauge views.
 */
public interface IGaugeViewImplFactory {

  public abstract IFuelometerViewImpl createFuelometerViewImpl(
      final String name, final String unitName, final IFuel minValueShown,
      final IFuel maxValueShown);

  public abstract ISpeedometerViewImpl createSpeedometerViewImpl(
      final String name, final String unitName, final ISpeed minValueShown,
      final ISpeed maxValueShown);

  public abstract IOdometerViewImpl createOdometerViewImpl(final String name,
      final String unitName);

  public abstract ILocTrackerViewImpl createLocTrackerViewImpl(final String name);
}
