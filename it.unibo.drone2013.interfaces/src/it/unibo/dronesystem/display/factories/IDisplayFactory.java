package it.unibo.dronesystem.display.factories;

//import akka.actor.ActorRef;
import it.unibo.dronesystem.data.factories.IGaugeValueFactory;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;
import it.unibo.dronesystem.display.dashboards.IDroneCtrlDash;
import it.unibo.dronesystem.display.dashboards.ISmartDeviceDash;
import it.unibo.dronesystem.display.dashboards.implementors.IDroneCtrlDashImpl;
import it.unibo.dronesystem.display.dashboards.implementors.ISmartDeviceDashImpl;
import it.unibo.dronesystem.display.displays.ICmdDisplay;
import it.unibo.dronesystem.display.displays.IGaugeDisplay;
import it.unibo.dronesystem.display.displays.implementors.ICmdDisplayImpl;
import it.unibo.dronesystem.display.displays.implementors.IGaugeDisplayImpl;
import it.unibo.dronesystem.display.views.IFuelometerView;
import it.unibo.dronesystem.display.views.ILocTrackerView;
import it.unibo.dronesystem.display.views.IOdometerView;
import it.unibo.dronesystem.display.views.ISpeedometerView;
import it.unibo.dronesystem.display.views.implementors.IFuelometerViewImpl;
import it.unibo.dronesystem.display.views.implementors.ILocTrackerViewImpl;
import it.unibo.dronesystem.display.views.implementors.IOdometerViewImpl;
import it.unibo.dronesystem.display.views.implementors.ISpeedometerViewImpl;

/**
 * Project: dronesystem
 * <p/>
 * A factory to create display-related objects.
 */
public interface IDisplayFactory {

  public abstract IDroneCtrlDash createDroneCtrlDash(
      final IDroneCtrlDashImpl implementor, final IGaugeDisplay gaugeDisplay,
      final ICmdDisplay cmdDisplay);

  public abstract ISmartDeviceDash createSmartDeviceDash(
      final ISmartDeviceDashImpl implementor, final IGaugeDisplay gaugeDisplay);

  public abstract IGaugeDisplay createGaugeDisplay(
      final IGaugeDisplayImpl implementor);

//  public abstract ICmdDisplay createCmdDisplay(final ActorRef cmdDisplayActor,
//      final ICmdDisplayImpl implementor, final ISpeed defaultSpeedVariation,
//      final IGaugeValueFactory gaugeValueFactory);

  public abstract IFuelometerView createFuelometerView(
      final IFuelometerViewImpl implementor);

  public abstract ISpeedometerView createSpeedometerView(
      final ISpeedometerViewImpl implementor);

  public abstract IOdometerView createOdometerView(
      final IOdometerViewImpl implementor);

  public abstract ILocTrackerView createLocTrackerView(
      final ILocTrackerViewImpl implementor);
}
