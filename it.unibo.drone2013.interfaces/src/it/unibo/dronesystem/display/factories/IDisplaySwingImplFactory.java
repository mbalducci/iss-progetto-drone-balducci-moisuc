package it.unibo.dronesystem.display.factories;

import it.unibo.dronesystem.display.dashboards.implementors.swing.IDroneCtrlDashSwingImpl;
import it.unibo.dronesystem.display.dashboards.implementors.swing.ISmartDeviceDashSwingImpl;
import it.unibo.dronesystem.display.displays.implementors.swing.ICmdDisplaySwingImpl;
import it.unibo.dronesystem.display.displays.implementors.swing.IGaugeDisplaySwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.IFuelometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.ILocTrackerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.IOdometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.ISpeedometerViewSwingImpl;

/**
 * Project: dronesystem
 * <p/>
 * A factory that adds the capability to create dashboards and displays to the
 * ones provided by {IGaugeViewImplFactory}.
 */
public interface IDisplaySwingImplFactory extends IGaugeViewImplFactory {

  public abstract IDroneCtrlDashSwingImpl createDroneCtrlDashImpl(
      final String name, IGaugeDisplaySwingImpl gaugeDisplay,
      ICmdDisplaySwingImpl cmdDisplay);

  public abstract ISmartDeviceDashSwingImpl createSmartDeviceDashImpl(
      final String name, IGaugeDisplaySwingImpl gaugeDisplay);

  public abstract IGaugeDisplaySwingImpl createGaugeDisplayImpl(
      final String name, 
      final IFuelometerViewSwingImpl fuelometerView,
      final ISpeedometerViewSwingImpl speedometerView,
      final IOdometerViewSwingImpl odometerView,
      final ILocTrackerViewSwingImpl locTrackerView);

  public abstract ICmdDisplaySwingImpl createCmdDisplayImpl(final String name);

}
