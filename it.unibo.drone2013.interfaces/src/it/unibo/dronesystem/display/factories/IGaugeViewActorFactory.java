package it.unibo.dronesystem.display.factories;

//import akka.actor.ActorRef;
//import akka.actor.ActorRefFactory;
import it.unibo.dronesystem.display.views.IGaugeView;

/**
 * Project: dronesystem
 */
public interface IGaugeViewActorFactory {

  /**
   * Create a new actor for a gauge view in order to receive
   */
//  public ActorRef createGaugeViewActor(final String name,
//      final ActorRefFactory factory, final IGaugeView gaugeView);
}
