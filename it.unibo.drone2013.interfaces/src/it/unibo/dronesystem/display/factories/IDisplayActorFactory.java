package it.unibo.dronesystem.display.factories;

//import akka.actor.ActorRef;
//import akka.actor.ActorRefFactory;
import it.unibo.dronesystem.display.displays.IGaugeDisplay;

/**
 * Project: dronesystem
 * <p/>
 * A factory to create display related actors.
 */
public interface IDisplayActorFactory {

  /**
   * Create a new {ICmdDisplayActor}
   * 
   * @return The created actor.
   */
//  public abstract ActorRef createCmdDisplayActor(final String name,
//      final ActorRefFactory factory, final ActorRef cmdServerActor);

  /**
   * Create a new {IGaugeDisplayActor}
   * 
   * @return The created actor.
   */
//  public abstract ActorRef createGaugeDisplayActor(final String name,
//      final ActorRefFactory factory, final ActorRef dataManagerActor,
//      final IGaugeDisplay gaugeDisplay);
}
