package it.unibo.dronesystem.display.views;

/**
 * Project: dronesystem
 * <p/>
 * Represent a view for a speedometer
 */
public interface ISpeedometerView extends IGaugeView {
  /* INF: Empty */
}
