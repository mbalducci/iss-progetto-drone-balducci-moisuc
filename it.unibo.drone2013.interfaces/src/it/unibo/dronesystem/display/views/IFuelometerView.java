package it.unibo.dronesystem.display.views;

/**
 * Project: dronesystem
 * <p/>
 * Represent a view for a fuelometer
 */
public interface IFuelometerView extends IGaugeView {
  /* INF: Empty */
}
