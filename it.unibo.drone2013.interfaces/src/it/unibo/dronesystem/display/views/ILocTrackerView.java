package it.unibo.dronesystem.display.views;

/**
 * Project: dronesystem
 * <p/>
 * Represent a view for a location tracker
 */
public interface ILocTrackerView extends IGaugeView {
  /* INF: Empty */
}
