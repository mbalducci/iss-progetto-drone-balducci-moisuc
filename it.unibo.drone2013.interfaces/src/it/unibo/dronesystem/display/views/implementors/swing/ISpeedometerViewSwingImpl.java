package it.unibo.dronesystem.display.views.implementors.swing;

import it.unibo.dronesystem.display.views.implementors.ISpeedometerViewImpl;

/**
 * Project: dronesystem
 * <p/>
 * A contract for a speedometer implementor that uses the Swing technology
 */
public interface ISpeedometerViewSwingImpl extends ISpeedometerViewImpl,
    IGaugeViewSwingImpl {
  /* INF: Empty */
}
