package it.unibo.dronesystem.display.views.implementors;

/**
 * Project: dronesystem
 * <p/>
 * Implementor for a {IGaugeView}
 */
public interface IGaugeViewImpl {

  /**
   * The technology-dependent update operation.
   * 
   * @param value
   *          The value for the view update.
   */
  public abstract void update(String value);
}
