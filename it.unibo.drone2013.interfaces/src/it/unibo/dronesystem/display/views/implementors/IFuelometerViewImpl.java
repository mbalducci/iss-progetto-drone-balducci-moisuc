package it.unibo.dronesystem.display.views.implementors;

/**
 * Project: dronesystem
 * <p/>
 * Implementor for a {IFuelometerView}
 */
public interface IFuelometerViewImpl extends IGaugeViewImpl {
  /* INF: Empty */
}
