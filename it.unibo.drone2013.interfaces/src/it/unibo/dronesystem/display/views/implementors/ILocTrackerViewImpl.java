package it.unibo.dronesystem.display.views.implementors;

/**
 * Project: dronesystem
 * <p/>
 * Implementor for a {ILocTrackerView}
 */
public interface ILocTrackerViewImpl extends IGaugeViewImpl {
  /* INF: Empty */
}
