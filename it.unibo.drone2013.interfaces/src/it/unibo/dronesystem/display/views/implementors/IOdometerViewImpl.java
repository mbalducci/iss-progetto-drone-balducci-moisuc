package it.unibo.dronesystem.display.views.implementors;

/**
 * Project: dronesystem
 * <p/>
 * Implementor for a {IOdometerView}
 */
public interface IOdometerViewImpl extends IGaugeViewImpl {
  /* INF: Empty */
}
