package it.unibo.dronesystem.display.views.implementors.swing;

import it.unibo.dronesystem.display.views.implementors.ILocTrackerViewImpl;

/**
 * Project: dronesystem
 * <p/>
 * A contract for a loctracker implementor that uses the Swing technology
 */
public interface ILocTrackerViewSwingImpl extends ILocTrackerViewImpl,
    IGaugeViewSwingImpl {
  /* INF: Empty */
}
