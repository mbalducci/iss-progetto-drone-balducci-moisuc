package it.unibo.dronesystem.display.views.implementors.swing;

import it.unibo.dronesystem.display.views.implementors.IOdometerViewImpl;

/**
 * Project: dronesystem
 * <p/>
 * A contract for a odometer implementor that uses the Swing technology
 */
public interface IOdometerViewSwingImpl extends IOdometerViewImpl,
    IGaugeViewSwingImpl {
  /* INF: Empty */
}
