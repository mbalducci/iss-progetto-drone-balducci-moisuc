package it.unibo.dronesystem.display.views.implementors;

/**
 * Project: dronesystem
 * <p/>
 * Implementor for a {ISpeedometerView}
 */
public interface ISpeedometerViewImpl extends IGaugeViewImpl {
  /* INF: Empty */
}
