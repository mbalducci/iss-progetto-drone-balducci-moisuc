package it.unibo.dronesystem.display.views.implementors.swing;

import it.unibo.dronesystem.display.views.implementors.IFuelometerViewImpl;

/**
 * Project: dronesystem
 * <p/>
 * A contract for a fuelometer implementor that uses the Swing technology
 */
public interface IFuelometerViewSwingImpl extends IFuelometerViewImpl,
    IGaugeViewSwingImpl {
  /* INF: Empty */
}
