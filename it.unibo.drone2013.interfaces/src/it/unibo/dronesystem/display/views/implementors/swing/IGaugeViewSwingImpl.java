package it.unibo.dronesystem.display.views.implementors.swing;

import it.unibo.dronesystem.display.views.implementors.IGaugeViewImpl;

import javax.swing.JPanel;

/**
 * Project: dronesystem
 * <p/>
 * A {IGaugeViewImpl} using the Swing technology
 */
public interface IGaugeViewSwingImpl extends IGaugeViewImpl {

  /**
   * @return The main panel for the view.
   */
  public abstract JPanel getMainPanel();

  /**
   * @return The view favor.
   */
  public abstract ViewSwingFavor getFavor();
}
