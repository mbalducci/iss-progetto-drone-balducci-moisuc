package it.unibo.dronesystem.display.views;

/**
 * Project: dronesystem
 * <p/>
 * Represent a view for an odometer
 */
public interface IOdometerView extends IGaugeView {
  /* INF: Empty */
}
