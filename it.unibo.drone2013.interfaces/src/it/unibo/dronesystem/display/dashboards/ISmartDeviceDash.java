package it.unibo.dronesystem.display.dashboards;

/**
 * Project: dronesystem
 * <p/>
 * A dashboard to control the drone and to visualize its data.
 */
public interface ISmartDeviceDash {

  /**
   * Start the dashboard (abstraction-side).
   */
  public abstract void start();

  /**
   * Stop the dashboard (abstraction-side).
   */
  public abstract void stop();
}
