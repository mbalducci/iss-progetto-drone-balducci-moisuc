package it.unibo.dronesystem.display.dashboards;

/**
 * Project: dronesystem
 * <p/>
 * A dashboard to visualize the drone data.
 */
public interface IDroneCtrlDash {

  /**
   * Start the dashboard (abstraction-side).
   */
  public abstract void start();

  /**
   * Stop the dashboard (abstraction-side).
   */
  public abstract void stop();
}
