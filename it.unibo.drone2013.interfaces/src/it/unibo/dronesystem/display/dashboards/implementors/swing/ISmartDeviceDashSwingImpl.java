package it.unibo.dronesystem.display.dashboards.implementors.swing;

import it.unibo.dronesystem.display.dashboards.implementors.ISmartDeviceDashImpl;

/**
 * Project: dronesystem
 */
public interface ISmartDeviceDashSwingImpl extends ISmartDeviceDashImpl {
  /* INF: Empty */
}
