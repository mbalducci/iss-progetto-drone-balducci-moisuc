package it.unibo.dronesystem.display.dashboards.implementors;

/**
 * Project: dronesystem
 * <p/>
 * A dashboard implementor to visualize the drone data.
 */
public interface ISmartDeviceDashImpl {

  /**
   * Start the dashboard (implementor-side).
   */
  public abstract void start();

  /**
   * Stop the dashboard (implementor-side).
   */
  public abstract void stop();
}
