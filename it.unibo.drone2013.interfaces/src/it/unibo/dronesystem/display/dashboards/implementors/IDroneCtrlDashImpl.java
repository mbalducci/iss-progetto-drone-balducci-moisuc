package it.unibo.dronesystem.display.dashboards.implementors;

/**
 * Project: dronesystem
 * <p/>
 * A dashboard implementor to control the drone and to visualize its data.
 */
public interface IDroneCtrlDashImpl {

  /**
   * Start the dashboard (implementor-side).
   */
  public abstract void start();

  /**
   * Stop the dashboard (implementor-side).
   */
  public abstract void stop();
}
