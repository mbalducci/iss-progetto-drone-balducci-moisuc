package it.unibo.dronesystem.display.dashboards.implementors.swing;

/**
 * Project: dronesystem
 * <p/>
 * A swing dashboard: a named frame with components.
 */
public interface IDashSwing {
  /* INF: Empty */
}
