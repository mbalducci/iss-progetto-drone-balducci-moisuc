package it.unibo.dronesystem.display.dashboards.implementors.swing;

import it.unibo.dronesystem.display.dashboards.implementors.IDroneCtrlDashImpl;

/**
 * Project: dronesystem
 */
public interface IDroneCtrlDashSwingImpl extends IDroneCtrlDashImpl {
  /* INF: Empty */
}
