package it.unibo.dronesystem.display.displays;

/**
 * Project: dronesystem
 */
public class InvalidInputError extends Exception {

  private final String component;
  private final String value;

  public InvalidInputError(final String component, final String value) {
    this.component = component;
    this.value = value;
  }

  @Override
  public String getMessage() {
    return String.format("The component %s received the invalid value: %s",
        this.component, this.value);
  }
}
