package it.unibo.dronesystem.display.displays;

import it.unibo.dronesystem.display.displays.implementors.ICmdDisplayImplListener;

/**
 * Project: dronesystem
 * <p/>
 * A display where a user can issue commands.
 */
public interface ICmdDisplay extends ICmdDisplayImplListener {
  /* INF: Empty */
}
