package it.unibo.dronesystem.display.displays;

/**
 * Project: dronesystem
 * <p/>
 * A display that shows the values for the associated gauge displays:
 * <ul>
 * <li>View for IFuelometer</li>
 * <li>View for IOdometer</li>
 * <li>View for ISpeedometer</li>
 * <li>View for ILocTracker</li>
 * </ul>
 */
public interface IGaugeDisplay {

  /**
   * Set the display status to the provided one.
   * 
   * @param status
   *          The display status.
   */
  public abstract void setStatus(final GaugeDisplayStatus status);
}
