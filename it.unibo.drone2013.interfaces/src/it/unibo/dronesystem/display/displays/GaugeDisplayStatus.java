package it.unibo.dronesystem.display.displays;

/**
 * Project: dronesystem
 * <p/>
 * Represent a status for a gauge display.
 */
public enum GaugeDisplayStatus {
  CONNECTED_TO_THE_SYSTEM, DISCONNECTED_FROM_THE_SYSTEM
}
