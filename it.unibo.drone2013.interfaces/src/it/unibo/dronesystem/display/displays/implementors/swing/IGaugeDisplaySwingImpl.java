package it.unibo.dronesystem.display.displays.implementors.swing;

import it.unibo.dronesystem.display.displays.implementors.IGaugeDisplayImpl;

import javax.swing.JPanel;

/**
 * Project: dronesystem
 * Package: it.unibo.dronesystem.display.implementors.swing
 * <p/>
 * A gauge display implementor using the Swing technology.
 */
public interface IGaugeDisplaySwingImpl extends IGaugeDisplayImpl {

  /**
   * @return The main panel for the underlying gauge display.
   */
  public abstract JPanel getMainPanel();
}
