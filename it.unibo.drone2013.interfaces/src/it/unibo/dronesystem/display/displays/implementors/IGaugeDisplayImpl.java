package it.unibo.dronesystem.display.displays.implementors;

import it.unibo.dronesystem.display.displays.GaugeDisplayStatus;

/**
 * Project: dronesystem
 * <p/>
 * An implementor for a {IGaugeDisplay}
 */
public interface IGaugeDisplayImpl {

  /**
   * Implementor-side for IGaugeDisplay#setStatus(GaugeDisplayStatus)
   * 
   * @param status
   *          The display status.
   */
  public abstract void setStatus(final GaugeDisplayStatus status);
}
