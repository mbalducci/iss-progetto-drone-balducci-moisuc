package it.unibo.dronesystem.display.displays.implementors.swing;

import it.unibo.dronesystem.display.displays.implementors.ICmdDisplayImpl;

import javax.swing.JPanel;

/**
 * Project: dronesystem
 * <p/>
 * A command display implementor using the Swing technology.
 */
public interface ICmdDisplaySwingImpl extends ICmdDisplayImpl {

  /**
   * @return The main panel for the underlying command display.
   */
  public abstract JPanel getMainPanel();
}
