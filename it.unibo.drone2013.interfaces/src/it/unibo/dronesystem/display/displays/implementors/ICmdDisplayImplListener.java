package it.unibo.dronesystem.display.displays.implementors;

import it.unibo.dronesystem.display.displays.InvalidInputError;

/**
 * Project: dronesystem
 * <p/>
 * A {ICmdDisplayImpl} listener.
 */
public interface ICmdDisplayImplListener {

  /**
   * Callback for the event: <i>a speed value has been decreased</i>.
   */
  public abstract void onIncSpeed();

  /**
   * Callback for the event: <i>a speed value has been increased</i>.
   */
  public abstract void onDecSpeed();

  /**
   * Callback for the event: <i>a speed value has been set</i>.
   * 
   * @param speedValueDefRep
   *          The speed value representation for the speed that
   *          has been set.
   */
  public abstract void onSetSpeed(final String speedValueDefRep)
      throws InvalidInputError;

  /**
   * Callback for the event: <i>start requested</i>
   */
  public abstract void onStart();

  /**
   * Callback for the event: <i>stop requested</i>
   */
  public abstract void onStop();
}
