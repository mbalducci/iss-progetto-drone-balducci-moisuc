package it.unibo.dronesystem.display.displays.implementors;

/**
 * Project: dronesystem
 * <p/>
 * An implementor for a {ICmdDisplay}
 */
public interface ICmdDisplayImpl {

  /**
   * Register the listener for the events triggered by the underlying
   * {ICmdDisplayImpl}.
   * 
   * @param listener
   *          The listener to be registered.
   */
  public abstract void register(final ICmdDisplayImplListener listener);
}
