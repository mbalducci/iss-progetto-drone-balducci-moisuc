package it.unibo.dronesystem.gauge.factories;

import it.unibo.dronesystem.gauge.gauges.IFuelometer;
import it.unibo.dronesystem.gauge.gauges.ILocTracker;
import it.unibo.dronesystem.gauge.gauges.IOdometer;
import it.unibo.dronesystem.gauge.gauges.ISpeedometer;

/**
 * Project: dronesystem
 * <p/>
 * Factory to create an object of the {IGauge} hierarchy
 */
public interface IGaugeFactory {

  /**
   * Create a new {IFuelometer}
   * 
   * @return The created {IFuelometer}
   */
  public abstract IFuelometer createFuelometer();

  /**
   * Create a new {ILocTracker}
   * 
   * @return The created {ILocTracker}
   */
  public abstract ILocTracker createLocTracker();

  /**
   * Create a new {IOdometer}
   * 
   * @return The created {IOdometer}
   */
  public abstract IOdometer createOdometer();

  /**
   * Create a new {ISpeedometer}
   * 
   * @return The created {ISpeedometer}
   */
  public abstract ISpeedometer createSpeedometer();
}
