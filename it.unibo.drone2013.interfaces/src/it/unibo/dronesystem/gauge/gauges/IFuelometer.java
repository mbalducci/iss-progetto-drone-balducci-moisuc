package it.unibo.dronesystem.gauge.gauges;

import it.unibo.dronesystem.data.gauge.fuel.IFuel;

/**
 * Project: dronesystem
 * <p/>
 * A fuelometer.
 */
public interface IFuelometer extends IGauge {

  /**
   * @return The minimum fuel value that the underlying fuelometer can assume.
   */
  public abstract IFuel getFuelMin();
}
