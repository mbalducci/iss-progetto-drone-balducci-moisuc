package it.unibo.dronesystem.gauge.gauges;

import it.unibo.dronesystem.data.gauge.speed.ISpeed;

/**
 * Project: dronesystem
 * <p/>
 * A speedometer.
 */
public interface ISpeedometer extends IGauge {

  /**
   * @return The minimum speed value that the underlying speedometer can assume.
   */
  public abstract ISpeed getSpeedMin();

  /**
   * @return The maximum speed value that the underlying speedometer can assume.
   */
  public abstract ISpeed getSpeedMax();
}
