package it.unibo.dronesystem.gauge.gauges;

/**
 * Project: dronesystem
 * <p/>
 * A odometer.
 */
public interface IOdometer extends IGauge {
  /* INF: Empty */
}
