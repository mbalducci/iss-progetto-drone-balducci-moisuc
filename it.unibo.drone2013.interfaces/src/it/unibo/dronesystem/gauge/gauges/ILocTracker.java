package it.unibo.dronesystem.gauge.gauges;

/**
 * Project: dronesystem
 * <p/>
 * A location tracker.
 */
public interface ILocTracker extends IGauge {
  /* INF: Empty */
}
