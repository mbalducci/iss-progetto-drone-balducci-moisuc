package it.unibo.iss.gruppo9.SmartDevice.DispatchWithRegister;

import it.unibo.contact.droneSystem.SmartDevice;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister.ApplParams;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.format.Formatter;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	TextView myView;
	IOutputView myAndroidView;
	SmartDeviceDashboard gaugeDisplay;
	private SmartDevice smartDevice;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dronelayout);

		// Customize SpeedometerView
		SpeedometerView speedometer = (SpeedometerView) findViewById(R.id.speedometer);

		// Add label converter
		speedometer.setLabelConverter(new SpeedometerView.LabelConverter() {
			@Override
			public String getLabelFor(double progress, double maxProgress) {
				return String.valueOf((int) Math.round(progress));
			}
		});

		// configure value range and ticks
		speedometer.setMaxSpeed(120);
		speedometer.setMajorTickStep(20);
		speedometer.setMinorTicks(9);

		// Configure value range colors
		speedometer.addColoredRange(82, 93, Color.GREEN);
		speedometer.addColoredRange(93, 110, Color.YELLOW);
		speedometer.addColoredRange(110, 120, Color.RED);

		ProgressBar fuelmeter = (ProgressBar) findViewById(R.id.fuelmeter);
		fuelmeter.setMax(100);

		myView = (TextView) findViewById(R.id.output);
		gaugeDisplay = new SmartDeviceDashboard(speedometer,fuelmeter,(TextView)findViewById(R.id.latitudeLabel),
				(TextView)findViewById(R.id.longitudeLabel),(TextView)findViewById(R.id.distanceLabel));
		myAndroidView = new AndroidOutputView(myView, gaugeDisplay, this);

		WifiManager wifiMgr = (WifiManager) getSystemService(WIFI_SERVICE);
		WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
		int ip = wifiInfo.getIpAddress();
		@SuppressWarnings("deprecation")
		String ipAddress = Formatter.formatIpAddress(ip);
		
		try {
			smartDevice = SmartDeviceSingleton.getInstance("tempName", myAndroidView, ipAddress, "8080");
		} catch (Exception e) {
			e.printStackTrace();
		}
		smartDevice.start();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
 
		// Clear all notification
		NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		nMgr.cancel(ApplParams.missionNotificationId);
	} 
		
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
