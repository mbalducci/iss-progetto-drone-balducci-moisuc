package it.unibo.iss.gruppo9.SmartDevice.DispatchWithRegister;

import it.unibo.contact.droneSystem.SmartDevice;
import it.unibo.is.interfaces.IOutputView;

public class SmartDeviceSingleton {
	private static SmartDevice instance;
	
	public static SmartDevice getInstance(String name, IOutputView view, String ip, String portNo) throws Exception{
		if(instance==null){
			instance = new SmartDevice(name,view,ip,portNo);
		}
		return instance;
	}
	

	
	private SmartDeviceSingleton(){}
}
