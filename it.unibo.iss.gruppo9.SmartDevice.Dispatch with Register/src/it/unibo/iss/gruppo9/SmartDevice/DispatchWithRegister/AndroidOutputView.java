package it.unibo.iss.gruppo9.SmartDevice.DispatchWithRegister;

import it.unibo.is.interfaces.IOutputView;
import android.widget.TextView;
import it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister.ApplParams;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;


public class AndroidOutputView implements IOutputView {

	private TextView myView;
	private SmartDeviceDashboard gaugeDisplay;
	private MainActivity parentActivity;

	public AndroidOutputView(TextView myView, SmartDeviceDashboard gaugeDisplay, MainActivity parentActivity) {
		super();
		this.myView = myView;
		this.gaugeDisplay = gaugeDisplay;
		this.parentActivity = parentActivity;
	}

	@Override
	public String getCurVal() {
		return (String) myView.getText();
	}

	@Override
	public void addOutput(final String msg) {
		myView.post(new Runnable() {
			@Override
			public void run() {
				myView.append(msg);
			}
		});
	}

	@Override
	public void setOutput(final String data) {

		myView.post(new Runnable() {
			@Override
			public void run() {

				if (data.equals(ApplParams.START)) {
					myView.post(new Runnable() {
						@Override
						public void run() {
							showNotification();
							myView.setText("New mission started");
						}
					});
				} else {// the only other messages are dataSD, so unwrap them
					String[] sensors = data.substring(6, data.length() - 1)
							.split(",");

					double newSpeed = 0;
					try {
						newSpeed = Double.parseDouble(sensors[0]);
						gaugeDisplay.setSpeed(newSpeed);
					} catch (Exception e) {
						e.printStackTrace();
					}

					int fuelInt = 100;
					try {
						fuelInt = Math.round(Float.parseFloat(sensors[1]));
						gaugeDisplay.setFuel(fuelInt);
					} catch (Exception e) {
						e.printStackTrace();
					}
					gaugeDisplay.setLatitudeLabel(sensors[2]);
					gaugeDisplay.setLongitudeLabel(sensors[3]);
					gaugeDisplay.setDistanceLabel(sensors[5]);
					// gaugeDisplay.setAltitudeLabel(sensors[4]);
				}

			}
		});

	}
	
	private void showNotification(){
		NotificationCompat.Builder mBuilder =	
	        new NotificationCompat.Builder(parentActivity)
	        .setSmallIcon(R.drawable.ic_launcher)
	        .setContentTitle("Drone Mission")
	        .setContentText("New mission started! Tap to follow drone.")
	        .setAutoCancel(true);
		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(parentActivity, MainActivity.class);
	
		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(parentActivity);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(MainActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(
		            0,
		            PendingIntent.FLAG_UPDATE_CURRENT
		        );
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager =
		    (NotificationManager) parentActivity.getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		mNotificationManager.notify(ApplParams.missionNotificationId, mBuilder.build());
	}
}
