package it.unibo.dronesystem.config;

public class DroneConfig {

	public static final int speedometerViewWidth = 200;		//280
	public static final int speedometerViewHeight = 200;	//280

	public static final int odometerViewWidth = 500;	//620
	public static final int odometerViewHeight = 90;	//120

	public static final int fuleometerViewWidth = 500;	//620
	public static final int fuelometerViewHeight = 90;	//120

	public static final double minSpeed = 2.0;	 
	public static final double maxSpeed = 120.0;	 

	public static final int minFuel = 30;	 
	public static final int maxFuel = 220;	 

	//Used by DroneCtrl0DashSwingImpl
	public static final int windowWidth = 800;	 
	public static final int windowHeight = 600;	 
	
}
