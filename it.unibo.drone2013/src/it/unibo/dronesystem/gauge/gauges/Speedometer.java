package it.unibo.dronesystem.gauge.gauges;

import it.unibo.dronesystem.data.factories.GaugeValueFactory;
import it.unibo.dronesystem.data.gauge.IGaugeValue;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;
import it.unibo.dronesystem.data.gauge.speed.Speed;

import java.math.BigDecimal;

/**
 * Project: dronesystem
 * <p/>
 * A {ISpeedometer} implementation.
 */
public class Speedometer extends Gauge implements ISpeedometer {

  public static ISpeed DEF_SPEED_MIN = new GaugeValueFactory()
                                         .createSpeed(BigDecimal.valueOf(24.6));
  public static ISpeed DEF_SPEED_MAX = new GaugeValueFactory()
                                         .createSpeed(BigDecimal.valueOf(246.8));

  private final ISpeed speedMin;
  private final ISpeed speedMax;

  /* { Constructors and factories */

  protected Speedometer(ISpeed speedMin, ISpeed speedMax) {
    super();

    if (speedMin == null) {
      this.speedMin = DEF_SPEED_MIN;
    } else {
      if (speedMin.compareTo(Speed.create(BigDecimal.ZERO)) < 0) {
        throw new IllegalArgumentException(
            "Invalid speedMin (it's lesser than zero)");
      }
      this.speedMin = speedMin;
    }
    if (speedMax == null) {
      this.speedMax = DEF_SPEED_MAX;
    } else {
      if (speedMax.compareTo(speedMin) < 0) {
        throw new IllegalArgumentException(
            "Invalid speedMax (it's lesser than speedMin)");
      }
      this.speedMax = speedMax;
    }
  }

  public static Speedometer create(ISpeed speedMin, ISpeed speedMax) {
    return new Speedometer(speedMin, speedMax);
  }

  public static Speedometer createWithDefSpeeds() {
    return new Speedometer(null, null);
  }

  public static Speedometer createWithDefSpeedMax(ISpeed speedMin) {
    return new Speedometer(speedMin, null);
  }

  public static Speedometer createWithDefSpeedMin(ISpeed speedMax) {
    return new Speedometer(null, speedMax);
  }

  /* } */

  /* { {ISpeedometer} implementation */

  /**
   * @see ISpeedometer#getSpeedMin()
   */
  @Override
  public ISpeed getSpeedMin() {
    return this.speedMin;
  }

  /**
   * @see ISpeedometer#getSpeedMin()
   */
  @Override
  public ISpeed getSpeedMax() {
    return this.speedMax;
  }

  /* } */

  /* { {Gauge} implementation */

  /**
   * @see Gauge#validateGaugeValue(IGaugeValue)
   */
  @Override
  protected Boolean validateGaugeValue(IGaugeValue gaugeValue) {
    return (gaugeValue instanceof ISpeed)
        && (((ISpeed) gaugeValue).compareTo(this.speedMin) >= 0)
        && (((ISpeed) gaugeValue).compareTo(this.speedMax) <= 0);
  }

  /* } */

}
