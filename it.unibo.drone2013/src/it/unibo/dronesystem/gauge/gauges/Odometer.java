package it.unibo.dronesystem.gauge.gauges;

import it.unibo.dronesystem.data.gauge.IGaugeValue;
import it.unibo.dronesystem.data.gauge.distancetraveled.IDistanceTraveled;

/**
 * Project: dronesystem
 * <p/>
 * A {IOdometer} implementation.
 */
public class Odometer extends Gauge implements IOdometer {

  /* { Constructors and factories */

  protected Odometer() {
    super();
  }

  public static Odometer create() {
    return new Odometer();
  }

  /* } */

  /* { {Gauge} implementation */

  /**
   * @see Gauge#validateGaugeValue(IGaugeValue)
   */
  @Override
  protected Boolean validateGaugeValue(final IGaugeValue gaugeValue) {
    return (gaugeValue instanceof IDistanceTraveled);
  }

  /* } */

}
