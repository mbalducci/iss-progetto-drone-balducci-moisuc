package it.unibo.dronesystem.gauge.gauges;

import it.unibo.dronesystem.data.gauge.IGaugeValue;
import it.unibo.dronesystem.data.gauge.geo.IGeo;

/**
 * Project: dronesystem
 * <p/>
 * A {ILocTracker} implementation.
 */
public class LocTracker extends Gauge implements ILocTracker {

  /* { Constructors and factories */

  protected LocTracker() {
    super();
  }

  public static LocTracker create() {
    return new LocTracker();
  }

  /* } */

  /* { {Gauge} implementation */

  /**
   * @see Gauge#validateGaugeValue(IGaugeValue)
   */
  @Override
  protected Boolean validateGaugeValue(final IGaugeValue gaugeValue) {
    return (gaugeValue instanceof IGeo);
  }

  /* } */

}
