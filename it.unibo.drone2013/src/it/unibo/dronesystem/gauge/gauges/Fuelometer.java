package it.unibo.dronesystem.gauge.gauges;

import it.unibo.dronesystem.data.factories.GaugeValueFactory;
import it.unibo.dronesystem.data.gauge.IGaugeValue;
import it.unibo.dronesystem.data.gauge.fuel.IFuel;

import java.math.BigDecimal;

/**
 * Project: dronesystem
 * <p/>
 * A {IFuelometer} implementation.
 */
public class Fuelometer extends Gauge implements IFuelometer {

  public static IFuel DEF_FUEL_MIN = new GaugeValueFactory()
                                       .createFuel(BigDecimal.valueOf(10));

  private final IFuel fuelMin;

  /* { Constructors and factories */

  protected Fuelometer(IFuel fuelMin) {
    super();

    if (fuelMin == null) {
      this.fuelMin = DEF_FUEL_MIN;
    } else {
      this.fuelMin = fuelMin;
    }
  }

  public static Fuelometer create(IFuel fuelMin) {
    return new Fuelometer(fuelMin);
  }

  public static Fuelometer createWithDefFuelMin() {
    return new Fuelometer(null);
  }

  /* { {IFuelometer} implementation */

  /**
   * @see IFuelometer#getFuelMin()
   */
  @Override
  public IFuel getFuelMin() {
    return this.fuelMin;
  }

  /* } */

  /* { {Gauge} implementation */

  /**
   * @see Gauge#validateGaugeValue(IGaugeValue)
   */
  @Override
  protected Boolean validateGaugeValue(IGaugeValue gaugeValue) {
    return (gaugeValue instanceof IFuel)
        && (((IFuel) gaugeValue).compareTo(this.fuelMin) >= 0);
  }

  /* } */

}
