package it.unibo.dronesystem.gauge.factories;

import it.unibo.dronesystem.data.gauge.fuel.IFuel;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;
import it.unibo.dronesystem.gauge.gauges.*;

/**
 * Project: dronesystem
 * <p/>
 * A {IGaugeFactory} implementation.
 */
public class GaugeFactory implements IGaugeFactory {

  private final IFuel  fuelMin;
  private final ISpeed speedMin;
  private final ISpeed speedMax;

  public GaugeFactory(final IFuel fuelMin, final ISpeed speedMin,
      final ISpeed speedMax) {
    this.fuelMin = fuelMin;
    this.speedMin = speedMin;
    this.speedMax = speedMax;
  }

  /* { {IGaugeFactory} implementation */

  /**
   * @see IGaugeFactory#createFuelometer()
   */
  @Override
  public IFuelometer createFuelometer() {
    return Fuelometer.create(this.fuelMin);
  }

  /**
   * @see IGaugeFactory#createLocTracker()
   */
  @Override
  public ILocTracker createLocTracker() {
    return LocTracker.create();
  }

  /**
   * @see IGaugeFactory#createOdometer()
   */
  @Override
  public IOdometer createOdometer() {
    return Odometer.create();
  }

  /**
   * @see IGaugeFactory#createSpeedometer()
   */
  @Override
  public ISpeedometer createSpeedometer() {
    return Speedometer.create(this.speedMin, this.speedMax);
  }

  /* } */

}
