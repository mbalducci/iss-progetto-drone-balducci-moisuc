package it.unibo.dronesystem.data.gauge.distancetraveled;

import java.math.BigDecimal;

/**
 * Project: dronesystem
 * <p/>
 * A {IDistanceTraveled} implementation with high precision.
 */
public class DistanceTraveled implements IDistanceTraveled {

  private final BigDecimal rep;

  /* { Constructors and factories */

  protected DistanceTraveled(final BigDecimal rep) {
    /* { Preconditions */
    if (rep.compareTo(BigDecimal.ZERO) < 0) {
      throw new IllegalArgumentException(
          "Invalid distance traveled. It cannot be negative.");
    }
    /* } */

    this.rep = BigDecimal.valueOf(rep.doubleValue());
  }

  public static DistanceTraveled create(final BigDecimal rep) {
    return new DistanceTraveled(rep);
  }

  /* } */

  /* { {IDistanceTraveled} implementation */

  /**
   * @see IDistanceTraveled#getFloatRep()
   */
  @Override
  public BigDecimal getFloatRep() {
    return this.rep;
  }

  /**
   * @see it.unibo.dronesystem.data.gauge.IGaugeValue#getDefRep()
   */
  @Override
  public String getDefRep() {
    return this.rep.toPlainString();
  }

  /**
   * @see it.unibo.dronesystem.data.gauge.IGaugeValue#equals(Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof IDistanceTraveled)) {
      return false;
    }
    final IDistanceTraveled distanceTraveled = (IDistanceTraveled) obj;
    return this.getFloatRep().equals(distanceTraveled.getFloatRep());
  }

  /**
   * @see IDistanceTraveled#compareTo(IDistanceTraveled)
   */
  @Override
  public int compareTo(final IDistanceTraveled distanceTraveled) {
    int result = 0;
    if (!this.equals(distanceTraveled)) {
      result = this.getFloatRep().compareTo(distanceTraveled.getFloatRep());
    }
    return result;
  }

  /* } */

  /* { Utility */

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + this.rep.hashCode();
    return result;
  }

  /* } */

  @Override
  public String toString() {
    return String.format("DistanceTraveled [value=%s]", this.getDefRep());
  }

}
