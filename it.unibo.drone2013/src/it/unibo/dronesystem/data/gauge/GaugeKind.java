package it.unibo.dronesystem.data.gauge;

/**
 * Project: dronesystem
 */
public enum GaugeKind {
  ODOMETER, FUELOMETER, SPEEDOMETER, LOCTRACKER;

  @Override
  public String toString() {
    switch (this) {
      case ODOMETER:
        return "Odometer";
      case FUELOMETER:
        return "Fuelometer";
      case SPEEDOMETER:
        return "Speedometer";
      case LOCTRACKER:
        return "Loctracker";
      default:
        throw new RuntimeException("Invalid gauge kind.");
    }
  }
}
