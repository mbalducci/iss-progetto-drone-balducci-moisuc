package it.unibo.dronesystem.data.gauge.speed;

import java.math.BigDecimal;

/**
 * Project: dronesystem
 * <p/>
 * A {ISpeed} implementation with high precision
 */
public class Speed implements ISpeed {

  private final BigDecimal rep;

  /* { Constructors and factories */

  protected Speed(final BigDecimal rep) {
    /* { Preconditions */
    if (rep.compareTo(BigDecimal.ZERO) < 0) {
      throw new IllegalArgumentException(
          "Invalid speed. It cannot be negative.");
    }
    /* } */

    this.rep = BigDecimal.valueOf(rep.doubleValue());
  }

  public static Speed create(final BigDecimal rep) {
    return new Speed(rep);
  }

  /* } */

  /* { {ISpeed} implementation */

  /**
   * @see ISpeed#getFloatRep()
   */
  @Override
  public BigDecimal getFloatRep() {
    return this.rep;
  }

  /**
   * @see it.unibo.dronesystem.data.gauge.IGaugeValue#getDefRep()
   */
  @Override
  public String getDefRep() {
    return this.rep.toPlainString();
  }

  /**
   * @see it.unibo.dronesystem.data.gauge.IGaugeValue#equals(Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof ISpeed)) {
      return false;
    }
    final ISpeed speed = (ISpeed) obj;
    return this.getFloatRep().equals(speed.getFloatRep());
  }

  /**
   * @see ISpeed#compareTo(ISpeed)
   */
  @Override
  public int compareTo(final ISpeed speed) {
    int result = 0;
    if (!this.equals(speed)) {
      result = this.getFloatRep().compareTo(speed.getFloatRep());
    }
    return result;
  }

  /* } */

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + this.rep.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return String.format("Speed [value=%s]", this.getDefRep());
  }

}
