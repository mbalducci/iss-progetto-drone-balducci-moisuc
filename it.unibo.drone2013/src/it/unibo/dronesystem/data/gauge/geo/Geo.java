package it.unibo.dronesystem.data.gauge.geo;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Project: dronesystem
 * <p/>
 * A {IGeo} implementation.
 */
public class Geo implements IGeo {

  private final IGeoCoordinate latitude;
  private final IGeoCoordinate longitude;

  /* { Constructors and factories */

  protected Geo(final IGeoCoordinate latitude, final IGeoCoordinate longitude) {
    /* { Preconditions */
    if (this.validateLatitude(latitude).equals(Boolean.FALSE)) {
      throw new IllegalArgumentException("Invalid latitude");
    }
    if (this.validateLongitude(longitude).equals(Boolean.FALSE)) {
      throw new IllegalArgumentException("Invalid longitude");
    }
    /* } */

    this.latitude = latitude;
    this.longitude = longitude;
  }

  public static Geo create(final IGeoCoordinate latitude,
      final IGeoCoordinate longitude) {
    return new Geo(latitude, longitude);
  }

  /* } */

  /* { {IGeo} implementation */

  /**
   * @see IGeo#getLongitude()
   */
  @Override
  public IGeoCoordinate getLatitude() {
    return this.latitude;
  }

  /**
   * @see IGeo#getLatitude()
   */
  @Override
  public IGeoCoordinate getLongitude() {
    return this.longitude;
  }

  /**
   * @see IGeo#isLongitudeValid()
   */
  @Override
  public Boolean isLatitudeValid() {
    return this.validateLatitude(this.latitude);
  }

  /**
   * @see IGeo#isLatitudeValid()
   */
  @Override
  public Boolean isLongitudeValid() {
    return this.validateLongitude(this.longitude);
  }

  /**
   * @see it.unibo.dronesystem.data.gauge.IGaugeValue#getDefRep()
   */
  @Override
  public String getDefRep() {
    return String.format("[%s,%s]", this.latitude.getDefRepInDMS(),
        this.longitude.getDefRepInDMS());
  }

  /**
   * @see it.unibo.dronesystem.data.gauge.IGaugeValue#equals(Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof IGeo)) {
      return false;
    }
    final IGeo geo = (IGeo) obj;
    return this.getLatitude().equals(geo.getLatitude())
        && this.getLongitude().equals(geo.getLongitude());
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + this.latitude.hashCode();
    result = 31 * result + this.longitude.hashCode();
    return result;
  }

  /* } */

  /* { Utility */

  private Boolean validateLatitude(IGeoCoordinate lat) {
    return (lat.getDegreesIntRep().compareTo(BigInteger.valueOf(-90)) >= 0)
        && (lat.getDegreesIntRep().compareTo(BigInteger.valueOf(90)) <= 0)
        && lat.validateCoordinate(BigDecimal.valueOf(-90),
            BigDecimal.valueOf(90));
  }

  private Boolean validateLongitude(IGeoCoordinate lon) {
    return (lon.getDegreesIntRep().compareTo(BigInteger.valueOf(-180)) >= 0)
        && (lon.getDegreesIntRep().compareTo(BigInteger.valueOf(180)) <= 0)
        && lon.validateCoordinate(BigDecimal.valueOf(-180),
            BigDecimal.valueOf(180));
  }

  /* } */

  @Override
  public String toString() {
    return String.format("Geo %s", this.getDefRep());
  }

}
