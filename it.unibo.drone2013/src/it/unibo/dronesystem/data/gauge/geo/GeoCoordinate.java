package it.unibo.dronesystem.data.gauge.geo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

/**
 * Project: dronesystem
 * <p/>
 * A {IGeoCoordinate} implementation with high precision.
 */
public class GeoCoordinate implements IGeoCoordinate {

  private final BigInteger degrees;
  private final BigInteger minutes;
  private final BigInteger seconds;

  /* { Constructors and factories */

  protected GeoCoordinate(final BigInteger degrees, final BigInteger minutes,
      final BigInteger seconds) {
    this.degrees = BigInteger.valueOf(degrees.longValue());
    this.minutes = BigInteger.valueOf(minutes.longValue());
    this.seconds = BigInteger.valueOf(seconds.longValue());
  }

  public static GeoCoordinate create(final BigInteger degrees,
      final BigInteger minutes, final BigInteger seconds) {
    return new GeoCoordinate(degrees, minutes, seconds);
  }

  public static GeoCoordinate create(final BigInteger degrees,
      final BigInteger minutes) {
    return new GeoCoordinate(degrees, minutes, BigInteger.ZERO);
  }

  public static GeoCoordinate create(final BigInteger degrees) {
    return new GeoCoordinate(degrees, BigInteger.ZERO, BigInteger.ZERO);
  }

  /* } */

  /* { {IGeoCoordinate} implementation */

  /**
   * @see IGeoCoordinate#getDegreesIntRep()
   */
  @Override
  public BigInteger getDegreesIntRep() {
    return this.degrees;
  }

  /**
   * @see IGeoCoordinate#getMinutesIntRep()
   */
  @Override
  public BigInteger getMinutesIntRep() {
    return this.minutes;
  }

  /**
   * @see IGeoCoordinate#getSecondsIntRep()
   */
  @Override
  public BigInteger getSecondsIntRep() {
    return this.seconds;
  }

  /**
   * @see IGeoCoordinate#getDecimalFloatRep()
   */
  @Override
  public BigDecimal getDecimalFloatRep() {
    BigDecimal degreesFloatRep = new BigDecimal(this.getDegreesIntRep());
    BigDecimal minutesFloatRep = new BigDecimal(this.getMinutesIntRep());
    BigDecimal secondsFloatRep = new BigDecimal(this.getSecondsIntRep());

    return degreesFloatRep.add(
        minutesFloatRep.divide(BigDecimal.valueOf(60.0), 3,
            RoundingMode.HALF_UP)).add(
        secondsFloatRep.divide(BigDecimal.valueOf(3600.0), 3,
            RoundingMode.HALF_UP));
  }

  /**
   * @see IGeoCoordinate#getDefRepInDMS()
   */
  @Override
  public String getDefRepInDMS() {
    return String.format("[%s,%s,%s]", this.getDegreesIntRep().toString(), this
        .getMinutesIntRep().toString(), this.getSecondsIntRep().toString());
  }

  /**
   * @see IGeoCoordinate#getDefRepInDecimal()
   */
  @Override
  public String getDefRepInDecimal() {
    return this.getDecimalFloatRep().toPlainString();
  }

  /**
   * @see IGeoCoordinate#validateCoordinate(java.math.BigDecimal,
   *      java.math.BigDecimal)
   */
  @Override
  public Boolean validateCoordinate(final BigDecimal minValueAllowed,
      final BigDecimal maxValueAllowed) {
    if (this.getDecimalFloatRep().compareTo(minValueAllowed) < 0) {
      return Boolean.FALSE;
    }

    if (this.getDecimalFloatRep().compareTo(maxValueAllowed) > 0) {
      return Boolean.FALSE;
    }

    if ((this.getMinutesIntRep().compareTo(BigInteger.ZERO) < 0)
        || (this.getMinutesIntRep().compareTo(BigInteger.valueOf(60)) > 0)) {
      return Boolean.FALSE;
    }

    if ((this.getSecondsIntRep().compareTo(BigInteger.ZERO) < 0)
        || (this.getSecondsIntRep().compareTo(BigInteger.valueOf(3600)) > 0)) {
      return Boolean.FALSE;
    }

    return Boolean.TRUE;
  }

  /**
   * @see IGeoCoordinate#equals(Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof IGeoCoordinate)) {
      return false;
    }
    final IGeoCoordinate coordinate = (IGeoCoordinate) obj;
    return this.getDegreesIntRep().equals(coordinate.getDegreesIntRep())
        && this.getMinutesIntRep().equals(coordinate.getMinutesIntRep())
        && this.getSecondsIntRep().equals(coordinate.getSecondsIntRep());
  }

  /**
   * @see IGeoCoordinate#compareTo(IGeoCoordinate)
   */
  @Override
  public int compareTo(final IGeoCoordinate geoCoordinate) {
    int result = 0;
    if (!this.equals(geoCoordinate)) {
      result = this.getDegreesIntRep().compareTo(
          geoCoordinate.getDegreesIntRep());
      if (result == 0) {
        result = this.getMinutesIntRep().compareTo(
            geoCoordinate.getMinutesIntRep());
        if (result == 0) {
          result = this.getSecondsIntRep().compareTo(
              geoCoordinate.getSecondsIntRep());
        }
      }
    }
    return result;
  }

  /* } */

  /* { Utility */

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + this.getDegreesIntRep().hashCode();
    result = 31 * result + this.getMinutesIntRep().hashCode();
    result = 31 * result + this.getSecondsIntRep().hashCode();
    return result;
  }

  /* } */

  @Override
  public String toString() {
    return String.format("GeoCoordinate %s", this.getDefRepInDMS());
  }

}
