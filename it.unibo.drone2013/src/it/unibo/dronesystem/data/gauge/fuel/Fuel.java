package it.unibo.dronesystem.data.gauge.fuel;

import java.math.BigDecimal;

/**
 * Project: dronesystem
 * <p/>
 * A {IFuel} implementation with high precision
 */
public class Fuel implements IFuel {

  private final BigDecimal rep;

  /* { Constructors and factories */

  protected Fuel(final BigDecimal rep) {
    /* { Preconditions */
    if (rep.compareTo(BigDecimal.ZERO) < 0) {
      throw new IllegalArgumentException("Invalid fuel. It cannot be negative.");
    }
    /* } */

    this.rep = BigDecimal.valueOf(rep.doubleValue());
  }

  public static Fuel create(final BigDecimal rep) {
    return new Fuel(rep);
  }

  /* } */

  /* { {IFuel} implementation */

  /**
   * @see IFuel#getFloatRep()
   */
  @Override
  public BigDecimal getFloatRep() {
    return this.rep;
  }

  /**
   * @see it.unibo.dronesystem.data.gauge.IGaugeValue#getDefRep()
   */
  @Override
  public String getDefRep() {
    return this.rep.toString();
  }

  /**
   * @see it.unibo.dronesystem.data.gauge.IGaugeValue#equals(Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof IFuel)) {
      return false;
    }
    final IFuel fuel = (IFuel) obj;
    return this.getFloatRep().equals(fuel.getFloatRep());
  }

  /**
   * @see IFuel#compareTo(IFuel)
   */
  @Override
  public int compareTo(final IFuel fuel) {
    int result = 0;
    if (!this.equals(fuel)) {
      result = this.getFloatRep().compareTo(fuel.getFloatRep());
    }
    return result;
  }

  /* } */

  /* { Utility */

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + this.rep.hashCode();
    return result;
  }

  /* } */

  @Override
  public String toString() {
    return String.format("Fuel [value=%s]", this.getDefRep());
  }

}
