package it.unibo.dronesystem.data.registering;

import it.unibo.dronesystem.data.IMsg;
import it.unibo.dronesystem.data.dronedata.droneid.IDroneId;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;

/**
 * Project: dronesystem
 * <p/>
 * A command to register a command receiver actor.
 */
public class RegisterCmdReceiverMsg implements IMsg {

  private final IDroneId associatedDroneId;
  private final String   cmdReceiverActorPath;

  private final ISpeed   speedMinAllowed;
  private final ISpeed   speedMaxAllowed;

  /* { Constructors and factories */

  protected RegisterCmdReceiverMsg(final IDroneId associatedDroneId,
      final String cmdReceiverActorPath, final ISpeed speedMinAllowed,
      final ISpeed speedMaxAllowed) {
    this.associatedDroneId = associatedDroneId;
    this.cmdReceiverActorPath = cmdReceiverActorPath;
    this.speedMinAllowed = speedMinAllowed;
    this.speedMaxAllowed = speedMaxAllowed;
  }

  public static RegisterCmdReceiverMsg create(final IDroneId associatedDroneId,
      final String cmdReceiverActorPath, final ISpeed speedMinAllowed,
      final ISpeed speedMaxAllowed) {
    return new RegisterCmdReceiverMsg(associatedDroneId, cmdReceiverActorPath,
        speedMinAllowed, speedMaxAllowed);
  }

  /* } */

  /* { Message properties */

  public IDroneId getAssociatedDroneId() {
    return this.associatedDroneId;
  }

  public String getCmdReceiverActorPath() {
    return this.cmdReceiverActorPath;
  }

  public ISpeed getSpeedMinAllowed() {
    return this.speedMinAllowed;
  }

  public ISpeed getSpeedMaxAllowed() {
    return this.speedMaxAllowed;
  }

  /* } */

  @Override
  public String toString() {
    return String.format("RegisterCmdReceiverMsg [droneId=%s][actorPath=%s]"
        + "[speedMin=%s][speedMax=%s]", this.associatedDroneId,
        this.cmdReceiverActorPath, this.speedMinAllowed.getDefRep(),
        this.speedMaxAllowed.getDefRep());
  }

}
