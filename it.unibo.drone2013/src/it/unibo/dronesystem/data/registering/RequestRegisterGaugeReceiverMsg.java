package it.unibo.dronesystem.data.registering;

import it.unibo.dronesystem.data.IMsg;
import it.unibo.dronesystem.data.dronedata.droneid.IDroneId;
import it.unibo.dronesystem.data.dronedata.dronemissionid.IDroneMissionId;
import it.unibo.dronesystem.data.gauge.GaugeKind;

/**
 * Project: dronesystem
 * <p/>
 * A message to request a gauge receiver.
 */
public class RequestRegisterGaugeReceiverMsg implements IMsg {

  private final String          receiverActorPath;

  private final IDroneId        droneOfInterest;
  private final IDroneMissionId missionOfInterest;

  private final GaugeKind       gaugeKind;

  /* { Constructors and factories */

  protected RequestRegisterGaugeReceiverMsg(final String receiverActorPath,
      final IDroneId droneOfInterest, final IDroneMissionId missionOfInterest,
      final GaugeKind gaugeKind) {
    this.receiverActorPath = receiverActorPath;

    this.droneOfInterest = droneOfInterest;
    this.missionOfInterest = missionOfInterest;

    this.gaugeKind = gaugeKind;
  }

  /**
   * Create a message to request a gauge receiver in order to receive
   * updates for </b>the provided droneOfInterest and missionOfInterest</b>.
   * 
   * @param receiverActorPath
   *          The path for the receiver actor.
   * @param droneOfInterest
   *          The drone identifier of interest (data from that
   *          drone will be sent to the gauge receiver actor).
   * @param missionOfInterest
   *          The drone identifier of interest (data from
   *          that drone will be sent to the gauge receiver actor).
   * @return The created {RequestRegisterGaugeReceiverMsg}
   */
  public static RequestRegisterGaugeReceiverMsg create(
      final String receiverActorPath, final IDroneId droneOfInterest,
      final IDroneMissionId missionOfInterest, final GaugeKind gaugeKind) {

    return new RequestRegisterGaugeReceiverMsg(receiverActorPath,
        droneOfInterest, missionOfInterest, gaugeKind);
  }

  /* } */

  /* { Message properties */

  public String getReceiverActorPath() {
    return this.receiverActorPath;
  }

  public GaugeKind getGaugeKind() {
    return this.gaugeKind;
  }

  /**
   * @return The identifier for the drone of interest. If the interest if for
   *         all drones it returns null.
   */
  public IDroneId getDroneOfInterest() {
    return this.droneOfInterest;
  }

  /**
   * @return The identifier for the mission of interest. If the interest if for
   *         all missions it returns null.
   */
  public IDroneMissionId getMissionOfInterest() {
    return this.missionOfInterest;
  }

  /* } */

  @Override
  public String toString() {
    return String.format("RequestRegisterGaugeReceiverMsg "
        + "[droneId=%s][missionId=%s][receiverPath=%s][gaugeKind=%s]",
        this.getDroneOfInterest(), this.getMissionOfInterest(),
        this.getReceiverActorPath(), this.getGaugeKind());
  }

}
