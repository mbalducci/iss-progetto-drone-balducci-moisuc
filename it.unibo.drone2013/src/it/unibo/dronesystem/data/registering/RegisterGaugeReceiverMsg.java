package it.unibo.dronesystem.data.registering;

import it.unibo.dronesystem.data.IMsg;

/**
 * Project: dronesystem
 * <p/>
 * A message to register a receiver. The factory to create the receiver is
 * provided because the responsibility about the receiver creation is of the
 * actor that receives this message.
 */
public class RegisterGaugeReceiverMsg implements IMsg {

  private final String receiverActorPath;

  /* { Constructors and factories */

  protected RegisterGaugeReceiverMsg(final String receiverActorPath) {
    this.receiverActorPath = receiverActorPath;
  }

  public static RegisterGaugeReceiverMsg create(final String receiverActorPath) {
    return new RegisterGaugeReceiverMsg(receiverActorPath);
  }

  /* } */

  /* { Message properties */

  public String getReceiverActorPath() {
    return this.receiverActorPath;
  }

  /* } */

}
