package it.unibo.dronesystem.data.registering;

import it.unibo.dronesystem.data.IMsg;
import it.unibo.dronesystem.data.dronedata.droneid.IDroneId;

/**
 * Project: dronesystem
 * <p/>
 * A command to register a command issuer actor.
 */
public class RegisterCmdIssuerMsg implements IMsg {

  private final String   actorPath;
  private final IDroneId droneIdOfInterest;

  /* { Constructors and factories */

  protected RegisterCmdIssuerMsg(final String actorPath,
      final IDroneId droneIdOfInterest) {
    this.actorPath = actorPath;
    this.droneIdOfInterest = droneIdOfInterest;
  }

  public static RegisterCmdIssuerMsg create(final String actorPath,
      final IDroneId droneIdOfInterest) {
    return new RegisterCmdIssuerMsg(actorPath, droneIdOfInterest);
  }

  /* } */

  /* { Message properties */

  public String getCmdIssuerActorPath() {
    return this.actorPath;
  }

  public IDroneId getDroneIdOfInterest() {
    return this.droneIdOfInterest;
  }

  /* } */

  @Override
  public String toString() {
    return String.format("RegisterCmdIssuerMsg [droneId=%s][actorPath=%s]",
        this.droneIdOfInterest, this.actorPath);
  }

}
