package it.unibo.dronesystem.data.dronedata.dronemissionid;

import java.math.BigInteger;

/**
 * Project: dronesystem
 * <p/>
 * A {IDroneId} implementation with high precision.
 */
public class DroneMissionId implements IDroneMissionId {

  private final BigInteger rep;

  /* { Constructors and factories */

  protected DroneMissionId(final BigInteger rep) {
    if (rep == null) {
      this.rep = rep;
    } else {
      this.rep = BigInteger.valueOf(rep.longValue());
    }
  }

  public static DroneMissionId create(final BigInteger rep) {
    return new DroneMissionId(rep);
  }

  /**
   * Create a mission id that represents any mission.
   */
  public static DroneMissionId createIdForAnyMission() {
    return new DroneMissionId(null);
  }

  /* } */

  /* { {IDroneId} implementation */

  /**
   * @see IDroneMissionId#getIntRep()
   */
  @Override
  public BigInteger getIntRep() {
    return this.rep;
  }

  /**
   * @see IDroneMissionId#isAnyMission()
   */
  @Override
  public Boolean isAnyMission() {
    return this.rep == null;
  }

  /**
   * @see IDroneMissionId#equals(Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof DroneMissionId)) {
      return false;
    }
    final DroneMissionId droneMissionId = (DroneMissionId) obj;
    return this.getIntRep().equals(droneMissionId.getIntRep());
  }

  /* } */

  /* { Utility */

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + this.rep.hashCode();
    return result;
  }

  /* } */

  @Override
  public String toString() {
    if (this.rep != null) {
      return String.format("DroneMissionId [missionId=%s]", this.rep);
    }
    return "DroneMissionId [AnyMission]";
  }

}
