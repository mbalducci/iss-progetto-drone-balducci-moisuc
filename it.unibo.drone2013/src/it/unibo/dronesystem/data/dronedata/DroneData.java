package it.unibo.dronesystem.data.dronedata;

import it.unibo.dronesystem.data.dronedata.droneid.IDroneId;
import it.unibo.dronesystem.data.dronedata.dronemissionid.IDroneMissionId;
import it.unibo.dronesystem.data.dronedata.image.IImage;
import it.unibo.dronesystem.data.gauge.distancetraveled.IDistanceTraveled;
import it.unibo.dronesystem.data.gauge.fuel.IFuel;
import it.unibo.dronesystem.data.gauge.geo.IGeo;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;

/**
 * Project: dronesystem
 * <p/>
 * A {IDroneData} implementation.
 */
public class DroneData implements IDroneData {

  private final IFuel             fuel;
  private final IDistanceTraveled distanceTraveled;
  private final ISpeed            speed;
  private final IGeo              geo;
  private final IImage            image;
  private final IDroneId          droneId;
  private final IDroneMissionId   droneMissionId;

  /* { Constructors and factories */

  protected DroneData(final IFuel fuel,
      final IDistanceTraveled distanceTraveled, final ISpeed speed,
      final IGeo geo, final IImage image, final IDroneId droneId,
      final IDroneMissionId droneMissionId) {

    this.fuel = fuel;
    this.distanceTraveled = distanceTraveled;
    this.speed = speed;
    this.geo = geo;
    this.image = image;
    this.droneId = droneId;
    this.droneMissionId = droneMissionId;
  }

  public static DroneData create(final IFuel fuel,
      final IDistanceTraveled distanceTraveled, final ISpeed speed,
      final IGeo geo, final IImage image, final IDroneId droneId,
      final IDroneMissionId droneMissionId) {

    return new DroneData(fuel, distanceTraveled, speed, geo, image, droneId,
        droneMissionId);
  }

  /* } */

  /*
   * { {IDroneData} implementation
   */

  /**
   * @see IDroneData#getFuel()
   */
  @Override
  public IFuel getFuel() {
    return this.fuel;
  }

  /**
   * @see IDroneData#getDistanceTraveled()
   */
  @Override
  public IDistanceTraveled getDistanceTraveled() {
    return this.distanceTraveled;
  }

  /**
   * @see IDroneData#getSpeed()
   */
  @Override
  public ISpeed getSpeed() {
    return this.speed;
  }

  /**
   * @see IDroneData#getGeo()
   */
  @Override
  public IGeo getGeo() {
    return this.geo;
  }

  /**
   * @see IDroneData#getImage()
   */
  @Override
  public IImage getImage() {
    return this.image;
  }

  /**
   * @see IDroneData#getDroneId()
   */
  @Override
  public IDroneId getDroneId() {
    return this.droneId;
  }

  /**
   * @see IDroneData#getDroneMissionId()
   */
  @Override
  public IDroneMissionId getDroneMissionId() {
    return this.droneMissionId;
  }

  /**
   * @see IDroneData#equals(Object)
   */
  @Override
  public boolean equals(final Object obj) {
    if (obj == this) {
      return true;
    }

    if (!(obj instanceof DroneData)) {
      return false;
    }

    final DroneData droneData = (DroneData) obj;

    return this.getFuel().equals(droneData.getFuel())
        && this.getDistanceTraveled().equals(droneData.getDistanceTraveled())
        && this.getSpeed().equals(droneData.getSpeed())
        && this.getGeo().equals(droneData.getGeo())
        && this.getImage().equals(droneData.getImage())
        && this.getDroneId().equals(droneData.getDroneId())
        && this.getDroneMissionId().equals(droneData.getDroneMissionId());
  }

  /* } */

  /* { Utility */

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + this.fuel.hashCode();
    result = 31 * result + this.distanceTraveled.hashCode();
    result = 31 * result + this.speed.hashCode();
    result = 31 * result + this.geo.hashCode();
    result = 31 * result + this.image.hashCode();
    result = 31 * result + this.droneId.hashCode();
    result = 31 * result + this.droneMissionId.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return String.format("DroneData { %s } { %s } { %s } { %s } { %s } "
        + "{ %s } { %s }", this.getFuel(), this.getDistanceTraveled(),
        this.getSpeed(), this.getGeo(), this.getImage(), this.getDroneId(),
        this.getDroneMissionId());
  }

  /* } */

}
