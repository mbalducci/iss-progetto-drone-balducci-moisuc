package it.unibo.dronesystem.data.dronedata.image;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;

import javax.imageio.ImageIO;

/**
 * Project: dronesystem
 * <p/>
 * A {IImage} implementation.
 */
public class Image implements IImage {

  private final byte[] rawData;

  /* { Constructors and factories */

  protected Image(final byte[] rawData) {
    this.rawData = rawData;
  }

  public static Image create(final byte[] rawData) {
    return new Image(rawData);
  }

  /* } */

  /* { {IImage} implementation */

  /**
   * @see IImage#getBlob()
   */
  @Override
  public byte[] getBlob() {
    return this.rawData;
  }

  /**
   * @see IImage#equals(Object)
   */
  @Override
  public boolean equals(final Object obj) {
    if (obj == this) {
      return true;
    }

    if (!(obj instanceof IImage)) {
      return false;
    }
    final IImage img = (IImage) obj;

    if (this.getBlob().length != img.getBlob().length) {
      return false;
    }

    for (int idx = 0; idx < this.getBlob().length; idx++) {
      if (this.getBlob()[idx] != img.getBlob()[idx]) {
        return false;
      }
    }

    return true;
  }

  /* } */

  /* { Utility */

  @Override
  public int hashCode() {
    int result = 17;
    for (int idx = 0; idx < this.getBlob().length; idx++) {
      result = 31 * result + this.getBlob()[idx];
    }
    return result;
  }

  /* } */

  @Override
  public String toString() {
    if (this.getBlob().length > 0) {
      BufferedImage img;
      try {
        img = ImageIO.read(new ByteArrayInputStream(this.getBlob()));
        if (img == null) {
          throw new Exception("Cannot convert to BufferedImage");
        }
      } catch (Exception exc) {
        return "Image";
      }
      return String.format("Image [width=%d][height=%d]", img.getWidth(),
          img.getHeight());
    }
    return "Image [size=empty]";
  }

}
