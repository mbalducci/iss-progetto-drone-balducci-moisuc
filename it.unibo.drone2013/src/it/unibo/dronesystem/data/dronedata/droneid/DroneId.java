package it.unibo.dronesystem.data.dronedata.droneid;

import java.math.BigInteger;

/**
 * Project: dronesystem
 * <p/>
 * A {IDroneId} implementation with high precision.
 */
public class DroneId implements IDroneId {

  private final BigInteger rep;

  /* { Constructors and factories */

  protected DroneId(final BigInteger rep) {
    this.rep = BigInteger.valueOf(rep.longValue());
  }

  public static DroneId create(final BigInteger rep) {
    return new DroneId(rep);
  }

  /* } */

  /* { {IDroneId} implementation */

  /**
   * @see IDroneId#getIntRep()
   */
  @Override
  public BigInteger getIntRep() {
    return this.rep;
  }

  /**
   * @see IDroneId#equals(Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof IDroneId)) {
      return false;
    }
    final IDroneId droneId = (IDroneId) obj;
    return this.getIntRep().equals(droneId.getIntRep());
  }

  /* } */

  /* { Utility */

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + this.rep.hashCode();
    return result;
  }

  /* } */

  @Override
  public String toString() {
    return String.format("DroneId [id=%s]", this.rep);
  }

}
