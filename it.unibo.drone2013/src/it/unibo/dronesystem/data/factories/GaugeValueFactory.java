package it.unibo.dronesystem.data.factories;

import it.unibo.dronesystem.data.gauge.distancetraveled.DistanceTraveled;
import it.unibo.dronesystem.data.gauge.distancetraveled.IDistanceTraveled;
import it.unibo.dronesystem.data.gauge.fuel.Fuel;
import it.unibo.dronesystem.data.gauge.fuel.IFuel;
import it.unibo.dronesystem.data.gauge.geo.Geo;
import it.unibo.dronesystem.data.gauge.geo.GeoCoordinate;
import it.unibo.dronesystem.data.gauge.geo.IGeo;
import it.unibo.dronesystem.data.gauge.geo.IGeoCoordinate;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;
import it.unibo.dronesystem.data.gauge.speed.Speed;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Project: dronesystem
 * <p/>
 * A {IGaugeValueFactory} implementation.
 */
public class GaugeValueFactory implements IGaugeValueFactory {

  /* { {IGaugeValueFactory} implementation */

  /**
   * @see IGaugeValueFactory#createSpeed(BigDecimal)
   */
  @Override
  public ISpeed createSpeed(final BigDecimal rep) {
    return Speed.create(rep);
  }

  /**
   * @see IGaugeValueFactory#createFuel(BigDecimal)
   */
  @Override
  public IFuel createFuel(final BigDecimal rep) {
    return Fuel.create(rep);
  }

  /**
   * @see IGaugeValueFactory#createDistanceTraveled(BigDecimal)
   */
  @Override
  public IDistanceTraveled createDistanceTraveled(final BigDecimal rep) {
    return DistanceTraveled.create(rep);
  }

  /**
   * @see IGaugeValueFactory#createGeo(IGeoCoordinate, IGeoCoordinate)
   */
  @Override
  public IGeo createGeo(final IGeoCoordinate latitude,
      final IGeoCoordinate longitude) {
    return Geo.create(latitude, longitude);
  }

  /**
   * @see IGaugeValueFactory#createGeoCoordinate(BigInteger, BigInteger,
   *      BigInteger)
   */
  @Override
  public IGeoCoordinate createGeoCoordinate(final BigInteger degrees,
      final BigInteger minutes, final BigInteger seconds) {
    return GeoCoordinate.create(degrees, minutes, seconds);
  }

  /* } */

}
