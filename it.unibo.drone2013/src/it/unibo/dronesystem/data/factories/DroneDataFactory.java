package it.unibo.dronesystem.data.factories;

import it.unibo.dronesystem.data.dronedata.DroneData;
import it.unibo.dronesystem.data.dronedata.IDroneData;
import it.unibo.dronesystem.data.dronedata.droneid.DroneId;
import it.unibo.dronesystem.data.dronedata.droneid.IDroneId;
import it.unibo.dronesystem.data.dronedata.dronemissionid.DroneMissionId;
import it.unibo.dronesystem.data.dronedata.dronemissionid.IDroneMissionId;
import it.unibo.dronesystem.data.dronedata.image.IImage;
import it.unibo.dronesystem.data.dronedata.image.Image;
import it.unibo.dronesystem.data.gauge.distancetraveled.IDistanceTraveled;
import it.unibo.dronesystem.data.gauge.fuel.IFuel;
import it.unibo.dronesystem.data.gauge.geo.IGeo;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;

import java.math.BigInteger;

/**
 * Project: dronesystem
 * <p/>
 * A {IDroneDataFactory} implementation.
 */
public class DroneDataFactory extends GaugeValueFactory implements
    IDroneDataFactory {

  /* { {IDroneDataFactory} implementation */

  /**
   * @see IDroneDataFactory#createImage(byte[])
   */
  @Override
  public IImage createImage(final byte[] rawData) {
    return Image.create(rawData);
  }

  /**
   * @see IDroneDataFactory#createDroneId(BigInteger)
   */
  @Override
  public IDroneId createDroneId(final BigInteger rep) {
    return DroneId.create(rep);
  }

  /**
   * @see IDroneDataFactory#createDroneMissionId(BigInteger)
   */
  @Override
  public IDroneMissionId createDroneMissionId(final BigInteger rep) {
    return DroneMissionId.create(rep);
  }

  /**
   * @see IDroneDataFactory#createDroneMissionId(BigInteger)
   */
  @Override
  public IDroneMissionId createDroneMissionIdForAnyMission() {
    return DroneMissionId.createIdForAnyMission();
  }

  /**
   * @see IDroneDataFactory#createDroneData(IFuel, IDistanceTraveled, ISpeed,
   *      IGeo, IImage, IDroneId, IDroneMissionId)
   */
  @Override
  public IDroneData createDroneData(final IFuel fuel,
      final IDistanceTraveled distanceTraveled, final ISpeed speed,
      final IGeo geo, final IImage image, final IDroneId droneId,
      final IDroneMissionId droneMissionId) {

    return DroneData.create(fuel, distanceTraveled, speed, geo, image, droneId,
        droneMissionId);
  }

  /* } */

}
