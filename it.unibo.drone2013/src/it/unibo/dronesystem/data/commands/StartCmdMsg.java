package it.unibo.dronesystem.data.commands;

/**
 * Project: dronesystem
 * <p/>
 * A {IStartCmdMsg} implementation.
 */
public class StartCmdMsg extends CmdMsg implements IStartCmdMsg {

  /* { Constructors and factories */

  protected StartCmdMsg() {
    super();
  }

  public static StartCmdMsg create() {
    return new StartCmdMsg();
  }

  /* } */

  @Override
  public String toString() {
    return "StartCmdMsg";
  }

}
