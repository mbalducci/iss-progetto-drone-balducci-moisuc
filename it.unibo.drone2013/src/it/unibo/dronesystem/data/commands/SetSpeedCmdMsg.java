package it.unibo.dronesystem.data.commands;

import it.unibo.dronesystem.data.gauge.speed.ISpeed;

/**
 * Project: dronesystem
 * <p/>
 * A {ISetSpeedCmdMsg} implementation.
 */
public class SetSpeedCmdMsg extends GaugeCmdMsg implements ISetSpeedCmdMsg {

  /* { Constructors and factories */

  protected SetSpeedCmdMsg(final ISpeed gaugeValue) {
    super(gaugeValue);
  }

  public static SetSpeedCmdMsg create(final ISpeed gaugeValue) {
    return new SetSpeedCmdMsg(gaugeValue);
  }

  /* } */

  @Override
  public String toString() {
    return String.format("SetSpeedCmdMsg [value=%s]", this.getValue()
        .getDefRep());
  }

}
