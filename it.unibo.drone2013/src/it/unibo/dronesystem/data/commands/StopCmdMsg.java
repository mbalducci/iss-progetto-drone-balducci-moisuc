package it.unibo.dronesystem.data.commands;

/**
 * Project: dronesystem
 * <p/>
 * A {IStopCmdMsg} implementation.
 */
public class StopCmdMsg extends CmdMsg implements IStopCmdMsg {

  /* { Constructors and factories */

  protected StopCmdMsg() {
    super();
  }

  public static StopCmdMsg create() {
    return new StopCmdMsg();
  }

  /* } */

  @Override
  public String toString() {
    return "StopCmdMsg";
  }

}
