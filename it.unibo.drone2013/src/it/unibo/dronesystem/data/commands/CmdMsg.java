package it.unibo.dronesystem.data.commands;

/**
 * Project: dronesystem
 * <p/>
 * A {ICmdMsg} implementation.
 */
public abstract class CmdMsg implements ICmdMsg {

  /* { Constructors and factories */

  protected CmdMsg() {}

  /* } */

}
