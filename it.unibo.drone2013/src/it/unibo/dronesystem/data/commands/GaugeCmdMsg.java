package it.unibo.dronesystem.data.commands;

import it.unibo.dronesystem.data.gauge.IGaugeValue;

/**
 * Project: dronesystem
 * <p/>
 * A {IGaugeCmdMsg} implementation.
 */
public abstract class GaugeCmdMsg extends CmdMsg implements IGaugeCmdMsg {

  protected final IGaugeValue gaugeValue;

  /* { Constructors and factories */

  protected GaugeCmdMsg(final IGaugeValue gaugeValue) {
    super();
    this.gaugeValue = gaugeValue;
  }

  /* } */

  /* { {IGaugeCmdMsg} implementation */

  /**
   * @see IGaugeCmdMsg#getValue()
   */
  @Override
  public IGaugeValue getValue() {
    return this.gaugeValue;
  }

  /* } */

}
