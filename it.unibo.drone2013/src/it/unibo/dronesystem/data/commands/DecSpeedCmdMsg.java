package it.unibo.dronesystem.data.commands;

import it.unibo.dronesystem.data.gauge.speed.ISpeed;

/**
 * Project: dronesystem
 * <p/>
 * A {IDecSpeedCmdMsg} implementation.
 */
public class DecSpeedCmdMsg extends GaugeCmdMsg implements IDecSpeedCmdMsg {

  /* { Constructors and factories */

  protected DecSpeedCmdMsg(final ISpeed gaugeValue) {
    super(gaugeValue);
  }

  public static DecSpeedCmdMsg create(final ISpeed gaugeValue) {
    return new DecSpeedCmdMsg(gaugeValue);
  }

  /* } */

  @Override
  public String toString() {
    return String.format("DecSpeedCmdMsg [amount=%s]", this.getValue()
        .getDefRep());
  }

}
