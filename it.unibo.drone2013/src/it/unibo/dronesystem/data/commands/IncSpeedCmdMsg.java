package it.unibo.dronesystem.data.commands;

import it.unibo.dronesystem.data.gauge.speed.ISpeed;

/**
 * Project: dronesystem
 * <p/>
 * A {IIncSpeedCmdMsg} implementation.
 */
public class IncSpeedCmdMsg extends GaugeCmdMsg implements IIncSpeedCmdMsg {

  /* { Constructors and factories */

  protected IncSpeedCmdMsg(final ISpeed gaugeValue) {
    super(gaugeValue);
  }

  public static IncSpeedCmdMsg create(final ISpeed gaugeValue) {
    return new IncSpeedCmdMsg(gaugeValue);
  }

  /* } */

  @Override
  public String toString() {
    return String.format("IncSpeedCmdMsg [amount=%s]", this.getValue()
        .getDefRep());
  }

}
