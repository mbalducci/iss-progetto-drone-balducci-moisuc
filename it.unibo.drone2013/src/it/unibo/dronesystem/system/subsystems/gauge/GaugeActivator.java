package it.unibo.dronesystem.system.subsystems.gauge;

import it.unibo.dronesystem.data.factories.GaugeValueFactory;
import it.unibo.dronesystem.data.factories.IGaugeValueFactory;
//import it.unibo.dronesystem.gauge.factories.GaugeActorFactory;
import it.unibo.dronesystem.gauge.factories.GaugeFactory;
//import it.unibo.dronesystem.gauge.factories.IGaugeActorFactory;
import it.unibo.dronesystem.gauge.factories.IGaugeFactory;
import it.unibo.dronesystem.system.subsystems.ISubsystemActivator;
import it.unibo.dronesystem.system.subsystems.SubsystemActivator;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Project: dronesystem
 * <p/>
 * The gauge subsystem activator
 */
public class GaugeActivator extends SubsystemActivator implements
    ISubsystemActivator {

  private Properties        gaugeProps;

  private BigDecimal        fuelMin;
  private BigDecimal        speedMin;
  private BigDecimal        speedMax;

  private GaugeValueFactory gaugeValueFactory;
  private GaugeFactory      gaugeFactory;
//  private GaugeActorFactory gaugeActorFactory;

  /* { Constructors and factories */

  protected GaugeActivator() throws Exception {
    super();
  }

  public static GaugeActivator create() throws Exception {
    return new GaugeActivator();
  }

  /* } */

  /* { {SubsystemActivator} implementation */

  /**
   * @see SubsystemActivator#getDependencies()
   */
  @Override
  protected Map<String, ISubsystemActivator> getDependencies() {
    return new HashMap<String, ISubsystemActivator>();
  }

  /**
   * @see SubsystemActivator#startSubsystem()
   */
  @Override
  protected void startSubsystem() throws Exception {
    this.setupGaugeProps();

    this.setupGaugeValueFactory();
    this.setupGaugeFactory();
//    this.setupGaugeActorFactory();
  }

  /**
   * @see SubsystemActivator#stopSubsystem()
   */
  @Override
  protected void stopSubsystem() throws Exception {
    /* INF: Empty */
  }

  /* } */

  /* { Setup & Start facilities */

  protected void setupGaugeValueFactory() {
    this.gaugeValueFactory = new GaugeValueFactory();
  }

  protected void setupGaugeProps() throws Exception {
    this.gaugeProps = new Properties();

    InputStream propsStream = this.getClass().getResourceAsStream(
        "/it/unibo/dronesystem/gauge/gauge_limits.properties");
    this.gaugeProps.load(propsStream);

    this.fuelMin = new BigDecimal(this.gaugeProps.getProperty("fuelMin"));
    this.speedMin = new BigDecimal(this.gaugeProps.getProperty("speedMin"));
    this.speedMax = new BigDecimal(this.gaugeProps.getProperty("speedMax"));
  }

  protected void setupGaugeFactory() {
    this.gaugeFactory = new GaugeFactory(this.getGaugeValueFactory()
        .createFuel(this.fuelMin), this.getGaugeValueFactory().createSpeed(
        this.speedMin), this.getGaugeValueFactory().createSpeed(this.speedMax));
  }

//  protected void setupGaugeActorFactory() {
//    this.gaugeActorFactory = new GaugeActorFactory();
//  }

  /* } */

  /* { Utility */

  public IGaugeValueFactory getGaugeValueFactory() {
    return this.gaugeValueFactory;
  }

  public IGaugeFactory getGaugeFactory() {
    return this.gaugeFactory;
  }

//  public IGaugeActorFactory getGaugeActorFactory() {
//    return this.gaugeActorFactory;
//  }

  /* } */
}
