package it.unibo.dronesystem.system.subsystems;

import java.util.Map;

/**
 * Project: dronesystem
 * <p/>
 * A basic class to create subsystem activators complaint with the
 * {ISubsystemActivator} contract.
 */
public abstract class SubsystemActivator implements ISubsystemActivator {
  private Boolean                                isStarted;
  private final Map<String, ISubsystemActivator> dependencies;

  /* { Constructors and factories */

  protected SubsystemActivator() throws Exception {
    this.isStarted = Boolean.FALSE;
    this.dependencies = this.getDependencies();
  }

  /* } */

  /* { {ISubsystemActivator} implementation */

  /**
   * @see ISubsystemActivator#start()
   */
  @Override
  public final void start() throws Exception {
    System.out.println(">>> Starting subsystem dependencies.");
    this.startDependencies();
    System.out.println(">>> Subsystem dependencies started.");
    System.out.println(">>> Starting subsystem.");
    this.startSubsystem();
    System.out.println(">>> Subsystem started.");
    this.isStarted = Boolean.TRUE;
  }

  /**
   * @see ISubsystemActivator#stop()
   */
  @Override
  public final void stop() throws Exception {
    System.out.println("<<< Stopping subsystem.");
    this.stopSubsystem();
    System.out.println("<<< Subsystem stopped.");
    System.out.println("<<< Stopping subsystem dependencies.");
    this.stopDependencies();
    System.out.println("<<< Subsystem dependencies stopped.");
    this.isStarted = Boolean.FALSE;
  }

  /**
   * @see ISubsystemActivator#isStarted()
   */
  @Override
  public Boolean isStarted() {
    return this.isStarted;
  }

  /* } */

  /* { Utility */

  /**
   * Start the subsystem dependencies.
   */
  protected void startDependencies() throws Exception {
    for (ISubsystemActivator subsystemActivator : this.dependencies.values()) {
      if (subsystemActivator.isStarted().equals(Boolean.FALSE)) {
        subsystemActivator.start();
      }
    }
  }

  /**
   * Stop the subsystem dependencies.
   */
  protected void stopDependencies() throws Exception {
    for (ISubsystemActivator subsystemActivator : this.dependencies.values()) {
      if (subsystemActivator.isStarted().equals(Boolean.TRUE)) {
        subsystemActivator.stop();
      }
    }
  }

  /**
   * Start the underlying subsystem.
   */
  protected abstract void startSubsystem() throws Exception;

  /**
   * Stop the underlying subsystem.
   */
  protected abstract void stopSubsystem() throws Exception;

  /**
   * @return The list of the dependencies for the underlying subsystem.
   */
  protected abstract Map<String, ISubsystemActivator> getDependencies()
      throws Exception;

  /**
   * @return The subsystem identified by the provided name.
   */
  protected ISubsystemActivator getDependency(final String name) {
    return this.dependencies.get(name);
  }

  /* } */

}
