package it.unibo.dronesystem.system.subsystems.display.displays;

import it.unibo.dronesystem.data.dronedata.droneid.IDroneId;
import it.unibo.dronesystem.data.dronedata.dronemissionid.IDroneMissionId;
import it.unibo.dronesystem.data.factories.DroneDataFactory;
import it.unibo.dronesystem.data.factories.IDroneDataFactory;
import it.unibo.dronesystem.data.gauge.GaugeKind;
import it.unibo.dronesystem.data.gauge.fuel.IFuel;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;
import it.unibo.dronesystem.data.registering.RequestRegisterGaugeReceiverMsg;
import it.unibo.dronesystem.display.displays.IGaugeDisplay;
import it.unibo.dronesystem.display.displays.implementors.swing.IGaugeDisplaySwingImpl;
//import it.unibo.dronesystem.display.factories.DisplayActorFactory;
import it.unibo.dronesystem.display.factories.DisplayFactory;
import it.unibo.dronesystem.display.factories.DisplaySwingImplFactory;
//import it.unibo.dronesystem.display.factories.GaugeViewActorFactory;
import it.unibo.dronesystem.display.factories.IDisplayActorFactory;
import it.unibo.dronesystem.display.factories.IDisplayFactory;
import it.unibo.dronesystem.display.factories.IDisplaySwingImplFactory;
import it.unibo.dronesystem.display.factories.IGaugeViewActorFactory;
import it.unibo.dronesystem.display.views.IFuelometerView;
import it.unibo.dronesystem.display.views.IGaugeView;
import it.unibo.dronesystem.display.views.ILocTrackerView;
import it.unibo.dronesystem.display.views.IOdometerView;
import it.unibo.dronesystem.display.views.ISpeedometerView;
import it.unibo.dronesystem.display.views.implementors.swing.IFuelometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.ILocTrackerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.IOdometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.ISpeedometerViewSwingImpl;
//import it.unibo.dronesystem.system.utils.ActorSystemManager;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Properties;

//import akka.actor.ActorRef;
//import akka.actor.ActorSystem;

/**
 * Project: dronesystem
 */
public class GaugeDisplayActivator {

  private Properties                gaugeDisplayProps;
  private IFuel                     minFuelValueShown;
  private IFuel                     maxFuelValueShown;
  private ISpeed                    minSpeedValueShown;
  private ISpeed                    maxSpeedValueShown;
  private IDroneId                  droneOfInterest;
  private IDroneMissionId           missionOfInterest;

  private IDroneDataFactory         droneDataFactory;
  private IDisplaySwingImplFactory  gaugeDisplayImplFactory;
  private IDisplayFactory           gaugeDisplayFactory;
  private IDisplayActorFactory      gaugeDisplayActorFactory;
  private IGaugeViewActorFactory    gaugeViewActorFactory;

  private IGaugeDisplay             gaugeDisplay;
  private IGaugeDisplaySwingImpl    gaugeDisplayImpl;

  private IFuelometerView           fuelometerView;
  private IFuelometerViewSwingImpl  fuelometerViewImpl;

  private ISpeedometerView          speedometerView;
  private ISpeedometerViewSwingImpl speedometerViewImpl;

  private IOdometerView             odometerView;
  private IOdometerViewSwingImpl    odometerViewImpl;

  private ILocTrackerView           locTrackerView;
  private ILocTrackerViewSwingImpl  locTrackerViewImpl;

//  private ActorSystemManager        actorSystemManager;
//  private ActorSystem               actorSystem;
//
//  private ActorRef                  dataManagerActor;
//  private ActorRef                  gaugeDisplayActor;

  /* { Constructors and factories */

  /**
   * A gauge display is hosted in a dashboard, so the host for the gauge display
   * should pass its actorSystem as argument. The gauge display will use that
   * actorSystem in order to create the actors related to the gauge display
   * (and the gauge views).
   */
  protected GaugeDisplayActivator()
      throws Exception {
    super();

    this.setupDroneDataFactory();
    this.setupGaugeDisplayFactories();
//    this.setupGaugeViewActorFactory();
    this.setupGaugeDisplayProps();

    this.setupGaugeViewsImpl();
    this.setupGaugeViews();
    this.setupGaugeDisplayImpl();
    this.setupGaugeDisplay();

//    this.actorSystemManager = actorSystemManager;
//    this.actorSystem = this.actorSystemManager.getActorSystem();

//    this.setupExternalActors();
//    this.setupGaugeViewsActors();
//    this.setupGaugeDisplayActor();
    
    	System.out.println("%%% GaugeDisplayActivator " + this);
  }

  public static GaugeDisplayActivator create() throws Exception {
    return new GaugeDisplayActivator();
  }

  /* } */

  /* { Setup */

  protected void setupGaugeDisplayProps() throws Exception {
    this.gaugeDisplayProps = new Properties();

    InputStream propsStream = this.getClass().getResourceAsStream(
        "/it/unibo/dronesystem/system/display/gaugedisplay.properties");
    this.gaugeDisplayProps.load(propsStream);

    this.minFuelValueShown = this.droneDataFactory.createFuel(new BigDecimal(
        this.gaugeDisplayProps.getProperty("minFuelValueShown")));

    this.maxFuelValueShown = this.droneDataFactory.createFuel(new BigDecimal(
        this.gaugeDisplayProps.getProperty("maxFuelValueShown")));

    this.minSpeedValueShown = this.droneDataFactory.createSpeed(new BigDecimal(
        this.gaugeDisplayProps.getProperty("minSpeedValueShown")));

    this.maxSpeedValueShown = this.droneDataFactory.createSpeed(new BigDecimal(
        this.gaugeDisplayProps.getProperty("maxSpeedValueShown")));

    this.droneOfInterest = this.droneDataFactory.createDroneId(new BigInteger(
        this.gaugeDisplayProps.getProperty("droneOfInterest")));

    String missionOfInterestRep = this.gaugeDisplayProps
        .getProperty("missionOfInterest");
    System.out.println("%%% missionOfInterestRep " + missionOfInterestRep + " minSpeedValueShown=" + minSpeedValueShown);
    if (missionOfInterestRep == null) {
      this.missionOfInterest = this.droneDataFactory.createDroneMissionIdForAnyMission();
    } else {
      this.missionOfInterest = this.droneDataFactory
          .createDroneMissionId(new BigInteger(missionOfInterestRep));
    }
  }

  private void setupDroneDataFactory() {
    this.droneDataFactory = new DroneDataFactory();
  }

  protected void setupGaugeDisplayFactories() {
    this.gaugeDisplayFactory = new DisplayFactory();
    this.gaugeDisplayImplFactory = new DisplaySwingImplFactory();
//    this.gaugeDisplayActorFactory = new DisplayActorFactory();
  }

//  protected void setupGaugeViewActorFactory() {
//    this.gaugeViewActorFactory = new GaugeViewActorFactory();
//  }

  protected void setupGaugeViewsImpl() {
    this.fuelometerViewImpl = (IFuelometerViewSwingImpl) this.gaugeDisplayImplFactory
        .createFuelometerViewImpl("Fuelometer", "liter",
            this.minFuelValueShown, this.maxFuelValueShown);

    this.speedometerViewImpl = (ISpeedometerViewSwingImpl) this.gaugeDisplayImplFactory
        .createSpeedometerViewImpl("Speedometer", "km/h",
            this.minSpeedValueShown, this.maxSpeedValueShown);

    this.odometerViewImpl = (IOdometerViewSwingImpl) this.gaugeDisplayImplFactory
        .createOdometerViewImpl("Odometer", "km");

    this.locTrackerViewImpl = (ILocTrackerViewSwingImpl) this.gaugeDisplayImplFactory
        .createLocTrackerViewImpl("LocTracker");
  }

  protected void setupGaugeViews() {
    this.fuelometerView = this.gaugeDisplayFactory
        .createFuelometerView(this.fuelometerViewImpl);

    this.speedometerView = this.gaugeDisplayFactory
        .createSpeedometerView(this.speedometerViewImpl);

    this.odometerView = this.gaugeDisplayFactory
        .createOdometerView(this.odometerViewImpl);

    this.locTrackerView = this.gaugeDisplayFactory
        .createLocTrackerView(this.locTrackerViewImpl);
  }

  protected void setupGaugeDisplayImpl() {
    this.gaugeDisplayImpl = this.gaugeDisplayImplFactory
        .createGaugeDisplayImpl("Gauge Display 2013", this.fuelometerViewImpl,
            this.speedometerViewImpl, this.odometerViewImpl,
            this.locTrackerViewImpl);
  }

  protected void setupGaugeDisplay() {
    this.gaugeDisplay = this.gaugeDisplayFactory.createGaugeDisplay(this.gaugeDisplayImpl);
  }

//  protected void setupExternalActors() {
//    this.dataManagerActor = this.actorSystem.actorFor(this.actorSystemManager
//        .getDataManagerActorPath());
//  }

//  protected void setupGaugeViewsActors() {
//    this.setupGaugeViewActor("FuelometerView", this.fuelometerView,
//        GaugeKind.FUELOMETER);
//
//    this.setupGaugeViewActor("SpeedometerView", this.speedometerView,
//        GaugeKind.SPEEDOMETER);
//
//    this.setupGaugeViewActor("OdometerView", this.odometerView,
//        GaugeKind.ODOMETER);
//
//    this.setupGaugeViewActor("LocTrackerView", this.locTrackerView,
//        GaugeKind.LOCTRACKER);
//  }

//  protected void setupGaugeViewActor(final String gaugeViewName,
//      final IGaugeView gaugeView, final GaugeKind gaugeKind) {
//    /* Create the actor for the provided gauge view */
//    ActorRef gaugeViewActor = this.gaugeViewActorFactory.createGaugeViewActor(
//        String.format("%s%d", gaugeViewName, this.hashCode()),
//        this.actorSystem, gaugeView);
//
//    RequestRegisterGaugeReceiverMsg msg = RequestRegisterGaugeReceiverMsg
//        .create(
//            gaugeViewActor.path().toStringWithAddress(
//                this.actorSystemManager.getAddress()), this.droneOfInterest,
//            this.missionOfInterest, gaugeKind);
//
//    /*
//     * request to the data manager actor for the registration to the gauge
//     * updates associated to the drone droneOfInterest and the mission
//     * missionOfInterest.
//     */
//    this.dataManagerActor.tell(msg, gaugeViewActor);
//  }

//  protected void setupGaugeDisplayActor() {
//    /* The gaugeDisplayActor watches for the dataManagerActor availability */
//    this.gaugeDisplayActor = this.gaugeDisplayActorFactory
//        .createGaugeDisplayActor("GaugeDisplayActor", this.actorSystem,
//            this.dataManagerActor, this.gaugeDisplay);
//  }

  /* } */

  /* { Utility */

  public IGaugeDisplaySwingImpl getGaugeDisplayImpl() {
    return this.gaugeDisplayImpl;
  }

  public IGaugeDisplay getGaugeDisplay() {
    return this.gaugeDisplay;
  }

  /* } */

}
