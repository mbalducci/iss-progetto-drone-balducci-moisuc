package it.unibo.dronesystem.system.subsystems.display.displays;

//import akka.actor.ActorRef;
//import akka.actor.ActorSystem;
import it.unibo.dronesystem.data.dronedata.droneid.IDroneId;
import it.unibo.dronesystem.data.factories.DroneDataFactory;
import it.unibo.dronesystem.data.factories.GaugeValueFactory;
import it.unibo.dronesystem.data.factories.IDroneDataFactory;
import it.unibo.dronesystem.data.factories.IGaugeValueFactory;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;
import it.unibo.dronesystem.data.registering.RegisterCmdIssuerMsg;
import it.unibo.dronesystem.display.displays.ICmdDisplay;
import it.unibo.dronesystem.display.displays.implementors.swing.ICmdDisplaySwingImpl;
import it.unibo.dronesystem.display.factories.*;
//import it.unibo.dronesystem.system.utils.ActorSystemManager;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Properties;

/**
 * Project: dronesystem
 */
public class CmdDisplayActivator {

//  private ActorSystemManager       actorSystemManager;
//  private ActorSystem              actorSystem;
//
//  private ActorRef                 cmdServerActor;
//  private ActorRef                 cmdDisplayActor;

  private IGaugeValueFactory       gaugeValueFactory;
  private IDroneDataFactory        droneDataFactory;
  private IDisplayFactory          cmdDisplayFactory;
  private IDisplaySwingImplFactory cmdDisplayImplFactory;
  private IDisplayActorFactory     cmdDisplayActorFactory;

  private Properties               cmdDisplayProps;
  private ISpeed                   ds;

  private ICmdDisplay              cmdDisplay;
  private ICmdDisplaySwingImpl     cmdDisplayImpl;

  private IDroneId                 droneOfInterest;

  /* { Constructors and factories */

  protected CmdDisplayActivator( )
      throws Exception {
    super();

    this.setupGaugeValueFactory();
    this.setupDroneDataFactory();
    this.setupCmdDisplayFactories();

    this.setupCmdDisplayProps();

//    this.actorSystemManager = actorSystemManager;
//    this.actorSystem = this.actorSystemManager.getActorSystem();

//    this.setupExternalActors();
//    this.setupCmdDisplayActor();

    this.setupCmdDisplayImpl();
    this.setupCmdDisplay();
  }

  public static CmdDisplayActivator create( ) throws Exception {
    return new CmdDisplayActivator( );
  }

  /* } */

  /* { Setup */

  protected void setupGaugeValueFactory() {
    this.gaugeValueFactory = new GaugeValueFactory();
  }

  protected void setupDroneDataFactory() {
    this.droneDataFactory = new DroneDataFactory();
  }

  protected void setupCmdDisplayFactories() {
    this.cmdDisplayFactory = new DisplayFactory();
    this.cmdDisplayImplFactory = new DisplaySwingImplFactory();
//    this.cmdDisplayActorFactory = new DisplayActorFactory();
  }

  protected void setupCmdDisplayProps() throws Exception {
    this.cmdDisplayProps = new Properties();

    InputStream propsStream = this.getClass().getResourceAsStream(
        "/it/unibo/dronesystem/display/cmddisplay.properties");
    this.cmdDisplayProps.load(propsStream);

    this.ds = this.gaugeValueFactory.createSpeed(new BigDecimal(
        this.cmdDisplayProps.getProperty("ds")));

    this.droneOfInterest = this.droneDataFactory.createDroneId(new BigInteger(
        this.cmdDisplayProps.getProperty("droneOfInterest")));
  }

//  protected void setupExternalActors() {
//    this.cmdServerActor = this.actorSystem.actorFor(this.actorSystemManager
//        .getCmdServerActorPath());
//  }
//
//  protected void setupCmdDisplayActor() {
//    this.cmdDisplayActor = this.cmdDisplayActorFactory.createCmdDisplayActor(
//        "CmdDisplayActor", this.actorSystem, this.cmdServerActor);
//
//    String cmdDisplayActorPath = this.cmdDisplayActor.path()
//        .toStringWithAddress(this.actorSystemManager.getAddress());
//
//    /*
//     * Register the cmdDisplayActor (that is a command issuer) tp the command
//     * server to issue commands for the drone with id droneOfInterest.
//     */
//    this.cmdServerActor.tell(
//        RegisterCmdIssuerMsg.create(cmdDisplayActorPath, this.droneOfInterest),
//        this.cmdDisplayActor);
//  }

  protected void setupCmdDisplayImpl() {
    this.cmdDisplayImpl = this.cmdDisplayImplFactory
        .createCmdDisplayImpl("Command Display");
  }

  protected void setupCmdDisplay() {
//    this.cmdDisplay = this.cmdDisplayFactory.createCmdDisplay(
//        this.cmdDisplayActor, this.cmdDisplayImpl, this.ds,
//        this.gaugeValueFactory);
  }

  /* } */

  /* { Utility */

  public ICmdDisplay getCmdDisplay() {
    return this.cmdDisplay;
  }

  public ICmdDisplaySwingImpl getCmdDisplayImpl() {
    return this.cmdDisplayImpl;
  }

  /* } */

}
