package it.unibo.dronesystem.system.subsystems.display;
 
//import akka.actor.ActorSystem;
import it.unibo.dronesystem.display.dashboards.IDroneCtrlDash;
import it.unibo.dronesystem.display.dashboards.implementors.swing.IDroneCtrlDashSwingImpl;
import it.unibo.dronesystem.display.displays.IGaugeDisplay;
import it.unibo.dronesystem.display.displays.implementors.swing.IGaugeDisplaySwingImpl;
import it.unibo.dronesystem.display.factories.DisplayFactory;
import it.unibo.dronesystem.display.factories.DisplaySwingImplFactory;
import it.unibo.dronesystem.display.factories.IDisplaySwingImplFactory;
import it.unibo.dronesystem.system.subsystems.ISubsystemActivator;
import it.unibo.dronesystem.system.subsystems.SubsystemActivator;
import it.unibo.dronesystem.system.subsystems.display.displays.CmdDisplayActivator;
import it.unibo.dronesystem.system.subsystems.display.displays.GaugeDisplayActivator;
//import it.unibo.dronesystem.system.utils.ActorSystemManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Project: dronesystem
 * <p/>
 * The drone control dashboard (of the display subsystem) activator
 */
public class DroneCtrlDashActivator extends SubsystemActivator implements
    ISubsystemActivator {

//  private ActorSystemManager       actorSystemManager;
//  private ActorSystem              actorSystem;

  private IDroneCtrlDash           droneCtrlDash;

  private GaugeDisplayActivator    gaugeDisplaySubsystem;
  private CmdDisplayActivator      cmdDisplaySubsystem;

  private DisplayFactory           displayFactory;
  private IDisplaySwingImplFactory displayImplFactory;

  /* { Constructors and factories */

  protected DroneCtrlDashActivator() throws Exception {
    super();
  }

  public static DroneCtrlDashActivator create() throws Exception {
    return new DroneCtrlDashActivator();
  }

  /* } */

  /* { {SubsystemActivator} implementation */

  /**
   * @see SubsystemActivator#getDependencies()
   */
  @Override
  protected Map<String, ISubsystemActivator> getDependencies() throws Exception {
    return new HashMap<String, ISubsystemActivator>();
  }

  /**
   * @see SubsystemActivator#startSubsystem()
   */
  @Override
  protected void startSubsystem() throws Exception {
    this.setupFactories();
//    this.setupActorSystem();
     this.setupGaugeDisplay();
//    this.setupCmdDisplay();
    this.setupDroneCtrlDash();   
    this.droneCtrlDash.start();
  }

  protected void setupDroneCtrlDash() {
	  System.out.println("%%% DroneCtrlDashActivator this.gaugeDisplaySubsystem.getGaugeDisplayImpl() " + 
			  this.gaugeDisplaySubsystem.getGaugeDisplayImpl());
//	  System.out.println("this.cmdDisplaySubsystem.getCmdDisplayImpl() " + 
//			  this.cmdDisplaySubsystem.getCmdDisplayImpl());
	  System.out.println("%%% DroneCtrlDashActivator this.gaugeDisplaySubsystem.getGaugeDisplay() " + 
			  this.gaugeDisplaySubsystem.getGaugeDisplay());
//	  System.out.println("this.cmdDisplaySubsystem.getCmdDisplay() " + 
//			  this.cmdDisplaySubsystem.getCmdDisplay());
	  
	  IGaugeDisplaySwingImpl gaugeDisplayImpl = this.gaugeDisplaySubsystem.getGaugeDisplayImpl();
	  IGaugeDisplay gaugeDisplay = this.gaugeDisplaySubsystem.getGaugeDisplay();
	  
	  IDroneCtrlDashSwingImpl droneCtrlDashB = this.displayImplFactory.createDroneCtrlDashImpl(
	            "Smart Device Dashboard",
	            gaugeDisplayImpl,
	            null); //this.cmdDisplaySubsystem.getCmdDisplayImpl()),

	   this.droneCtrlDash = this.displayFactory.createDroneCtrlDash(
			    droneCtrlDashB,
			    gaugeDisplay, 
		        null //this.cmdDisplaySubsystem.getCmdDisplay()
		        );
  }

  /**
   * @see SubsystemActivator#getDependencies()
   */
  @Override
  protected void stopSubsystem() throws Exception {
    this.droneCtrlDash.stop();
  }

  /* } */

  /* { Setup & Start facilities */

  protected void setupFactories() {
    this.displayFactory = new DisplayFactory();
    this.displayImplFactory = new DisplaySwingImplFactory();
  }

//  protected void setupActorSystem() throws Exception {
//    this.actorSystemManager = ActorSystemManager
//        .create("dronecontroldashboard");
//    this.actorSystem = this.actorSystemManager.getActorSystem();
//
//    this.actorSystemManager.listenForUnhandledMessages();
//  }
//
  protected void setupGaugeDisplay() throws Exception {
    this.gaugeDisplaySubsystem = GaugeDisplayActivator.create();
  }
//
//  protected void setupCmdDisplay() throws Exception {
//    this.cmdDisplaySubsystem = CmdDisplayActivator
//        .create(this.actorSystemManager);
//  }

}
