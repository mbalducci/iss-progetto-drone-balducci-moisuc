package it.unibo.dronesystem.display.dashboards;

import it.unibo.dronesystem.display.dashboards.implementors.IDroneCtrlDashImpl;
import it.unibo.dronesystem.display.displays.ICmdDisplay;
import it.unibo.dronesystem.display.displays.IGaugeDisplay;

/**
 * Project: dronesystem
 * <p/>
 * A {IDroneCtrlDash} implementation.
 */
@SuppressWarnings("FieldCanBeLocal")
public class DroneCtrlDash implements IDroneCtrlDash {

  private final IDroneCtrlDashImpl implementor;

  private final IGaugeDisplay      gaugeDisplay;
  private final ICmdDisplay        cmdDisplay;

  /* { Constructors and factories */

  protected DroneCtrlDash(final IDroneCtrlDashImpl implementor,
      final IGaugeDisplay gaugeDisplay, final ICmdDisplay cmdDisplay) {

    this.implementor = implementor;

    this.cmdDisplay = cmdDisplay;
    this.gaugeDisplay = gaugeDisplay;
  }

  public static DroneCtrlDash create(final IDroneCtrlDashImpl implementor,
      final IGaugeDisplay gaugeDisplay, final ICmdDisplay cmdDisplay) {

    return new DroneCtrlDash(implementor, gaugeDisplay, cmdDisplay);
  }

  /* } */

  /* { {IDroneCtrlDash} implementation */

  /**
   * @see IDroneCtrlDash#start()
   */
  @Override
  public void start() {
    this.implementor.start();
  }

  /**
   * @see IDroneCtrlDash#stop()
   */
  @Override
  public void stop() {
    this.implementor.stop();
  }

  /* } */

}
