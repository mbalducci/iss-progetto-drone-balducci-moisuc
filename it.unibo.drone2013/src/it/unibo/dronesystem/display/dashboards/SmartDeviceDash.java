package it.unibo.dronesystem.display.dashboards;

import it.unibo.dronesystem.display.dashboards.implementors.ISmartDeviceDashImpl;
import it.unibo.dronesystem.display.displays.IGaugeDisplay;

/**
 * Project: dronesystem
 * <p/>
 * A {ISmartDeviceDash} implementation.
 */
public class SmartDeviceDash implements ISmartDeviceDash {

  private final IGaugeDisplay        gaugeDisplay;

  private final ISmartDeviceDashImpl implementor;

  /* { Constructors and factories */

  protected SmartDeviceDash(ISmartDeviceDashImpl implementor,
      IGaugeDisplay gaugeDisplay) {
    this.implementor = implementor;
    this.gaugeDisplay = gaugeDisplay;
  }

  public static SmartDeviceDash create(ISmartDeviceDashImpl implementor,
      IGaugeDisplay gaugeDisplay) {

    return new SmartDeviceDash(implementor, gaugeDisplay);
  }

  /* } */

  /* { {ISmartDeviceDash} implementation */

  /**
   * @see ISmartDeviceDash#start()
   */
  @Override
  public void start() {
    this.implementor.start();
  }

  /**
   * @see ISmartDeviceDash#stop()
   */
  @Override
  public void stop() {
    this.implementor.stop();
  }

  /* } */

}
