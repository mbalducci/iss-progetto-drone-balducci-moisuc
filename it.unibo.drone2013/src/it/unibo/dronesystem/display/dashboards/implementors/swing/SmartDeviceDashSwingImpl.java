package it.unibo.dronesystem.display.dashboards.implementors.swing;

import it.unibo.dronesystem.data.factories.GaugeValueFactory;
import it.unibo.dronesystem.display.displays.implementors.swing.GaugeDisplaySwingImpl;
import it.unibo.dronesystem.display.displays.implementors.swing.IGaugeDisplaySwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.FuelometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.LocTrackerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.OdometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.SpeedometerViewSwingImpl;

import java.math.BigDecimal;

/**
 * Project: dronesystem
 * <p/>
 * A {ISmartDeviceDashSwingImpl} implementation
 */
public class SmartDeviceDashSwingImpl extends DashSwing implements
    ISmartDeviceDashSwingImpl {

  /* { Constructors and factories */

  public SmartDeviceDashSwingImpl(final String name,
      final IGaugeDisplaySwingImpl gaugeDisplay) {

    super(name);

    this.addComponent(gaugeDisplay.getMainPanel(), 0, 0, 1, 1, 1.0, 1.0);
  }

  public static SmartDeviceDashSwingImpl create(final String name,
      final IGaugeDisplaySwingImpl gaugeDisplay) {

    return new SmartDeviceDashSwingImpl(name, gaugeDisplay);
  }

  /* } */

  /* { {ISmartDeviceDashSwingImpl} implementation */

  /**
   * @see ISmartDeviceDashSwingImpl#start()
   */
  @Override
  public void start() {
    super.start();
  }

  /**
   * @see ISmartDeviceDashSwingImpl#stop()
   */
  @Override
  public void stop() {
    super.stop();
  }

  /* } */

  /* { Usage example */

  public static void main(String[] args) {
    GaugeValueFactory gaugeValueFactory = new GaugeValueFactory();

    SpeedometerViewSwingImpl speedometerView = SpeedometerViewSwingImpl.create(
        "Speedometer", "kmh",
        gaugeValueFactory.createSpeed(BigDecimal.valueOf(2.0)),
        gaugeValueFactory.createSpeed(BigDecimal.valueOf(120.0)));

    FuelometerViewSwingImpl fuelometerView = FuelometerViewSwingImpl.create(
        "Fuelometer", "l",
        gaugeValueFactory.createFuel(BigDecimal.valueOf(30)),
        gaugeValueFactory.createFuel(BigDecimal.valueOf(220)));

    OdometerViewSwingImpl odometerView = OdometerViewSwingImpl.create(
        "Odometer", "km");

    LocTrackerViewSwingImpl locTrackerView = LocTrackerViewSwingImpl
        .create("LocTracker");

    GaugeDisplaySwingImpl gaugeDisplay = GaugeDisplaySwingImpl.create(
        "Gauge Display", fuelometerView, speedometerView, odometerView,
        locTrackerView);

    SmartDeviceDashSwingImpl smartDeviceDash = SmartDeviceDashSwingImpl.create(
        "Smart Device Dashboard", gaugeDisplay);

    smartDeviceDash.start();
  }

  /* } */

}
