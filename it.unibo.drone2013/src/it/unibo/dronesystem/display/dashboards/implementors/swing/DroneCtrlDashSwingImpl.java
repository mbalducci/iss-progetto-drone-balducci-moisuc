package it.unibo.dronesystem.display.dashboards.implementors.swing;

import it.unibo.dronesystem.config.DroneConfig;
import it.unibo.dronesystem.data.factories.GaugeValueFactory;
import it.unibo.dronesystem.display.displays.implementors.swing.CmdDisplaySwingImpl;
import it.unibo.dronesystem.display.displays.implementors.swing.GaugeDisplaySwingImpl;
import it.unibo.dronesystem.display.displays.implementors.swing.ICmdDisplaySwingImpl;
import it.unibo.dronesystem.display.displays.implementors.swing.IGaugeDisplaySwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.FuelometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.LocTrackerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.OdometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.SpeedometerViewSwingImpl;

import java.math.BigDecimal;

/**
 * Project: dronesystem
 * <p/>
 * A {IDroneCtrlDashSwingImpl} implementation
 */
public class DroneCtrlDashSwingImpl extends DashSwing implements
    IDroneCtrlDashSwingImpl {

  /* { Constructors and factories */

  protected DroneCtrlDashSwingImpl(final String name,
      final IGaugeDisplaySwingImpl gaugeDisplay,
      final ICmdDisplaySwingImpl cmdDisplay) {

    super(name);

//int rowIdx, int colIdx, int rowCount, int colCount, double horizontalWeight, double verticalWeight
    this.addComponent(gaugeDisplay.getMainPanel(), 0, 0, 1, 1, 4.0, 4.0);
    this.addComponent(cmdDisplay.getMainPanel(), 1, 0, 1, 1, 1.0, 1.0);
    this.frame.setSize(DroneConfig.windowWidth, DroneConfig.windowHeight); //by AN
  }

  public static DroneCtrlDashSwingImpl create(final String name,
      final IGaugeDisplaySwingImpl gaugeDisplay,
      final ICmdDisplaySwingImpl cmdDisplay) {
	  System.out.println("%%% DroneCtrlDashSwingImpl"  );
    return new DroneCtrlDashSwingImpl(name, gaugeDisplay, cmdDisplay);
  }

  /* } */

  /* { {IDroneControlDashSwingImpl} implementation */

  /**
   * @see IDroneCtrlDashSwingImpl#start()
   */
  @Override
  public void start() {
    super.start();
  }

  /**
   * @see IDroneCtrlDashSwingImpl#stop()
   */
  @Override
  public void stop() {
    super.stop();
  }

  /* } */

  /* { Usage example */

  public static void main(String[] args) {
    GaugeValueFactory gaugeValueFactory = new GaugeValueFactory();

    CmdDisplaySwingImpl cmdDisplay = CmdDisplaySwingImpl.create("CmdDisplay");

    SpeedometerViewSwingImpl speedometerView = SpeedometerViewSwingImpl.create(
        "Speedometer", "kmh",
        gaugeValueFactory.createSpeed(BigDecimal.valueOf(2.0)),
        gaugeValueFactory.createSpeed(BigDecimal.valueOf(120.0)));

    FuelometerViewSwingImpl fuelometerView = FuelometerViewSwingImpl.create(
        "Fuelometer", "l",
        gaugeValueFactory.createFuel(BigDecimal.valueOf(30)),
        gaugeValueFactory.createFuel(BigDecimal.valueOf(220)));

    OdometerViewSwingImpl odometerView = OdometerViewSwingImpl.create(
        "Odometer", "km");

    LocTrackerViewSwingImpl locTrackerView = LocTrackerViewSwingImpl
        .create("LocTracker");

    GaugeDisplaySwingImpl gaugeDisplay = GaugeDisplaySwingImpl.create(
        "Gauge Display", fuelometerView, speedometerView, odometerView,
        locTrackerView);

    DroneCtrlDashSwingImpl droneCtrlDash = DroneCtrlDashSwingImpl.create(
        "Drone Control Dashboard", gaugeDisplay, cmdDisplay);

    droneCtrlDash.start();
  }

  /* } */

}
