package it.unibo.dronesystem.display.views.implementors.swing;

import eu.hansolo.steelseries.gauges.LinearBargraph;	//from  SteelSeries-3.9.30.jar
import eu.hansolo.steelseries.tools.BackgroundColor;
import eu.hansolo.steelseries.tools.FrameDesign;
import eu.hansolo.steelseries.tools.FrameEffect;
import eu.hansolo.steelseries.tools.LcdColor;
import it.unibo.dronesystem.config.DroneConfig;
import it.unibo.dronesystem.data.gauge.fuel.IFuel;
import it.unibo.dronesystem.display.views.implementors.IFuelometerViewImpl;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.math.BigInteger;

/**
 * Project: dronesystem
 * <p/>
 * A {IFuelometerViewSwingImpl} implementation.
 */
@SuppressWarnings("FieldCanBeLocal")
public class FuelometerViewSwingImpl extends GaugeViewSwingImpl implements
    IFuelometerViewSwingImpl {

  protected String               unitName;

  protected final LinearBargraph valueField;

  private final BigInteger       minValueFieldWidth;
  private final BigInteger       minValueFieldHeight;

  private final IFuel            minValueShown;
  private final IFuel            maxValueShown;

  /* { Constructors and factories */

  protected FuelometerViewSwingImpl(final String name, final String unitName,
      final IFuel minValueShown, final IFuel maxValueShown) {
    super(name);

    this.unitName = unitName;

    this.minValueFieldWidth = BigInteger.valueOf(DroneConfig.fuleometerViewWidth);
    this.minValueFieldHeight = BigInteger.valueOf(DroneConfig.fuelometerViewHeight);

    this.minValueShown = minValueShown;
    this.maxValueShown = maxValueShown;

    this.valueField = new LinearBargraph();
    this.setupValueField();
  }

  public static FuelometerViewSwingImpl create(final String name,
      final String unitName, final IFuel minValueShown,
      final IFuel maxValueShown) {

    return new FuelometerViewSwingImpl(name, unitName, minValueShown,
        maxValueShown);
  }

  /* } */

  /* { Setup */

  protected void setupValueField() {
    this.valueField.init(this.minValueFieldWidth.intValue(),
        this.minValueFieldHeight.intValue());

    this.valueField.setMinValue(this.minValueShown.getFloatRep().doubleValue());
    this.valueField.setMaxValue(this.maxValueShown.getFloatRep().doubleValue());

    this.valueField.setTitle(this.getName());
    this.valueField.setUnitString(this.unitName);

    this.valueField.setDigitalFont(true);
    this.valueField.setLcdColor(LcdColor.SECTIONS_LCD);
    this.valueField.setFrameEffect(FrameEffect.EFFECT_INNER_FRAME);
    this.valueField.setFrameDesign(FrameDesign.GLOSSY_METAL);
    this.valueField.setBackgroundColor(BackgroundColor.PUNCHED_SHEET);

    FlowLayout mainPanelLayout = new FlowLayout();
    mainPanelLayout.setAlignment(FlowLayout.CENTER);
    this.getMainPanel().setLayout(mainPanelLayout);
    this.getMainPanel().add(this.valueField);

    this.valueField.setPreferredSize(new Dimension(this.minValueFieldWidth
        .intValue(), this.minValueFieldHeight.intValue()));
  }

  /* } */

  /* { {IFuelometerViewImpl} implementation */

  /**
   * @see IFuelometerViewImpl#update(String)
   */
  @Override
  public void update(final String value) {
    EventQueue.invokeLater(new Runnable() {

      @Override
      public void run() {
        FuelometerViewSwingImpl.this.valueField.setValueAnimated(Double
            .parseDouble(value));
      }
    });
  }

  /* { {GaugeViewSwingImpl} implementation */

  /**
   * @see GaugeViewSwingImpl#getFavor()
   */
  @Override
  public ViewSwingFavor getFavor() {
    return ViewSwingFavor.NEUTRAL;
  }

  /* } */

}
