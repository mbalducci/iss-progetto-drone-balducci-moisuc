package it.unibo.dronesystem.display.views.implementors.swing;

import eu.hansolo.steelseries.gauges.Linear;
import eu.hansolo.steelseries.tools.BackgroundColor;
import eu.hansolo.steelseries.tools.FrameDesign;
import eu.hansolo.steelseries.tools.FrameEffect;
import eu.hansolo.steelseries.tools.LcdColor;

import it.unibo.dronesystem.config.DroneConfig;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.math.BigInteger;

/**
 * Project: dronesystem
 * <p/>
 * A {IOdometerViewSwingImpl} implementation.
 */
@SuppressWarnings("FieldCanBeLocal")
public class OdometerViewSwingImpl extends GaugeViewSwingImpl implements
    IOdometerViewSwingImpl {
  private final String     unitName;
  private final Linear     valueField;
  private final BigInteger minValueFieldWidth;
  private final BigInteger minValueFieldHeight;

  /* { Constructors and factories */

  protected OdometerViewSwingImpl(final String name, final String unitName) {
    super(name);

    this.unitName = unitName;

    this.minValueFieldWidth = BigInteger.valueOf(DroneConfig.odometerViewWidth);
    this.minValueFieldHeight = BigInteger.valueOf(DroneConfig.odometerViewHeight);

    this.valueField = new Linear();
    this.setupValueField();
  }

  public static OdometerViewSwingImpl create(final String name,
      final String unitName) {

    return new OdometerViewSwingImpl(name, unitName);
  }

  /* } */

  /* { Setup */

  protected void setupValueField() {
    this.valueField.init(this.minValueFieldWidth.intValue(),
        this.minValueFieldHeight.intValue());

    this.valueField.setTitle(this.getName());
    this.valueField.setUnitString(this.unitName);

    this.valueField.setMinValue(0.0);
    this.valueField.setMaxValue(1.6);

    this.valueField.setLcdDecimals(8);
    this.valueField.setDigitalFont(true);
    this.valueField.setLcdColor(LcdColor.SECTIONS_LCD);
    this.valueField.setFrameEffect(FrameEffect.EFFECT_INNER_FRAME);
    this.valueField.setFrameDesign(FrameDesign.GLOSSY_METAL);
    this.valueField.setBackgroundColor(BackgroundColor.PUNCHED_SHEET);

    FlowLayout mainPanelLayout = new FlowLayout();
    mainPanelLayout.setAlignment(FlowLayout.CENTER);
    this.getMainPanel().setLayout(mainPanelLayout);
    this.getMainPanel().add(this.valueField);

    this.valueField.setPreferredSize(new Dimension(this.minValueFieldWidth
        .intValue(), this.minValueFieldHeight.intValue()));
  }

  /* } */

  /* { {IOdometerViewSwingImpl} implementation */

  /**
   * @see IOdometerViewSwingImpl#update(String)
   */
  @Override
  public void update(final String value) {
    EventQueue.invokeLater(new Runnable() {

      @Override
      public void run() {
        OdometerViewSwingImpl.this.valueField.setValueAnimated(Double
            .parseDouble(value));
      }
    });
  }

  /* } */

  /* { {GaugeViewSwingImpl} implementation */

  /**
   * @see GaugeViewSwingImpl#getFavor()
   */
  @Override
  public ViewSwingFavor getFavor() {
    return ViewSwingFavor.HORIZONTAL;
  }

  /* } */

}
