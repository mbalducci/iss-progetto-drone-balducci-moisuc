package it.unibo.dronesystem.display.views.implementors.swing;

import it.unibo.dronesystem.data.factories.GaugeValueFactory;
import it.unibo.dronesystem.data.factories.IGaugeValueFactory;
import it.unibo.dronesystem.data.gauge.geo.IGeo;
import it.unibo.dronesystem.data.gauge.geo.IGeoCoordinate;
import org.jdesktop.swingx.JXMapKit;	//from swingx-ws-1.0.jar
import org.jdesktop.swingx.JXMapKit.DefaultProviders;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.Waypoint;
import org.jdesktop.swingx.mapviewer.WaypointPainter;
import org.jdesktop.swingx.mapviewer.WaypointRenderer;

import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Project: dronesystem
 * <p/>
 * A {ILocTrackerViewSwingImpl} implementation.
 */
public class LocTrackerViewSwingImpl extends GaugeViewSwingImpl implements
    ILocTrackerViewSwingImpl {

  private final JXMapKit       mapField;
  private final List<Waypoint> waypoints;

  private final JButton        goToStartPosField;
  private final JButton        goToLastPosField;

  private final Pattern        decimalRegex;
  private final Pattern        dmsRegex;

  /* { Constructors and factories */

  protected LocTrackerViewSwingImpl(final String name) {
    super(name);
    this.decimalRegex = Pattern.compile("\\[(.+),(.+)\\]");
    this.dmsRegex = Pattern
        .compile("\\[\\[(.+),(.+),(.+)\\],\\[(.+),(.+),(.+)\\]\\]");

    this.mapField = new JXMapKit();
    this.waypoints = new ArrayList<Waypoint>();

    this.goToStartPosField = new JButton();
    this.goToLastPosField = new JButton();

    this.setupMapField();
    this.setupGoToLastPosField();
    this.setupGoToStartPosField();
    this.setupMainPanel();
  }

  public static LocTrackerViewSwingImpl create(final String name) {
    return new LocTrackerViewSwingImpl(name);
  }

  /* } */

  /* { Setup */

  protected void setupMapField() {
    this.mapField.setDefaultProvider(DefaultProviders.OpenStreetMaps);
    this.mapField.setZoomSliderVisible(true);
    this.mapField.setZoomButtonsVisible(true);
    this.mapField.setMiniMapVisible(true);
    this.mapField.setAddressLocationShown(true);
  }

  protected void setupGoToStartPosField() {
    this.goToStartPosField.setEnabled(false);
    this.goToStartPosField.setText("Go To Starting Position");
    this.goToStartPosField.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent actionEvent) {
        if (!(waypoints.isEmpty())) {
          goToPos(waypoints.get(0).getPosition());
        }
      }
    });
  }

  protected void setupGoToLastPosField() {
    this.goToLastPosField.setEnabled(false);
    this.goToLastPosField.setText("Go To Latest Position");
    this.goToLastPosField.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent actionEvent) {
        if (!(waypoints.isEmpty())) {
          goToPos(waypoints.get(waypoints.size() - 1).getPosition());
        }
      }
    });
  }

  protected void setupMainPanel() {
    this.addComponent(this.getMainPanel(), this.mapField, 0, 0, 1, 2, 1.0, 1.0);
    this.addComponent(this.getMainPanel(), this.goToStartPosField, 1, 0, 1, 1,
        1.0, 0.2);
    this.addComponent(this.getMainPanel(), this.goToLastPosField, 1, 1, 1, 1,
        1.0, 0.2);
  }

  /* } */

  /* { {ILocTrackerViewSwingImpl} implementation */

  /**
   * @see ILocTrackerViewSwingImpl#update(String)
   */
  @Override
  public void update(final String value) {
    GeoPosition pos = this.stringToGeoPos(value);
    if (pos != null) {
      this.setPos(pos);
      this.goToPos(pos); // added by Mattia
    }
  }

  /* } */

  /* { {GaugeViewSwingImpl} implementation */

  /**
   * @see GaugeViewSwingImpl#getFavor()
   */
  @Override
  public ViewSwingFavor getFavor() {
    return ViewSwingFavor.NEUTRAL;
  }

  /* } */

  /* { Utility */

  /**
   * Converts the {String} representation of a geographic value to the
   * {GeoPosition} representation.
   * 
   * @param value
   *          The String representation of the geographic value.
   * @return The GeoPosition representation of the geographic value.
   *         On invalid value it returns null.
   */
  protected GeoPosition stringToGeoPos(final String value) {
    GeoPosition position = null;

    /* Convert assuming value is in decimal notation */
    try {
      Matcher match = this.decimalRegex.matcher(value);
      if (match.matches()) {
        position = new GeoPosition(
            new BigDecimal(match.group(1)).doubleValue(), new BigDecimal(
                match.group(2)).doubleValue());
      }
    } catch (NumberFormatException ignored) {
    }
    
    /* Convert assuming value is in dms notation */
    if (position == null) {
      IGaugeValueFactory gaugeValueFactory = new GaugeValueFactory();
      try {
        Matcher match = this.dmsRegex.matcher(value);
        if (match.matches()) {
          IGeoCoordinate latitude = gaugeValueFactory.createGeoCoordinate(
              new BigInteger(match.group(1)), new BigInteger(match.group(2)),
              new BigInteger(match.group(3)));
          IGeoCoordinate longitude = gaugeValueFactory.createGeoCoordinate(
              new BigInteger(match.group(4)), new BigInteger(match.group(5)),
              new BigInteger(match.group(6)));

          IGeo geo = gaugeValueFactory.createGeo(latitude, longitude);

          position = new GeoPosition(geo.getLatitude().getDecimalFloatRep()
              .doubleValue(), geo.getLongitude().getDecimalFloatRep()
              .doubleValue());
        }
      } catch (NumberFormatException ignored) {
      }
    }
    System.out.println("Geopos: "+position);
    return position;
  }

  /**
   * Set the position in the mapField to the provided one.
   * It also unlocks the features available only when there is a valid
   * position in the mapField.
   * 
   * @param pos
   *          The new position available for the mapField.
   */
  protected void setPos(final GeoPosition pos) {
    this.addWaypoint(pos);

    this.goToStartPosField.setVisible(true);
    this.goToStartPosField.setEnabled(true);

    this.goToLastPosField.setVisible(true);
    this.goToLastPosField.setEnabled(true);
  }

  protected void goToPos(final GeoPosition pos) {
    EventQueue.invokeLater(new Runnable() {

      @Override
      public void run() {
        LocTrackerViewSwingImpl.this.mapField.setAddressLocation(pos);
      }
    });
  }

  protected void addWaypoint(final GeoPosition pos) {
    this.waypoints.add(new Waypoint(pos));

    WaypointPainter painter = new WaypointPainter();
    painter.setWaypoints(new HashSet<Waypoint>(this.waypoints));
    painter.setRenderer(new WaypointRenderer() {

      @Override
      public boolean paintWaypoint(final Graphics2D g, final JXMapViewer map,
          final Waypoint waypoint) {
        g.setColor(Color.RED);
        g.drawLine(-5, -5, +5, +5);
        g.drawLine(-5, +5, +5, -5);
        return true;
      }
    });

    this.mapField.getMainMap().setOverlayPainter(painter);
  }

  /* } */

  /* { Usage example */

  public static void main(String[] args) {
    LocTrackerViewSwingImpl locTrackerView = LocTrackerViewSwingImpl
        .create("LocTracker View");

    JFrame frame = new JFrame("Frame for CmdDisplay Example");
    frame.setContentPane(locTrackerView.getMainPanel());
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);

    locTrackerView.setPos(new GeoPosition(20.123, 30.123));
    locTrackerView.setPos(new GeoPosition(30.23, 40.123));
    locTrackerView.setPos(new GeoPosition(-70.123, 10.123));
    locTrackerView.setPos(new GeoPosition(1.123, -80.123));
  }

  /* } */

}
