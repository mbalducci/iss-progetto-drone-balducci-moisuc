package it.unibo.dronesystem.display.views.implementors.swing;

import eu.hansolo.steelseries.gauges.Radial; //requires trident at run time
import eu.hansolo.steelseries.tools.BackgroundColor;
import eu.hansolo.steelseries.tools.FrameDesign;
import eu.hansolo.steelseries.tools.FrameEffect;
import eu.hansolo.steelseries.tools.LcdColor;
import it.unibo.dronesystem.config.DroneConfig;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Project: dronesystem
 * <p/>
 * A {ISpeedometerViewSwingImpl} implementation.
 */
@SuppressWarnings("FieldCanBeLocal")
public class SpeedometerViewSwingImpl extends GaugeViewSwingImpl implements
    ISpeedometerViewSwingImpl {

  private final String     unitName;

  protected final Radial   valueField;

  private final BigInteger minValueFieldWidth;
  private final BigInteger minValueFieldHeight;

  private final ISpeed     minValueShown;
  private final ISpeed     maxValueShown;

  /* { Constructors and factories */

  protected SpeedometerViewSwingImpl(final String name, final String unitName,
      final ISpeed minValueShown, final ISpeed maxValueShown) {
    super(name);

    this.unitName = unitName;

    this.minValueFieldWidth = BigInteger.valueOf(DroneConfig.speedometerViewWidth);
    this.minValueFieldHeight = BigInteger.valueOf(DroneConfig.speedometerViewHeight);

    this.minValueShown = minValueShown;
    this.maxValueShown = maxValueShown;

    this.valueField = new Radial();
    this.setupValueField();
  }

  public static SpeedometerViewSwingImpl create(final String name,
      final String unitName, final ISpeed minValueShown,
      final ISpeed maxValueShown) {

    return new SpeedometerViewSwingImpl(name, unitName, minValueShown,
        maxValueShown);
  }

  /* } */

  /* { Setup */

  protected void setupValueField() {
    this.valueField.init(this.minValueFieldWidth.intValue(),
        this.minValueFieldHeight.intValue());

    this.valueField.setMinValue(this.minValueShown.getFloatRep().doubleValue());
    this.valueField.setMaxValue(this.maxValueShown.getFloatRep().doubleValue());

    this.valueField.setTitle(this.getName());
    this.valueField.setUnitString(this.unitName);

    this.valueField.setTrackVisible(true);
    this.valueField.setMinMeasuredValueVisible(true);
    this.valueField.setMaxMeasuredValueVisible(true);

    this.valueField.setTrackStart((this.maxValueShown.getFloatRep()
        .subtract(this.minValueShown.getFloatRep())).multiply(
        BigDecimal.valueOf(0.7)).doubleValue());
    this.valueField
        .setTrackStop(this.maxValueShown.getFloatRep().doubleValue());

    this.valueField.setDigitalFont(true);
    this.valueField.setLcdColor(LcdColor.SECTIONS_LCD);
    this.valueField.setFrameEffect(FrameEffect.EFFECT_INNER_FRAME);
    this.valueField.setFrameDesign(FrameDesign.GLOSSY_METAL);
    this.valueField.setBackgroundColor(BackgroundColor.PUNCHED_SHEET);

    FlowLayout mainPanelLayout = new FlowLayout();
    mainPanelLayout.setAlignment(FlowLayout.CENTER);
    this.getMainPanel().setLayout(mainPanelLayout);
    this.getMainPanel().add(this.valueField);

    this.valueField.setPreferredSize(new Dimension(this.minValueFieldWidth
        .intValue(), this.minValueFieldHeight.intValue()));
  }

  /* { {ISpeedometerViewSwingImpl} implementation */

  /**
   * @see ISpeedometerViewSwingImpl#update(String)
   */
  @Override
  public void update(final String value) {
    EventQueue.invokeLater(new Runnable() {

      @Override
      public void run() {
        SpeedometerViewSwingImpl.this.valueField.setValueAnimated(Double
            .parseDouble(value));
      }
    });
  }

  /* } */

  /* { {GaugeViewSwingImpl} implementation */

  /**
   * @see GaugeViewSwingImpl#getFavor()
   */
  @Override
  public ViewSwingFavor getFavor() {
    return ViewSwingFavor.HORIZONTAL;
  }

  /* } */

}
