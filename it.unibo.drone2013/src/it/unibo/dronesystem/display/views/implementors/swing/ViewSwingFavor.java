package it.unibo.dronesystem.display.views.implementors.swing;

/**
 * Project: dronesystem
 * <p/>
 * Represent the favor of a view that uses the Swing technology.
 */
public enum ViewSwingFavor {
  NEUTRAL, HORIZONTAL, VERTICAL
}
