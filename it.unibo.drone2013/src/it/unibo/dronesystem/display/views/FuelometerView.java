package it.unibo.dronesystem.display.views;

import it.unibo.dronesystem.display.views.implementors.IFuelometerViewImpl;

/**
 * Project: dronesystem
 * <p/>
 * A {IFuelometerView} implementation.
 */
public class FuelometerView extends GaugeView implements IFuelometerView {

  /* { Constructors and factories */

  protected FuelometerView(final IFuelometerViewImpl implementor) {
    super(implementor);
  }

  public static FuelometerView create(final IFuelometerViewImpl implementor) {

    return new FuelometerView(implementor);
  }

  /* } */

}
