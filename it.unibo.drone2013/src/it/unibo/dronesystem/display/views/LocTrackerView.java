package it.unibo.dronesystem.display.views;

import it.unibo.dronesystem.display.views.implementors.ILocTrackerViewImpl;

/**
 * Project: dronesystem
 * <p/>
 * A {ILocTrackerView} implementation.
 */
public class LocTrackerView extends GaugeView implements ILocTrackerView {

  /* { Constructors and factories */

  protected LocTrackerView(final ILocTrackerViewImpl implementor) {
    super(implementor);
  }

  public static LocTrackerView create(final ILocTrackerViewImpl implementor) {

    return new LocTrackerView(implementor);
  }

  /* } */

}
