package it.unibo.dronesystem.display.views;

import it.unibo.dronesystem.display.views.implementors.IOdometerViewImpl;

/**
 * Project: dronesystem
 * <p/>
 * A {IOdometerView} implementation.
 */
public class OdometerView extends GaugeView implements IOdometerView {

  /* { Constructors and factories */

  protected OdometerView(final IOdometerViewImpl implementor) {
    super(implementor);
  }

  public static OdometerView create(final IOdometerViewImpl implementor) {

    return new OdometerView(implementor);
  }

  /* } */

}
