package it.unibo.dronesystem.display.views;

import it.unibo.dronesystem.display.views.implementors.ISpeedometerViewImpl;

/**
 * Project: dronesystem
 * <p/>
 * A {ISpeedometerView} implementation.
 */
public class SpeedometerView extends GaugeView implements ISpeedometerView {

  /* { Constructors and factories */

  protected SpeedometerView(final ISpeedometerViewImpl implementor) {
    super(implementor);
  }

  public static SpeedometerView create(final ISpeedometerViewImpl implementor) {

    return new SpeedometerView(implementor);
  }

  /* } */

}
