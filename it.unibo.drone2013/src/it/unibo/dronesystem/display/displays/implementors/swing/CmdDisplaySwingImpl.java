package it.unibo.dronesystem.display.displays.implementors.swing;

import it.unibo.dronesystem.display.displays.InvalidInputError;
import it.unibo.dronesystem.display.displays.implementors.CmdDisplayImpl;
import org.jdesktop.swingx.JXTextField;  //from swingx-core-1.6.2-2.jar

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Project: dronesystem
 * <p/>
 * A {ICmdDisplaySwingImpl} implementation.
 */
public class CmdDisplaySwingImpl extends CmdDisplayImpl implements
    ICmdDisplaySwingImpl {

  private final String      name;
  private final JPanel      mainPanel;
  private final JLabel      titleLbl;
  private final JPanel      cmdPanel;
  private final JButton     decSpeedBtn;
  private final JButton     incSpeedBtn;
  private final JButton     setSpeedBtn;
  private final JButton     startBtn;
  private final JButton     stopBtn;
  private final JXTextField setSpeedTxt;
  private final JLabel      statusLbl;

  /* { Constructors and factories */

  protected CmdDisplaySwingImpl(final String name) {
    super();

    this.name = name;
    this.mainPanel = new JPanel();
    this.titleLbl = new JLabel();
    this.cmdPanel = new JPanel();
    this.incSpeedBtn = new JButton();
    this.decSpeedBtn = new JButton();
    this.setSpeedBtn = new JButton();
    this.startBtn = new JButton();
    this.stopBtn = new JButton();
    this.setSpeedTxt = new JXTextField();
    this.statusLbl = new JLabel();

    this.setupTitleLbl();
    this.setupStatusLbl();
    this.setupDecSpeedBtn();
    this.setupIncSpeedBtn();
    this.setupSetSpeedBtn();
    this.setupSetSpeedTxt();
    this.setupStartBtn();
    this.setupStopBtn();

    this.setupCmdPanel();
    this.setupMainPanel();
  }

  /**
   * Create a new {CmdDisplaySwingImpl} with the provided name as title.
   * 
   * @param name
   *          The title for the new {CmdDisplaySwingImpl}
   * @return The created {CmdDisplaySwingImpl}
   */
  public static CmdDisplaySwingImpl create(final String name) {
    return new CmdDisplaySwingImpl(name);
  }

  /* } */

  /* { Setup */

  protected void setupTitleLbl() {
    this.titleLbl.setFont(new Font("Arial", Font.BOLD, 16));
    this.titleLbl.setText(this.name);
  }

  protected void setupDecSpeedBtn() {
    this.getDecSpeedBtn().setToolTipText("Press to decrement the speed of DS");
    this.getDecSpeedBtn().setText("Decrement Speed");
    this.getDecSpeedBtn().addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        onDecSpeed();
      }
    });
  }

  protected void setupIncSpeedBtn() {
    this.getIncSpeedBtn().setToolTipText("Press to increment the speed of DS");
    this.getIncSpeedBtn().setText("Increment Speed");
    this.getIncSpeedBtn().addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        onIncSpeed();
      }
    });
  }

  protected void setupSetSpeedBtn() {
    this.getSetSpeedBtn().setToolTipText(
        "Press to set the speed of the " + "value in the text field");
    this.getSetSpeedBtn().setText("Set Speed");
    this.getSetSpeedBtn().addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        onSetSpeed(setSpeedTxt.getText());
      }
    });
  }

  protected void setupStartBtn() {
    this.getStartBtn().setToolTipText("Start the mission");
    this.getStartBtn().setText("Start");
    this.getStartBtn().addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        onStart();
      }
    });
  }

  protected void setupStopBtn() {
    this.getStopBtn().setToolTipText("Stop the mission");
    this.getStopBtn().setText("Stop");
    this.getStopBtn().addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        onStop();
      }
    });
  }

  protected void setupSetSpeedTxt() {
    this.getSetSpeedTxt().setPrompt("Insert the speed you want to set");
    this.getSetSpeedTxt().setText("");
  }

  protected void setupCmdPanel() {
    this.getCmdPanel().setLayout(new GridBagLayout());

    this.addComponent(this.getCmdPanel(), new Box(0), 0, 0, 1, 4, 1.0, 2.0);
    this.addComponent(this.getCmdPanel(), this.getSetSpeedTxt(), 1, 0, 1, 3,
        8.0, 0);
    this.addComponent(this.getCmdPanel(), this.getSetSpeedBtn(), 1, 3, 1, 1, 0,
        0);
    this.addComponent(this.getCmdPanel(), new Box(0), 2, 0, 1, 4, 1.0, 2.0);
    this.addComponent(this.getCmdPanel(), this.getIncSpeedBtn(), 3, 0, 1, 1, 0,
        0);
    this.addComponent(this.getCmdPanel(), this.getDecSpeedBtn(), 3, 1, 1, 1, 0,
        0);
    this.addComponent(this.getCmdPanel(), new Box(0), 4, 0, 1, 4, 1.0, 2.0);
    this.addComponent(this.getCmdPanel(), this.getStartBtn(), 5, 0, 1, 1, 0, 0);
    this.addComponent(this.getCmdPanel(), this.getStopBtn(), 5, 1, 1, 1, 0, 0);
    this.addComponent(this.getCmdPanel(), new Box(0), 5, 2, 1, 2, 8.0, 1.0);
    this.addComponent(this.getCmdPanel(), new Box(0), 6, 0, 1, 4, 1.0, 2.0);
  }

  protected void setupStatusLbl() {
    this.getStatusLbl().setToolTipText("The commands status");
    this.statusLbl.setFont(new Font("Arial", Font.ITALIC, 12));
    this.setStatus("Command display initialization done.");
  }

  protected void setupMainPanel() {
    this.getMainPanel().setLayout(new BorderLayout());
    this.getMainPanel().add(this.getTitleLbl(), BorderLayout.NORTH);
    this.getMainPanel().add(this.getCmdPanel(), BorderLayout.CENTER);
    this.getMainPanel().add(this.getStatusLbl(), BorderLayout.SOUTH);
  }

  /* } */

  /* { Gui events callbacks */

  protected void onSetSpeed(final String speedRep) {
    try {
      this.notifySetSpeed(speedRep);
      //this.setStatus(String.format("Speed set to %s", speedRep));
    } catch (InvalidInputError error) {
      this.setStatus("Received an invalid input speed. Please insert a correct one.");
      this.getSetSpeedTxt().setText("");
    }
  }

  protected void onIncSpeed() {
    this.notifyIncSpeed();
    //this.setStatus(String.format("Speed increased"));
  }

  protected void onDecSpeed() {
    this.notifyDecSpeed();    
    //this.setStatus(String.format("Speed decreased"));
  }

  protected void onStart() {
    this.notifyStart();
    //this.setStatus(String.format("Started"));
  }

  protected void onStop() {
    this.notifyStop();
    //this.setStatus(String.format("Stopped"));
  }

  /**
   * @see ICmdDisplaySwingImpl#getMainPanel()
   */
  @Override
  public JPanel getMainPanel() {
    return this.mainPanel;
  }

  /* } */

  /* { Utility */

  /**
   * Set the status label to the provided statusTxt
   * 
   * @param statusTxt
   *          The new text for the status label
   */
  protected void setStatus(final String statusTxt) {
    this.statusLbl.setText(statusTxt);
  }

  /**
   * Getter for this.decSpeedBtn
   * 
   * @return The decrement speed button
   */
  protected JButton getDecSpeedBtn() {
    return this.decSpeedBtn;
  }

  /**
   * Getter for this.incSpeedBtn
   * 
   * @return The increment speed button
   */
  protected JButton getIncSpeedBtn() {
    return this.incSpeedBtn;
  }

  /**
   * Getter for this.setSpeedBtn
   * 
   * @return The set speed button
   */
  protected JButton getSetSpeedBtn() {
    return this.setSpeedBtn;
  }

  /**
   * Getter for this.startBtn
   * 
   * @return The start button
   */
  protected JButton getStartBtn() {
    return this.startBtn;
  }

  /**
   * Getter for this.stopBtn
   * 
   * @return The stop button
   */
  protected JButton getStopBtn() {
    return this.stopBtn;
  }

  /**
   * Getter for this.setSpeedTxt
   * 
   * @return The set speed text field
   */
  protected JXTextField getSetSpeedTxt() {
    return this.setSpeedTxt;
  }

  /**
   * Getter for this.cmdPanel
   * 
   * @return The command panel
   */
  protected JPanel getCmdPanel() {
    return this.cmdPanel;
  }

  /**
   * Getter for this.titleLbl
   * 
   * @return The title label
   */
  protected JLabel getTitleLbl() {
    return this.titleLbl;
  }

  /**
   * Getter for this.statusLbl
   * 
   * @return The status label
   */
  public JLabel getStatusLbl() {
    return this.statusLbl;
  }

  /**
   * Add the provided component in the provided container
   * 
   * @param container
   *          The container in which the component will be added
   * @param component
   *          The component row
   */
  protected void addComponent(Container container, Component component,
      int rowIdx, int colIdx, int rowCount, int colCount,
      double horizontalWeight, double verticalWeight) {
    Insets insets = new Insets(0, 0, 0, 0);
    GridBagConstraints gbc = new GridBagConstraints(colIdx, rowIdx, colCount,
        rowCount, horizontalWeight, verticalWeight, GridBagConstraints.NORTH,
        GridBagConstraints.BOTH, insets, 0, 0);
    container.add(component, gbc);
  }

  /* } */

  /* { Usage example */

  public static void main(String[] args) {
    JFrame frame = new JFrame("Frame for CmdDisplay Example");
    frame.setContentPane(new CmdDisplaySwingImpl("CmdDisplay").getCmdPanel());
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  }

  /* } */

}
