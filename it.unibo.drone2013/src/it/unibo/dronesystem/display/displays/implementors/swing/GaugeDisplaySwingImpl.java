package it.unibo.dronesystem.display.displays.implementors.swing;

import it.unibo.dronesystem.display.displays.GaugeDisplayStatus;
import it.unibo.dronesystem.display.views.implementors.swing.IFuelometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.ILocTrackerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.IOdometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.ISpeedometerViewSwingImpl;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.*;

/**
 * Project: dronesystem
 * <p/>
 * A {IGaugeDisplaySwingImpl} implementation.
 */
public class GaugeDisplaySwingImpl implements IGaugeDisplaySwingImpl {

  private final String                    name;

  private final IFuelometerViewSwingImpl  fuelometerView;
  private final ISpeedometerViewSwingImpl speedometerView;
  private final IOdometerViewSwingImpl    odometerView;
  private final ILocTrackerViewSwingImpl  locTrackerView;

  private final JPanel                    mainPanel;
  private final JLabel                    titleLbl;
  private final JPanel                    gaugesPanel;
  private final JLabel                    statusLbl;

  /* { Constructors and factories */

  protected GaugeDisplaySwingImpl(final String name,
      final IFuelometerViewSwingImpl fuelometerView,
      final ISpeedometerViewSwingImpl speedometerView,
      final IOdometerViewSwingImpl odometerView,
      final ILocTrackerViewSwingImpl locTrackerView) {

    this.name = name;
    this.fuelometerView = fuelometerView;
    this.speedometerView = speedometerView;
    this.odometerView = odometerView;
    this.locTrackerView = locTrackerView;

    this.mainPanel = new JPanel();
    this.titleLbl = new JLabel();
    this.gaugesPanel = new JPanel();
    this.statusLbl = new JLabel();

    this.setupTitleLbl();
    this.setupGaugesPanel();
    this.setupStatusLbl();
    this.setupMainPanel();
  }

  public static GaugeDisplaySwingImpl create(final String name,
      final IFuelometerViewSwingImpl fuelometerView,
      final ISpeedometerViewSwingImpl speedometerView,
      final IOdometerViewSwingImpl odometerView,
      final ILocTrackerViewSwingImpl locTrackerView) {
    return new GaugeDisplaySwingImpl(name, fuelometerView, speedometerView,
        odometerView, locTrackerView);
  }

  /* } */

  /* { Setup */

  protected void setupTitleLbl() {
    this.titleLbl.setFont(new Font("Arial", Font.BOLD, 16));
    this.titleLbl.setText(this.name);
  }

  protected void setupStatusLbl() {
    this.statusLbl.setFont(new Font("Arial", Font.ITALIC, 12));
    this.statusLbl.setText("Gauge Display initialized.");
  }

  protected void setupGaugesPanel() {
    this.getGaugesPanel().setLayout(new GridBagLayout());

  //int rowIdx, int colIdx, int rowCount, int colCount, double horizontalWeight, double verticalWeight
     this.addComponent(this.getGaugesPanel(),
        this.locTrackerView.getMainPanel(), 0, 0, 1, 5, 1.0, 1.0);
    this.addComponent(this.getGaugesPanel(),
        this.speedometerView.getMainPanel(), 1, 0, 2, 3, 1.0, 0.1);
    this.addComponent(this.getGaugesPanel(),
        this.fuelometerView.getMainPanel(), 1, 3, 1, 2, 0.4, 0.1);
    this.addComponent(this.getGaugesPanel(), this.odometerView.getMainPanel(),
        2, 3, 1, 2, 0.4, 0.1);
  }

  protected void setupMainPanel() {
    this.getMainPanel().setLayout(new BorderLayout());
    this.getMainPanel().add(this.getTitleLbl(), BorderLayout.NORTH);
    this.getMainPanel().add(this.getGaugesPanel(), BorderLayout.CENTER);
    this.getMainPanel().add(this.getStatusLbl(), BorderLayout.SOUTH);
  }

  /* } */

  /* { {IGaugeDisplaySwingImpl} implementation */

  /**
   * @see IGaugeDisplaySwingImpl#getMainPanel()
   */
  @Override
  public JPanel getMainPanel() {
    return this.mainPanel;
  }

  /**
   * @see IGaugeDisplaySwingImpl#setStatus(GaugeDisplayStatus)
   */
  @Override
  public void setStatus(final GaugeDisplayStatus status) {
    switch (status) {
      case CONNECTED_TO_THE_SYSTEM:
        this.setStatus("The display has been connected to the system.");
        break;
      case DISCONNECTED_FROM_THE_SYSTEM:
        this.setStatus("The display has been disconnected from the system.");
        break;
    }
  }

  /* } */

  /* { Utility */

  /**
   * Getter for this.gaugesPanel
   * 
   * @return The gauges panel
   */
  protected JPanel getGaugesPanel() {
    return this.gaugesPanel;
  }

  /**
   * Getter for this.titleLbl
   * 
   * @return The title label
   */
  protected JLabel getTitleLbl() {
    return this.titleLbl;
  }

  public JLabel getStatusLbl() {
    return statusLbl;
  }

  public void setStatus(String txt) {
    this.statusLbl.setText(txt);
  }

  /**
   * Add the provided component in the provided container
   * 
   * @param container
   *          The container in which the component will be added
   * @param component
   *          The component row
   */
  protected void addComponent(Container container, Component component,
      int rowIdx, int colIdx, int rowCount, int colCount,
      double horizontalWeight, double verticalWeight) {
    Insets insets = new Insets(0, 0, 0, 0);
    GridBagConstraints gbc = new GridBagConstraints(colIdx, rowIdx, colCount,
        rowCount, horizontalWeight, verticalWeight, GridBagConstraints.NORTH,
        GridBagConstraints.BOTH, insets, 0, 0);
    container.add(component, gbc);
  }

  /* } */

}
