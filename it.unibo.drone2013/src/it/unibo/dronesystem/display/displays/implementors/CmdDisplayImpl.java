package it.unibo.dronesystem.display.displays.implementors;

import it.unibo.dronesystem.display.displays.InvalidInputError;

import java.util.ArrayList;
import java.util.List;

/**
 * Project: dronesystem
 */
public abstract class CmdDisplayImpl implements ICmdDisplayImpl {

  private final List<ICmdDisplayImplListener> listeners;

  /* { Constructors and factories */

  protected CmdDisplayImpl() {
    this.listeners = new ArrayList<ICmdDisplayImplListener>();
  }

  /* } */

  /* {ICmdDisplayImpl} implementation */

  /**
   * @see ICmdDisplayImpl#register(ICmdDisplayImplListener)
   */
  @Override
  public void register(final ICmdDisplayImplListener listener) {
    System.out.printf("Registered the listener: %s%n", listener);
    this.listeners.add(listener);
  }

  /* } */

  /* { Notifiers */

  protected void notifyIncSpeed() {
    for (ICmdDisplayImplListener listener : this.listeners) {
      listener.onIncSpeed();
    }
  }

  protected void notifyDecSpeed() {
    for (ICmdDisplayImplListener listener : this.listeners) {
      listener.onDecSpeed();
    }
  }

  protected void notifySetSpeed(final String speedDefRep)
      throws InvalidInputError {
    for (ICmdDisplayImplListener listener : this.listeners) {
      listener.onSetSpeed(speedDefRep);
    }
//	  System.out.println(" *** CmdDisplayImpl notifySetSpeed " + speedDefRep );
  }

  protected void notifyStart() {
    for (ICmdDisplayImplListener listener : this.listeners) {
      listener.onStart();
    }
//	  System.out.println(" *** CmdDisplayImpl notifyStart "   );
  }

  protected void notifyStop() {
    for (ICmdDisplayImplListener listener : this.listeners) {
      listener.onStop();
    }
  }

  /* } */

}
