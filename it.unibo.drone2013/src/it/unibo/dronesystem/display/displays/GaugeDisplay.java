package it.unibo.dronesystem.display.displays;

import it.unibo.dronesystem.display.displays.implementors.IGaugeDisplayImpl;

/**
 * Project: dronesystem
 * <p/>
 * A {IGaugeDisplay} implementation.
 */
public class GaugeDisplay implements IGaugeDisplay {

  private final IGaugeDisplayImpl implementor;

  /* { Constructors and factories */

  protected GaugeDisplay(final IGaugeDisplayImpl implementor) {
    this.implementor = implementor;
  }

  public static GaugeDisplay create(final IGaugeDisplayImpl implementor) {

    return new GaugeDisplay(implementor);
  }

  /* } */

  /* { {IGaugeDisplay} implementation */

  @Override
  public void setStatus(final GaugeDisplayStatus status) {
    this.implementor.setStatus(status);
  }

  /* } */

}
