package it.unibo.dronesystem.display.factories;

import it.unibo.dronesystem.data.gauge.fuel.IFuel;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;
import it.unibo.dronesystem.display.dashboards.implementors.swing.DroneCtrlDashSwingImpl;
import it.unibo.dronesystem.display.dashboards.implementors.swing.IDroneCtrlDashSwingImpl;
import it.unibo.dronesystem.display.dashboards.implementors.swing.ISmartDeviceDashSwingImpl;
import it.unibo.dronesystem.display.dashboards.implementors.swing.SmartDeviceDashSwingImpl;
import it.unibo.dronesystem.display.displays.implementors.swing.CmdDisplaySwingImpl;
import it.unibo.dronesystem.display.displays.implementors.swing.GaugeDisplaySwingImpl;
import it.unibo.dronesystem.display.displays.implementors.swing.ICmdDisplaySwingImpl;
import it.unibo.dronesystem.display.displays.implementors.swing.IGaugeDisplaySwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.*;

/**
 * Project: dronesystem
 * <p/>
 * A {IDisplaySwingImplFactory} implementation.
 */
public class DisplaySwingImplFactory extends GaugeViewImplFactory implements
    IDisplaySwingImplFactory {

  /* { {IDisplaySwingImplFactory} implementation */

  /**
   * @see IDisplaySwingImplFactory #createDroneCtrlDashImpl(String,
   *      IGaugeDisplaySwingImpl, ICmdDisplaySwingImpl)
   */
  @Override
  public IDroneCtrlDashSwingImpl createDroneCtrlDashImpl(final String name,
      IGaugeDisplaySwingImpl gaugeDisplay, ICmdDisplaySwingImpl cmdDisplay) {
	  System.out.println("%%% createDroneCtrlDashImpl");
    return DroneCtrlDashSwingImpl.create(name, gaugeDisplay, cmdDisplay);
  }

  /**
   * @see IDisplaySwingImplFactory #createSmartDeviceDashImpl(String,
   *      IGaugeDisplaySwingImpl)
   */
  @Override
  public ISmartDeviceDashSwingImpl createSmartDeviceDashImpl(final String name,
      IGaugeDisplaySwingImpl gaugeDisplay) {
    return SmartDeviceDashSwingImpl.create(name, gaugeDisplay);
  }

  /**
   * @see IDisplaySwingImplFactory#createGaugeDisplayImpl (String,
   *      IFuelometerViewSwingImpl, ISpeedometerViewSwingImpl,
   *      IOdometerViewSwingImpl, ILocTrackerViewSwingImpl)
   */
  @Override
  public IGaugeDisplaySwingImpl createGaugeDisplayImpl(final String name,
      final IFuelometerViewSwingImpl fuelometerView,
      final ISpeedometerViewSwingImpl speedometerView,
      final IOdometerViewSwingImpl odometerView,
      final ILocTrackerViewSwingImpl locTrackerView) {
    return GaugeDisplaySwingImpl.create(name, fuelometerView, speedometerView,
        odometerView, locTrackerView);
  }

  /**
   * @see IDisplaySwingImplFactory#createCmdDisplayImpl(String)
   */
  @Override
  public ICmdDisplaySwingImpl createCmdDisplayImpl(final String name) {
    return CmdDisplaySwingImpl.create(name);
  }

  /**
   * @see IDisplaySwingImplFactory#createFuelometerViewImpl (String, String,
   *      IFuel, IFuel)
   */
  @Override
  public IFuelometerViewSwingImpl createFuelometerViewImpl(final String name,
      final String unitName, final IFuel minValueShown,
      final IFuel maxValueShown) {
    return FuelometerViewSwingImpl.create(name, unitName, minValueShown,
        maxValueShown);
  }

  /**
   * @see IDisplaySwingImplFactory#createSpeedometerViewImpl (String, String,
   *      ISpeed, ISpeed)
   */
  @Override
  public ISpeedometerViewSwingImpl createSpeedometerViewImpl(final String name,
      final String unitName, final ISpeed minValueShown,
      final ISpeed maxValueShown) {
    return SpeedometerViewSwingImpl.create(name, unitName, minValueShown,
        maxValueShown);
  }

  /**
   * @see IDisplaySwingImplFactory#createOdometerViewImpl (String, String)
   */
  @Override
  public IOdometerViewSwingImpl createOdometerViewImpl(final String name,
      final String unitName) {
    return OdometerViewSwingImpl.create(name, unitName);
  }

  /**
   * @see IDisplaySwingImplFactory#createLocTrackerViewImpl (String)
   */
  @Override
  public ILocTrackerViewSwingImpl createLocTrackerViewImpl(final String name) {
    return LocTrackerViewSwingImpl.create(name);
  }

  /* } */
}
