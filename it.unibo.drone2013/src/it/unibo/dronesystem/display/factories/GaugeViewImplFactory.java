package it.unibo.dronesystem.display.factories;

/**
 * Project: dronesystem
 * <p/>
 * A (abstract) {IGaugeViewImplFactory} implementation that defines default
 * attributes, methods for its implementations.
 */
public abstract class GaugeViewImplFactory implements IGaugeViewImplFactory {
  /* INF: Empty */
}
