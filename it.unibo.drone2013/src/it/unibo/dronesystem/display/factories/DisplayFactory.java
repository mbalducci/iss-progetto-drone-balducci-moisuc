package it.unibo.dronesystem.display.factories;


import it.unibo.dronesystem.data.factories.IGaugeValueFactory;
import it.unibo.dronesystem.data.gauge.speed.ISpeed;
import it.unibo.dronesystem.display.dashboards.DroneCtrlDash;
import it.unibo.dronesystem.display.dashboards.IDroneCtrlDash;
import it.unibo.dronesystem.display.dashboards.ISmartDeviceDash;
import it.unibo.dronesystem.display.dashboards.SmartDeviceDash;
import it.unibo.dronesystem.display.dashboards.implementors.IDroneCtrlDashImpl;
import it.unibo.dronesystem.display.dashboards.implementors.ISmartDeviceDashImpl;
//import it.unibo.dronesystem.display.displays.CmdDisplay;
import it.unibo.dronesystem.display.displays.GaugeDisplay;
import it.unibo.dronesystem.display.displays.ICmdDisplay;
import it.unibo.dronesystem.display.displays.IGaugeDisplay;
import it.unibo.dronesystem.display.displays.implementors.ICmdDisplayImpl;
import it.unibo.dronesystem.display.displays.implementors.IGaugeDisplayImpl;
import it.unibo.dronesystem.display.views.*;
import it.unibo.dronesystem.display.views.implementors.IFuelometerViewImpl;
import it.unibo.dronesystem.display.views.implementors.ILocTrackerViewImpl;
import it.unibo.dronesystem.display.views.implementors.IOdometerViewImpl;
import it.unibo.dronesystem.display.views.implementors.ISpeedometerViewImpl;

/**
 * Project: dronesystem
 * <p/>
 * A {IDisplayFactory} implementation.
 */
public class DisplayFactory implements IDisplayFactory {

  /* { {IDisplayFactory} implementation */

  /**
   * @see IDisplayFactory#createDroneCtrlDash (IDroneCtrlDashImpl,
   *      IGaugeDisplay, ICmdDisplay)
   */
  @Override
  public IDroneCtrlDash createDroneCtrlDash(
      final IDroneCtrlDashImpl implementor, final IGaugeDisplay gaugeDisplay,
      final ICmdDisplay cmdDisplay) {
    return DroneCtrlDash.create(implementor, gaugeDisplay, cmdDisplay);
  }

  /**
   * @see IDisplayFactory#createSmartDeviceDash (ISmartDeviceDashImpl,
   *      IGaugeDisplay)
   */
  @Override
  public ISmartDeviceDash createSmartDeviceDash(
      final ISmartDeviceDashImpl implementor, final IGaugeDisplay gaugeDisplay) {
    return SmartDeviceDash.create(implementor, gaugeDisplay);
  }

  /**
   * @see IDisplayFactory#createGaugeDisplay(IGaugeDisplayImpl)
   */
  @Override
  public IGaugeDisplay createGaugeDisplay(final IGaugeDisplayImpl implementor) {
    return GaugeDisplay.create(implementor);
  }

  /**
   * @see IDisplayFactory#createCmdDisplay(ActorRef, ICmdDisplayImpl, ISpeed,
   *      IGaugeValueFactory)
   */
//  @Override
//  public ICmdDisplay createCmdDisplay(final ActorRef cmdDisplayActor,
//      final ICmdDisplayImpl implementor, final ISpeed defaultSpeedVariation,
//      final IGaugeValueFactory gaugeValueFactory) {
//    return CmdDisplay.create(cmdDisplayActor, implementor,
//        defaultSpeedVariation, gaugeValueFactory);
//  }

  /**
   * @see IDisplayFactory#createFuelometerView(IFuelometerViewImpl)
   */
  @Override
  public IFuelometerView createFuelometerView(
      final IFuelometerViewImpl implementor) {
    return FuelometerView.create(implementor);
  }

  /**
   * @see IDisplayFactory#createSpeedometerView(ISpeedometerViewImpl)
   */
  @Override
  public ISpeedometerView createSpeedometerView(
      final ISpeedometerViewImpl implementor) {
    return SpeedometerView.create(implementor);
  }

  /**
   * @see IDisplayFactory#createOdometerView(IOdometerViewImpl)
   */
  @Override
  public IOdometerView createOdometerView(final IOdometerViewImpl implementor) {
    return OdometerView.create(implementor);
  }

  /**
   * @see IDisplayFactory#createLocTrackerView(ILocTrackerViewImpl)
   */
  @Override
  public ILocTrackerView createLocTrackerView(
      final ILocTrackerViewImpl implementor) {
    return LocTrackerView.create(implementor);
  }

  /* } */

}
