package it.unibo.dronesystem.main;
import java.math.BigDecimal;
import it.unibo.dronesystem.data.factories.GaugeValueFactory;
import it.unibo.dronesystem.display.dashboards.implementors.swing.DroneCtrlDashSwingImpl;
import it.unibo.dronesystem.display.displays.implementors.ICmdDisplayImplListener;
import it.unibo.dronesystem.display.displays.implementors.swing.CmdDisplaySwingImpl;
import it.unibo.dronesystem.display.views.*;
import it.unibo.dronesystem.display.views.implementors.swing.FuelometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.LocTrackerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.OdometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.SpeedometerViewSwingImpl;

/*
 * The command display is associated (via register, see initCmdDisplay) 
 * to a oo listener (MyObjListenerOnView by AN) that has a reference to a set of gauge views
 * The update MUST BE re-designed so to delegate (via message-passing)
 * the responsibility  to some  (active) control entity.
 */
public class MainGaugeSimpleWithView {
protected GaugeValueFactory gaugeValueFactory;

protected ISpeedometerView  speedometerView ;
protected IFuelometerView  fuelometerView ;
protected IOdometerView  odometerView ;
protected ILocTrackerView  locTrackerView ;

protected SpeedometerViewSwingImpl speedometerViewImpl;
protected FuelometerViewSwingImpl fuelometerViewImpl;
protected OdometerViewSwingImpl odometerViewImpl;
protected LocTrackerViewSwingImpl locTrackerViewImpl;

protected GaugeDisplaySwingImplOnViewImpl gaugeDisplayImpl;
protected CmdDisplaySwingImpl cmdDisplayImpl;
protected DroneCtrlDashSwingImpl droneCtrlDashImpl;

   protected void doJob() {
      try {
     	System.out.println("%%% Creating DroneCtrlDashActivator");
     	init( ) ;
     	start();
     } catch (Exception exc) {
      System.out.printf("Failed to start the subsystem. Reason: %s%n", exc.getMessage());
       exc.printStackTrace();
    }
   }

   protected void init(){
	   initFactories( ); 
	   initGaugeViews( );
	   initGaugeDisplay( ) ;
	   initCmdDisplay( ) ;
	   initDashBoard( ) ;
    }
   
   protected void initFactories( ){
	  //Init the factory of the values (domain model)
	  gaugeValueFactory = new GaugeValueFactory();	   
   }
   protected void initGaugeViewImpl( ) {
	 speedometerViewImpl = SpeedometerViewSwingImpl.create(
		        "Speedometer", "kmh",
		        gaugeValueFactory.createSpeed(BigDecimal.valueOf(2.0)),
		        gaugeValueFactory.createSpeed(BigDecimal.valueOf(120.0)));	   
	  fuelometerViewImpl = FuelometerViewSwingImpl.create(
			  "Fuelometer", "l",
			  gaugeValueFactory.createFuel(BigDecimal.valueOf(30)),
			  gaugeValueFactory.createFuel(BigDecimal.valueOf(220)));
	  odometerViewImpl = OdometerViewSwingImpl.create("Odometer", "km");
//	  locTrackerViewImpl = LocTrackerViewSwingImpl.create("LocTracker");
   }
 
   protected void initGaugeViews( ) {
	 initGaugeViewImpl( );  	   
	 speedometerView  = SpeedometerView.create( speedometerViewImpl );			 	  
 	 fuelometerView = FuelometerView.create( fuelometerViewImpl ); 
 	 odometerView  = OdometerView.create( odometerViewImpl );
// 	 locTrackerView  = LocTrackerView.create( locTrackerViewImpl );
   }
   
   protected void initGaugeDisplay( ) {
	    gaugeDisplayImpl = GaugeDisplaySwingImplOnViewImpl.create(
		        "Gauge Display", 
		        fuelometerViewImpl, 
		        speedometerViewImpl,
		        odometerViewImpl
//		        locTrackerViewImpl
		        );	   
   }
   public void initCmdDisplay( ) {
	  cmdDisplayImpl = CmdDisplaySwingImpl.create("CmdDisplay");
	  ICmdDisplayImplListener listener = 
			  new MyObjListenerOnView( 
					  speedometerView, 
					  fuelometerView,
					  odometerView
//					  locTrackerView
			  ) ;
	  cmdDisplayImpl.register(listener);	   
   }
  public void initDashBoard( ) {
	 droneCtrlDashImpl = DroneCtrlDashSwingImpl.create(
			        "Drone Control Dashboard", gaugeDisplayImpl, cmdDisplayImpl );
  }
  
  protected void start(){
	  droneCtrlDashImpl.start();	  
  }
  
  
  
  /*
   * MAIN
   */
  public static void main(String[] args) {
    System.out.println(">>> MainGaugeSimpleWithView Starting.");
    MainGaugeSimpleWithView system = new MainGaugeSimpleWithView();
    system.doJob();
  }

 

}
