package it.unibo.dronesystem.main;

import it.unibo.dronesystem.display.displays.InvalidInputError;
import it.unibo.dronesystem.display.displays.implementors.ICmdDisplayImplListener;
import it.unibo.dronesystem.display.views.*;

public class MyObjListenerOnView implements ICmdDisplayImplListener{
protected ISpeedometerView speedometerViewImpl ;
protected IFuelometerView fuelometerViewImpl ;
protected IOdometerView odometerViewImpl ;
protected ILocTrackerView locTrackerViewImpl;
	public MyObjListenerOnView(ISpeedometerView speedometerView ){
		this.speedometerViewImpl = speedometerView;
	}
	public MyObjListenerOnView(ISpeedometerView speedometerView,
			IFuelometerView fuelometerView ){
		this(speedometerView);
		this.fuelometerViewImpl = fuelometerView;
	}
	public MyObjListenerOnView(
			ISpeedometerView speedometerView,
			IFuelometerView fuelometerView,
			IOdometerView odometerView){
		this(speedometerView,fuelometerView);
		this.odometerViewImpl = odometerView;
	}
	public MyObjListenerOnView(
			ISpeedometerView speedometerView,
			IFuelometerView fuelometerView,
			IOdometerView odometerView,
			ILocTrackerView locTrackerViewImpl
			){
		this(speedometerView,fuelometerView);
		this.locTrackerViewImpl = locTrackerViewImpl;
	}
	@Override
	public void onIncSpeed() {
 	
	}

	@Override
	public void onDecSpeed() {
 		
	}

	@Override
	public void onSetSpeed(String speedValueDefRep) throws InvalidInputError {
 		System.out.println("MyObjListener " + speedValueDefRep);
 		System.out.println("MyObjListener speedometerView= " + speedometerViewImpl);
 		speedometerViewImpl.update(speedValueDefRep);
	}

	@Override
	public void onStart() {
		fuelometerViewImpl.update("80");
		odometerViewImpl.update("1");
//		locTrackerViewImpl.update("45");
	}

	@Override
	public void onStop() {
		speedometerViewImpl.update("0");
		fuelometerViewImpl.update("40");
		odometerViewImpl.update("0");
		
	}

}
