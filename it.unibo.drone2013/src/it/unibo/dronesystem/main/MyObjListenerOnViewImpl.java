package it.unibo.dronesystem.main;


import it.unibo.dronesystem.display.displays.InvalidInputError;
import it.unibo.dronesystem.display.displays.implementors.ICmdDisplayImplListener;
import it.unibo.dronesystem.display.views.implementors.swing.IFuelometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.ILocTrackerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.IOdometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.ISpeedometerViewSwingImpl;

public class MyObjListenerOnViewImpl implements ICmdDisplayImplListener{
protected ISpeedometerViewSwingImpl speedometerViewImpl ;
protected IFuelometerViewSwingImpl fuelometerViewImpl ;
protected IOdometerViewSwingImpl odometerViewImpl ;
protected ILocTrackerViewSwingImpl locTrackerViewImpl;
	public MyObjListenerOnViewImpl(ISpeedometerViewSwingImpl speedometerView ){
		this.speedometerViewImpl = speedometerView;
	}
	public MyObjListenerOnViewImpl(ISpeedometerViewSwingImpl speedometerView,
			IFuelometerViewSwingImpl fuelometerView ){
		this(speedometerView);
		this.fuelometerViewImpl = fuelometerView;
	}
	public MyObjListenerOnViewImpl(
			ISpeedometerViewSwingImpl speedometerView,
			IFuelometerViewSwingImpl fuelometerView,
			IOdometerViewSwingImpl odometerView){
		this(speedometerView,fuelometerView);
		this.odometerViewImpl = odometerView;
	}
	public MyObjListenerOnViewImpl(
			ISpeedometerViewSwingImpl speedometerView,
			IFuelometerViewSwingImpl fuelometerView,
			IOdometerViewSwingImpl odometerView,
			ILocTrackerViewSwingImpl locTrackerViewImpl
			){
		this(speedometerView,fuelometerView);
		this.locTrackerViewImpl = locTrackerViewImpl;
	}
	@Override
	public void onIncSpeed() {

	}

	@Override
	public void onDecSpeed() {
 		
	}

	@Override
	public void onSetSpeed(String speedValueDefRep) throws InvalidInputError {
 		System.out.println("MyObjListener " + speedValueDefRep);
 		System.out.println("MyObjListener speedometerView= " + speedometerViewImpl);
 		speedometerViewImpl.update(speedValueDefRep);
	}

	@Override
	public void onStart() {
		fuelometerViewImpl.update("80");
		odometerViewImpl.update("1");
//		locTrackerViewImpl.update("45");
	}

	@Override
	public void onStop() {
		speedometerViewImpl.update("0");
		fuelometerViewImpl.update("40");
		odometerViewImpl.update("0");
		
	}

}
