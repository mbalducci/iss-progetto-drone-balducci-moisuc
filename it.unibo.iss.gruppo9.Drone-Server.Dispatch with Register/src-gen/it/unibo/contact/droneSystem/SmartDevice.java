package it.unibo.contact.droneSystem;

import it.unibo.contact.droneSystem.SmartDeviceSupport;
import it.unibo.contact.platformuv.RunTimeKb;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister.viewer.Info;

public class SmartDevice extends SmartDeviceSupport {
	
	String ip;
	String portNo;
	
	public SmartDevice(String name) throws Exception {
		super(name);
	}

	public SmartDevice(String name, IOutputView view, String ip, String portNo) throws Exception {
		super(name);
		this.view = view;
		this.ip = ip;
		this.portNo= portNo;
		RunTimeKb.init(view);
		// Protocols for application messages
		RunTimeKb.addSubject("TCP" , "drone" , "registerSD","192.168.1.100",8010 );
		

	}
	
	
	@Override
	protected void addOutput(String data) throws Exception {
		view.addOutput(data);
	}

	@Override
	protected void updateSDGui(String data) throws Exception {
		view.setOutput(data);
	}

	@Override
	protected void register() throws Exception {
		curAcquireOneReply=hl_smartDevice_demand_registerSD_drone("register("+ip+","+portNo+")");
		curReply=curAcquireOneReply.acquireReply(5000);
		setName(curReply.msgContent());
		// Register for application msg with the unique Id given by the drone

		RunTimeKb.addSubject("TCP", getName(), "sensorDataSD", ip,	Integer.parseInt(portNo));
		RunTimeKb.addSubject("TCP", getName(), "notifyMissionStarted", ip,	Integer.parseInt(portNo)+1);
		initLastMsgRdMemo();
	}
	
	protected void showNotification(String data) throws Exception {
				view.setOutput(data);
	}
		

}
