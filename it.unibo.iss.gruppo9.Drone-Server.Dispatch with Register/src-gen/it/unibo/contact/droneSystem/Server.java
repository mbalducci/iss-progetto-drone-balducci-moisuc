package it.unibo.contact.droneSystem;


import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister.ApplParams;
import it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister.DroneCtrlDashSwingImpl;



public class Server extends ServerSupport {
	
	DroneCtrlDashSwingImpl serverGui;
	
	
	double fuel,lat,lon,alt,dist;
	int speed;
	
	
	public Server(String name) throws Exception {
		super(name);
	}

	//Estrae il nome del comando ricevuto dal messaggio
	@Override
	protected String getCmdFromMsgS(String msg) throws Exception {
		Struct msgt = (Struct)Term.createTerm( msg );
		return msgt.getName().trim();
	}

	//Controlla che la velocit� impostata con SetSpeed sia compresa tra MIN_SPEED e MAX_SPEED
	@Override
	protected boolean checkSpeed(String speedString) throws Exception {
		int speed=0;
		speedString=speedString.substring(speedString.indexOf('(')+1,speedString.indexOf(')'));
		try{
			speed = Integer.parseInt(speedString);
		}catch(NumberFormatException e){
			return false;
		}
		if(speed >= ApplParams.MIN_SPEED && speed <= ApplParams.MAX_SPEED){
			return true;
		}
		return false;
	}

	//Verifica che il livello di carburante non sia sceso sotto il livello minimo
	@Override
	protected boolean isLowFuel() throws Exception {
		if(fuel < ApplParams.MIN_FUEL){
			return true;
		}
		return false;
	}

	//Salva una copia locale dei dati appena ricevuti dal drone
	@Override
	protected void updateDroneData(String data) throws Exception {
		Struct msgt = (Struct)Term.createTerm( data );
		speed = Integer.parseInt(""+msgt.getArg(0));
		fuel = Double.parseDouble(""+msgt.getArg(1));
		lat = Double.parseDouble(""+msgt.getArg(2));
		lon = Double.parseDouble(""+msgt.getArg(3));
		alt = Double.parseDouble(""+msgt.getArg(4));
		dist = Double.parseDouble(""+msgt.getArg(5));
		
	}

	//Verifica che non si sforino i bound della velocit� a seguito in un incSpeed o decSpeed
	@Override
	protected boolean checkModifySpeed(String command) throws Exception {
		int deltaSpeed= Integer.parseInt(command.substring(command.indexOf('(')+1,command.indexOf(')')));
		command = getCmdFromMsgS(command);
		if(command.equals(ApplParams.INC) && speed+deltaSpeed <= ApplParams.MAX_SPEED){
			speed += deltaSpeed;
			return true;
		}
		if(command.equals(ApplParams.DEC) && speed-deltaSpeed >= ApplParams.MIN_SPEED){
			speed -= deltaSpeed;
			return true;
		}
		return false;
	}

}
