package it.unibo.contact.droneSystem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.contact.platformuv.RawBuffer;
import it.unibo.is.interfaces.IContentSegmentable;

public class MServer extends MServerSupport {

	String lastPhotoPath;
	File log;

	public MServer(String name) throws Exception {
		super(name);
	}

	@Override
	protected int getFileSizeAndSetName(String rMsg) throws Exception {
		Struct rt = (Struct) Term.createTerm(rMsg);
		fileNameToReceive = getNameClean("" + rt.getArg(1));
		return Integer.parseInt("" + rt.getArg(0));
	}

	@Override
	protected void createMissionFolder(String missionOutputFolder) {
		try {
			File f = new File(missionOutputFolder);
			f.mkdirs();
			log = new File(missionOutputFolder + "log.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected IContentSegmentable createFileToBuild(String fileDestPath)
			throws Exception {
		lastPhotoPath = fileDestPath;
		return new it.unibo.contact.platformuv.ContentSegmentable(
				new java.io.FileOutputStream(lastPhotoPath));
	}

	@Override
	protected void buildTheFileFromRaw() throws Exception {
		int rowLength = 0;
		int bufDim = 8000;
		if (0 == (rowMsgLength % 8000))
			bufDim = bufDim + 20;
		// showMsg("			*** bufDim=" + bufDim) ;
		byte[] buffer = new byte[bufDim];
		showMsg("===================================================");
		while (rowLength < rowMsgLength) {
			int n = supportIn.receiveRaw(buffer);
			rowLength = rowLength + n;
			showMsg("***buildTheFileFromRaw received " + n + "/" + rowLength
					+ "/" + rowMsgLength);
			RawBuffer rawBuf = new RawBuffer(buffer, n);
			fileToBuild.newSegment(rawBuf);
		}
		fileToBuild.closeReceiving();
	}

	protected String getNameClean(String title) {
		if (title.startsWith("'"))
			return title.substring(1, title.length() - 1);
		else
			return title;
	}

	@Override
	protected void savePhotoAndData(String sensorData) {
		// TODO da implementare, associa le informazioni dei sensori
		// (sensorData) alla foto appena ricevuta
		PrintWriter out = null;
		try {
			out = new PrintWriter(new BufferedWriter(new FileWriter(log, true)));
			out.println(new File(lastPhotoPath).getName() + "=" + sensorData);
			out.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
