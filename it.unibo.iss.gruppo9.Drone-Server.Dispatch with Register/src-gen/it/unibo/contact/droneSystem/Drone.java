package it.unibo.contact.droneSystem;

import java.util.ArrayList;
import it.unibo.contact.platformuv.RunTimeKb;
import it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister.ApplParams;
import it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister.InvalidSpeedCmdException;
import it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister.SDAddress;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;


public class Drone extends DroneSupport{
	
	long lastUpdate;
	long currentTime;
	double timeElapsed;
	ArrayList<SDAddress> smartDevices;

	public Drone(String name) throws Exception {
		super(name);
		smartDevices = new ArrayList<SDAddress>();
	}

	@Override
	protected int getNFromMsg(String msg) throws Exception {
		Struct msgt = (Struct)Term.createTerm( msg );
		return Integer.parseInt(""+msgt.getArg(0));
	}

	@Override
	protected String getCmdFromMsg(String msg) throws Exception {
		Struct msgt = (Struct)Term.createTerm( msg );
		return msgt.getName();
	}

	@Override
	protected int modifySpeed(String cmd, int actualSpeed, int deltaSpeed)
			throws Exception {
		if(cmd.equals(ApplParams.INC)){
			actualSpeed += deltaSpeed;
			return actualSpeed;
		}
			if(cmd.equals(ApplParams.DEC)){
			actualSpeed -= deltaSpeed;
			return actualSpeed;
		}
		throw new InvalidSpeedCmdException(cmd);
	}
	
	@Override
	protected void setStartParams() throws Exception {
		lastUpdate=System.currentTimeMillis();
		lat= ApplParams.INITIAL_LATITUDE;
		lon= ApplParams.INITIAL_LONGITUDE;
	}
	
	@Override
	protected void calcSensors() throws Exception {
		currentTime = System.currentTimeMillis();
		timeElapsed=((double)(currentTime - lastUpdate))/1000/3600;
		fuel = fuel - (actualSpeed * 30 * timeElapsed);
		distance = distance + actualSpeed * timeElapsed;
		lon = lon + actualSpeed * timeElapsed / 111.319;
		lastUpdate=currentTime;
	}

	@Override
	protected void delay(int dt) throws Exception {
		Thread.sleep(dt);		
	}

	@Override
	protected void registerSD(String sdInfo) throws Exception {
		String [] infos = sdInfo.substring(sdInfo.indexOf('(')+1, sdInfo.indexOf(')')).split(",");
		
		SDAddress sd = new SDAddress("sd"+smartDevices.size(),infos[0],infos[1]);
		smartDevices.add(sd);
		
		curRequest.replyToCaller(sd.getName());
		
		RunTimeKb.addSubject("TCP" , sd.getName() , "notifyMissionStarted", sd.getIp(), sd.getPortNo()+1 );
		RunTimeKb.addSubject("TCP" , sd.getName() , "sensorDataSD", sd.getIp(), sd.getPortNo() );		
	}
	
	@Override
	protected void sendDataToSD(String data) throws Exception {
		for(int i=0; i<smartDevices.size(); i++){
			hl_drone_forward_sensorDataSD_smartDevice(data, smartDevices.get(i).getName());
		}		
	}

	@Override
	protected void notifySD() throws Exception {
		for(int i=0; i<smartDevices.size(); i++){
			hl_drone_forward_notifyMissionStarted_smartDevice(ApplParams.START, smartDevices.get(i).getName());
		}		
	}

	
	
}
