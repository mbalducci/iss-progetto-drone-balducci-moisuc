package it.unibo.contact.droneSystem;

import java.io.File;
import it.unibo.contact.platformuv.ContentSegmentable;
import it.unibo.is.interfaces.protocols.IConnInteraction;
import it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister.DroneCamera;

public class MDrone extends MDroneSupport {

	DroneCamera camera = null;

	public MDrone(String name) throws Exception {
		super(name);
		this.camera = new DroneCamera(missionInputFolder, fileNameToSend,
				fileFormatToSend);
	}

	public void takePhoto(String infos){
		camera.takePhoto(infos);
	}
	
	@Override
	protected void delayM(int DTF) throws Exception {
		Thread.sleep(DTF);
	}

	protected void sendTheFileContent(ContentSegmentable segmentableSender,
			IConnInteraction supportRaw) throws Exception {
		byte[] segm = segmentableSender.nextSegment();
		while (segm != null) {
			showMsg("Source sending ... " + segm.length);
			supportRaw.sendRaw(segm);
			segm = segmentableSender.nextSegment();
		}
		segmentableSender.closeSending();
		delayM(250); // The receiver must commute in
	}

	@Override
	protected void createMissionInputFolder(String missionInputFolder)
			throws Exception {
		try {
			File f = new File(missionInputFolder); 
		f.mkdirs();
//		System.out.println("Created missionInputFolder: "+f.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
