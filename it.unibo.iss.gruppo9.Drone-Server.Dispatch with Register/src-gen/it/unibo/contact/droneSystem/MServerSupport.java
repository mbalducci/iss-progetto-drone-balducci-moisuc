/*
*  Generated by AN Unibo
*/
package it.unibo.contact.droneSystem;
//Import generated by the contact spec
 	import  it.unibo.contact.platformuv.ContentSegmentable  ;
 	import  it.unibo.contact.platformuv.TcpOut  ;
 	import  it.unibo.is.interfaces.IContentSegmentable  ;
 	import  it.unibo.is.interfaces.platforms.ILindaLike  ;
 	import  it.unibo.is.interfaces.protocols.IConnInteraction  ;
 	import  java.io.FileInputStream  ;
 	import  org.eclipse.xtext.xbase.lib.StringExtensions  ;
//Other Import
import it.unibo.contact.platformuv.*;
import it.unibo.is.interfaces.*;
import it.unibo.is.interfaces.platforms.*;
//import org.eclipse.xtext.xbase.lib.*;
//import org.eclipse.xtext.xbase.lib.Functions.*;
import java.util.Vector;
import it.unibo.contact.platformuv.LindaLike;
import it.unibo.is.interfaces.protocols.IConnInteraction;
//import java.awt.Color;
//For Xbase code 
import org.eclipse.xtext.xbase.lib.Functions.*;
import org.eclipse.xtext.xbase.lib.*;

public abstract class MServerSupport extends Subject{
	private static MServer obj = null;
	private IMessage resCheckMsg;
	private boolean resCheck;
	/*
	* Factory method: returns a singleton
	*/
	public static MServer create(String name) throws Exception{
		if( obj == null ) obj = new MServer(name);
		return obj;
	}
	/* -------------------------------------
	* Local state of the subject
	* --------------------------------------
	*/
	protected int lastMsgNum = 0;
	
	
	//Constructor
	public MServerSupport(String name) throws Exception{
		super(name);
	 	isMultiInput=true;
	 	inputMessageList=new String[]{"dronePhoto","mServerStart","mServerStop", "endSelectInput"};
	 	initLastMsgRdMemo();  //put in initGui since the name must be set
		//Singleton
		if( obj != null ) return;
		 obj = (MServer)this;
	}
	
	/* -------------------------------------
	* Init
	* --------------------------------------
	*/
	//PREPARE INPUT THREADS
	public void initInputSupports() throws Exception{
	PlatformExpert.findInSupport( getName(), "dronePhoto" ,true,view);
	}
	
 	protected void initLastMsgRdMemo(){
 			lastMsgRdMemo.put("dronePhoto"+getName(),0);
 			lastMsgRdMemo.put("mServerStart"+getName(),0);
 			lastMsgRdMemo.put("mServerStop"+getName(),0);
 	}
	protected void initGui(){
		if( env != null ) view = env.getOutputView();
	    initLastMsgRdMemo(); //put here since the name must be set
	}
	
	/* -------------------------------------
	* State-based Behavior
	* -------------------------------------- 
	*/ 
	protected abstract int getFileSizeAndSetName(java.lang.String msg) throws Exception;
	protected abstract it.unibo.is.interfaces.IContentSegmentable createFileToBuild(java.lang.String fileDestPath) throws Exception;
	protected abstract void buildTheFileFromRaw() throws Exception;
	protected abstract void savePhotoAndData(java.lang.String sensorData) throws Exception;
	protected abstract void createMissionFolder(java.lang.String missionOutputFolder) throws Exception;
	/* --- USER DEFINED STATE ACTIONS --- */
	protected void setSupportIn() throws Exception{
	
	supportIn =RunTimeKb.getSubjectInConnectionSupport(getName() ,"","dronePhoto") ;
	}
	/* --- USER DEFINED TASKS --- */
	/* 
		Each state acquires some input and performs some action 
		Each state is mapped into a void method 
	*/
	//Variable behavior declarations
	protected 
	String rMsg = "";
	protected 
	String sensorData = "";
	protected 
	String fileNameToReceive = "";
	protected 
	String recFileName = "";
	protected 
	int rowMsgLength = 0;
	protected 
	IContentSegmentable fileToBuild = null;
	protected 
	IConnInteraction supportIn = null;
	protected 
	String outputFolder = "";
	protected 
	String missionOutputFolder = "";
	protected 
	int missionCounter = 0;
	protected 
	int photoCounter = 0;
	public  it.unibo.is.interfaces.IContentSegmentable get_fileToBuild(){ return fileToBuild; }
	public  it.unibo.is.interfaces.protocols.IConnInteraction get_supportIn(){ return supportIn; }
	
	protected boolean endStateControl = false;
	protected String curstate ="mServerInit";
	protected void stateControl( ) throws Exception{
		boolean debugMode = System.getProperty("debugMode" ) != null;
	 		while( ! endStateControl ){
	 			//DEBUG 
	 			if(debugMode) debugNextState(); 
	 			//END DEBUG
			/* REQUIRES Java Compiler 1.7
			switch( curstate ){
				case "mServerInit" : mServerInit(); break; 
				case "mServerReady" : mServerReady(); break; 
				case "mServerNewMission" : mServerNewMission(); break; 
				case "mServerWorking" : mServerWorking(); break; 
				case "mServerReceive" : mServerReceive(); break; 
				case "mServerEndError" : mServerEndError(); break; 
				case "mServerStopNormally" : mServerStopNormally(); break; 
			}//switch	
			*/
			if( curstate.equals("mServerInit")){ mServerInit(); }
			else if( curstate.equals("mServerReady")){ mServerReady(); }
			else if( curstate.equals("mServerNewMission")){ mServerNewMission(); }
			else if( curstate.equals("mServerWorking")){ mServerWorking(); }
			else if( curstate.equals("mServerReceive")){ mServerReceive(); }
			else if( curstate.equals("mServerEndError")){ mServerEndError(); }
			else if( curstate.equals("mServerStopNormally")){ mServerStopNormally(); }
		}//while
		//DEBUG 
		//if( synch != null ) synch.add(getName()+" reached the end of stateControl loop"  );
	 	}
	 	protected void selectInput(boolean mostRecent, Vector<String> tempList) throws Exception{
		Vector<IMessage> queries=comSup.prepareInput(mostRecent,getName(),
				SysKb.getSyskb(),tempList.toArray(),InteractPolicy.nopolicy() );
		//showMsg("*** queries" + queries);
		curInputMsg = selectOneInput(mostRecent,queries);	
		curInputMsgContent = curInputMsg.msgContent();	
	}
	
	protected void mServerReady()  throws Exception{
		
		showMsg("Ready to new start");
		/* --- TRANSITION TO NEXT STATE --- */
		Vector<String> tempList=new Vector<String>();
		tempList.add("mServerStart");
		 		if( tempList.size()==0){
					resetCurVars();
					do_terminationState();
					endStateControl=true;
					return;
				}
		selectInput(false,tempList);
		if(curInputMsg.msgId().equals("mServerStart")){ 
		curstate = "mServerNewMission";
		return;
		}//if curInputMsg mServerStart
	}
	protected void mServerNewMission()  throws Exception{
		
		missionCounter =missionCounter+1;
		photoCounter =0;
		missionOutputFolder =outputFolder+missionCounter+"/";
		createMissionFolder( missionOutputFolder );curstate = "mServerWorking"; 
		//resetCurVars(); //leave the current values on
		return;
		/* --- TRANSITION TO NEXT STATE --- */
	}
	protected void mServerWorking()  throws Exception{
		
		/* --- TRANSITION TO NEXT STATE --- */
		Vector<String> tempList=new Vector<String>();
		tempList.add("mServerStop");
		tempList.add("dronePhoto");
		 		if( tempList.size()==0){
					resetCurVars();
					do_terminationState();
					endStateControl=true;
					return;
				}
		selectInput(false,tempList);
		if(curInputMsg.msgId().equals("mServerStop")){ 
		curstate = "mServerStopNormally";
		return;
		}//if curInputMsg mServerStop
		if(curInputMsg.msgId().equals("dronePhoto")){ 
		curstate = "mServerReceive";
		return;
		}//if curInputMsg dronePhoto
	}
	protected void mServerReceive()  throws Exception{
		try{
		
		photoCounter =photoCounter+1;
		rMsg =curInputMsgContent;
		showMsg("> "+rMsg);
		curRequest.replyToCaller( "ready" ); 
		rowMsgLength =getFileSizeAndSetName( rMsg ) ;
		showMsg("acquire raw content "+rowMsgLength);
		recFileName =missionOutputFolder+photoCounter+"_"+it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister.ApplParams.prefix() +fileNameToReceive;
		fileToBuild =createFileToBuild( recFileName ) ;
		setSupportIn(  );showMsg("supportIn "+supportIn);
		buildTheFileFromRaw(  ); curRequest=hl_mServer_grant_dronePhoto();
		 curInputMsg= curRequest.getReceivedMessage();
		 curInputMsgContent = curInputMsg.msgContent();
		sensorData =curInputMsgContent;
		showMsg("Received photo and data "+sensorData);
		savePhotoAndData( sensorData );showMsg("closing channel");
		curRequest.replyToCaller( "bye bye" ); 
		showMsg("--------------------------------------------");
		curstate = "mServerWorking"; 
		//resetCurVars(); //leave the current values on
		return;
		/* --- TRANSITION TO NEXT STATE --- */
		}catch(Exception e){
		//onFault
		curException = e; 
		mServerEndError(); 
		}
	}	
	protected void mServerEndError()  throws Exception{
		
		showMsg("ERROR ... "+curException);
		/* --- TRANSITION TO NEXT STATE --- */
		resetCurVars();
		do_terminationState();
		endStateControl=true;
	}
	protected void mServerStopNormally()  throws Exception{
		
		showMsg("mServer END");
		showMsg("-------------------------------------------------");
		curstate = "mServerReady"; 
		//resetCurVars(); //leave the current values on
		return;
		/* --- TRANSITION TO NEXT STATE --- */
	}
	protected void mServerInit()  throws Exception{
		
		outputFolder =it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister.ApplParams.MISSION_OUT_FOLDER;
		showMsg("mServer ON");
		curstate = "mServerReady"; 
		//resetCurVars(); //leave the current values on
		return;
		/* --- TRANSITION TO NEXT STATE --- */
	}
	
   	
 	/* -------------------------------------
	* COMMUNICATION CORE OPERATIONS for mServer
	* --------------------------------------
	*/
 
	protected IMessageAndContext hl_mServer_grant_dronePhoto(   ) throws Exception {
	//EXPERT for COMPOSED mServer_grant_dronePhoto isInput=true withAnswer=true applVisible=true
	IMessageAndContext answer = comSup.inOut(
	getName() ,"dronePhoto", 
	"mServer_dronePhoto(ANYx1y2,dronePhoto,M,N)" ); 
	return answer;
	
	}
	
	protected IMessage hl_mServer_accept_mServerStart(   ) throws Exception {
	//EXPERT for COMPOSED mServer_accept_mServerStart isInput=true withAnswer=true applVisible=false
	IMessage answer = comSup.inOutAck(
	getName() ,"mServerStart", 
	"mServer_mServerStart(ANYx1y2,mServerStart,M,N)" ); 
	return answer;
	
	}
	
	protected IMessage hl_mServer_accept_mServerStop(   ) throws Exception {
	//EXPERT for COMPOSED mServer_accept_mServerStop isInput=true withAnswer=true applVisible=false
	IMessage answer = comSup.inOutAck(
	getName() ,"mServerStop", 
	"mServer_mServerStop(ANYx1y2,mServerStop,M,N)" ); 
	return answer;
	
	}
	
	
 	/* -------------------------------------
	* CONNECTION OPERATIONS for mServer
	* --------------------------------------
	*/
	
	/* -------------------------------------
	* Local body of the subject
	* --------------------------------------
	*/
	
		public void doJob() throws Exception{ stateControl(); }
	 	//INSERTED FOR DEBUG
		protected boolean nextStep = false;
		protected String stateBreakpoint = null;
		protected Vector<String> synch;
		protected synchronized void debugNextState() throws Exception{
			if( stateBreakpoint != null && ! curstate.equals(stateBreakpoint) ) return;
			while( stateBreakpoint != null && curstate.equals(stateBreakpoint) ){
				showMsg(" stateBreakpoint reached "  +  stateBreakpoint);
				synch.add("stateBreakpoint reached " + stateBreakpoint);
				//showMsg("wait");
	 			wait();			
			}
	//		if( stateBreakpoint != null   ) { //resumed!
	// 	 	stateBreakpoint = null;
	//			return;
			}
	//		while( ! nextStep ) wait();
	//		if( stateBreakpoint != null ) debugNextState();
	//		else{
	//			showMsg("resume nextStep");
	//			synch.add("nextStep done");
	//			nextStep = false;
	//		}
	//	}
	//	public synchronized void nextStateStep(Vector<String> synch) throws Exception{
	//		showMsg("nextStateStep " + curstate );
	//		this.synch = synch;
	//		nextStep = true;
	//		notifyAll();
	//	}
		public synchronized void nextStateStep(String state, Vector<String> synch) throws Exception{
			this.synch = synch;
			stateBreakpoint = state;
			nextStep = true;
			showMsg("nextStateStep stateBreakpoint=" + stateBreakpoint );
	 		notifyAll();
		}
		//END INSERTED FOR DEBUG
			
		protected void do_terminationState() throws Exception {
			//showMsg(  " ---- END STATE LOOP ---- " );
		}
	
	protected IMessage acquire(String msgId) throws Exception{
	  //showMsg("acquire "  +  msgId ); 
	  IMessage m;
	  //USER MESSAGES
	  if( msgId.equals("dronePhoto")){
	  	curRequest = hl_mServer_grant_dronePhoto();
	  	m = curRequest.getReceivedMessage();
	  	return m;		
	  }
	  if( msgId.equals("mServerStart")){
	  	m = hl_mServer_accept_mServerStart();
	  	return m;
	  }
	  if( msgId.equals("mServerStop")){
	  	m = hl_mServer_accept_mServerStop();
	  	return m;
	  }
	 if( msgId.equals("endSelectInput")){
	  String ms = MsgUtil.bm(MsgUtil.channelInWithPolicy(InteractPolicy.nopolicy(),getName(), "endSelectInput"), 
	    getName(), "endSelectInput", "ANYx1y2", "N");
	  //Serve the auto-dispatch
	  IMessage min = core.in(new Message(ms).toString());
	  return min;
	 }
	  throw new Exception("Wrong msgId:"+  msgId);
	}//acquire	
	
	/* -------------------------------------
	* Operations (from Java)
	* --------------------------------------
	*/
	
		
	/* -------------------------------------
	* Termination
	* --------------------------------------
	*/
	public void terminate() throws Exception{ //by EndSubjectConnections
		mServerGrant_dronePhotoEnd();
		mServerAccept_mServerStartEnd();
		mServerAccept_mServerStopEnd();
	 			 //Auto-forward a dispatch to finish selectInput operations
	 		    String ms =
	 		      MsgUtil.bm(MsgUtil.channelInWithPolicy(InteractPolicy.nopolicy(),getName(), "endSelectInput"), 
	 		       getName(), "endSelectInput", "endSelectInput", "0");
	 		    core.out(ms);
	if( synch != null ){
		synch.add(getName()+" reached the end of loop"  );
	}
	obj = null;
	//System.out.println(getName() + " terminated");
	}	
	// Teminate operations
	protected void mServerGrant_dronePhotoEnd() throws Exception{
	 	PlatformExpert.findInSupportToEnd(getName(),"dronePhoto",view );
		//showMsg("terminate mServerGrant_dronePhoto");
	}
	protected void mServerAccept_mServerStartEnd() throws Exception{
	 		PlatformExpert.findInSupportToEnd(getName(),"mServerStart",view );
		//showMsg("terminate mServerAccept_mServerStart");
	}
	protected void mServerAccept_mServerStopEnd() throws Exception{
	 		PlatformExpert.findInSupportToEnd(getName(),"mServerStop",view );
		//showMsg("terminate mServerAccept_mServerStop");
	}
}//MServerSupport
