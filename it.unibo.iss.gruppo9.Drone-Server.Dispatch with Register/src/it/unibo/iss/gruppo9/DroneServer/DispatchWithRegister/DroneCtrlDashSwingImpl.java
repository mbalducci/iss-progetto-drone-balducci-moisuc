package it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister;

import it.unibo.dronesystem.config.DroneConfig;
import it.unibo.dronesystem.data.factories.GaugeValueFactory;
import it.unibo.dronesystem.display.dashboards.implementors.swing.DashSwing;
import it.unibo.dronesystem.display.dashboards.implementors.swing.IDroneCtrlDashSwingImpl;
import it.unibo.dronesystem.display.displays.implementors.ICmdDisplayImplListener;
import it.unibo.dronesystem.display.displays.implementors.swing.CmdDisplaySwingImpl;
import it.unibo.dronesystem.display.displays.implementors.swing.GaugeDisplaySwingImpl;
import it.unibo.dronesystem.display.displays.implementors.swing.ICmdDisplaySwingImpl;
import it.unibo.dronesystem.display.displays.implementors.swing.IGaugeDisplaySwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.FuelometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.LocTrackerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.OdometerViewSwingImpl;
import it.unibo.dronesystem.display.views.implementors.swing.SpeedometerViewSwingImpl;

import java.awt.Color;
import java.math.BigDecimal;

/**
 * Project: dronesystem
 * <p/>
 * A {IDroneCtrlDashSwingImpl} implementation
 */
public class DroneCtrlDashSwingImpl extends DashSwing implements
    IDroneCtrlDashSwingImpl {
	
	CmdDisplaySwingImpl cmdDisplay;
	SpeedometerViewSwingImpl speedometerView;
	FuelometerViewSwingImpl fuelometerView;
	OdometerViewSwingImpl odometerView;
	LocTrackerViewSwingImpl locTrackerView;
	GaugeDisplaySwingImpl gaugeDisplay; //ci serve?
	
  /* { Constructors and factories */

	
	
	/*protected DroneCtrlDashSwingImpl(final String name,
      final IGaugeDisplaySwingImpl gaugeDisplay,
      final ICmdDisplaySwingImpl cmdDisplay) {

    super(name);

//int rowIdx, int colIdx, int rowCount, int colCount, double horizontalWeight, double verticalWeight
    this.addComponent(gaugeDisplay.getMainPanel(), 0, 0, 1, 1, 4.0, 4.0);
    this.addComponent(cmdDisplay.getMainPanel(), 1, 0, 1, 1, 1.0, 1.0);
    this.frame.setSize(DroneConfig.windowWidth, DroneConfig.windowHeight); //by AN
  }*/

  public DroneCtrlDashSwingImpl(String name) {
	super(name);
}

/*public static DroneCtrlDashSwingImpl create(final String name,
      final IGaugeDisplaySwingImpl gaugeDisplay,
      final ICmdDisplaySwingImpl cmdDisplay) {
	  System.out.println("%%% DroneCtrlDashSwingImpl"  );
    return new DroneCtrlDashSwingImpl(name, gaugeDisplay, cmdDisplay);
  }*/

  /* } */

  /* { {IDroneControlDashSwingImpl} implementation */

  /**
   * @see IDroneCtrlDashSwingImpl#start()
   */
  @Override
  public void start() {
    super.start();
  }

  /**
   * @see IDroneCtrlDashSwingImpl#stop()
   */
  @Override
  public void stop() {
    super.stop();
  }

  /* } */

  /* { Usage example */

  public void initComponents() {
    GaugeValueFactory gaugeValueFactory = new GaugeValueFactory();

    cmdDisplay = CmdDisplaySwingImpl.create("CmdDisplay");

    speedometerView = SpeedometerViewSwingImpl.create(
        "Speedometer", "kmh",
        gaugeValueFactory.createSpeed(BigDecimal.valueOf(2.0)),
        gaugeValueFactory.createSpeed(BigDecimal.valueOf(120.0)));

    fuelometerView = FuelometerViewSwingImpl.create(
        "Fuelometer", "l",
        gaugeValueFactory.createFuel(BigDecimal.valueOf(0)),
        gaugeValueFactory.createFuel(BigDecimal.valueOf(30)));

    odometerView = OdometerViewSwingImpl.create(
        "Odometer", "km");

    locTrackerView = LocTrackerViewSwingImpl
        .create("LocTracker");

    gaugeDisplay = GaugeDisplaySwingImpl.create(
        "Gauge Display", fuelometerView, speedometerView, odometerView,
        locTrackerView);

    this.addComponent(gaugeDisplay.getMainPanel(), 0, 0, 1, 1, 4.0, 4.0);
    this.addComponent(cmdDisplay.getMainPanel(), 1, 0, 1, 1, 1.0, 1.0);
    this.frame.setSize(DroneConfig.windowWidth, DroneConfig.windowHeight);
  }
  
  public void setCmdDisplayListener(ICmdDisplayImplListener listener){
	  cmdDisplay.register(listener);
  }
  
  public void updateSpeedometer(String value){
	  speedometerView.update(value);
  }
  
  public void updateFuelometer(String value){
	  fuelometerView.update(value);
  }
  
  public void updateLocTracker(String value){
	  locTrackerView.update(value);
  }
  
  public void updateOdometer(String value){
	  odometerView.update(value);
  }

  public void showMsgLabel(String msg){
	  cmdDisplay.getStatusLbl().setText(msg);
	  cmdDisplay.getStatusLbl().setForeground(Color.BLACK);
  }
  
  public void showErrorMsgLabel(String msg){
	  cmdDisplay.getStatusLbl().setText(msg);
	  cmdDisplay.getStatusLbl().setForeground(Color.RED);
  }
  
  /* } */

}
