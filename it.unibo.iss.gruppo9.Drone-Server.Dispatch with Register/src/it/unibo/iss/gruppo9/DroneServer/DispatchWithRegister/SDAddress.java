package it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister;

public class SDAddress {
	String ip;
	int portNo;
	String name;
	public SDAddress(String name, String ip, String portNo) {
		super();
		this.name=name;
		this.ip = ip;
		this.portNo = Integer.parseInt(portNo);
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPortNo() {
		return portNo;
	}
	public void setPortNo(int portNo) {
		this.portNo = portNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
