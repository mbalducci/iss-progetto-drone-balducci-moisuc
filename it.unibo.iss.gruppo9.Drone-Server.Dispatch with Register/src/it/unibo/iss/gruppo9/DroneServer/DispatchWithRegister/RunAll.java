package it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister;

import it.unibo.contact.droneSystem.CtxDroneMain;
import it.unibo.contact.droneSystem.CtxServerMain;
import it.unibo.contact.droneSystem.DroneSystemMain;

public class RunAll {

	public static void main(String[] args) {
		addExitMessage();
		startSystem(args);
		startDrone(args);
		startServer(args);

	}

	private static void addExitMessage() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				System.out.println("Application finished!");
			}
		});
	}

	private static void startSystem(final String[] args) {
		Thread systemThread = new Thread() {
			public void run() {
				try {
					DroneSystemMain.main(args);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		systemThread.start();
		System.out.println("System started.");
		insertPause(1000);

	}

	private static void startServer(final String[] args) {
		Thread serverThread = new Thread() {
			public void run() {
				try {
					CtxServerMain.main(args);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		serverThread.start();

		System.out.println("Server started.");
		insertPause(1000);
	}

	private static void startDrone(final String[] args) {
		Thread droneThread = new Thread() {
			public void run() {
				try {
					CtxDroneMain.main(args);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		droneThread.start();
		System.out.println("Drone started.");
	}

	private static void insertPause(int milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
