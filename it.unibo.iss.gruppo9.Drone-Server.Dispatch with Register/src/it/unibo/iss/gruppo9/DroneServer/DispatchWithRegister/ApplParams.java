package it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister;

public class ApplParams {

	public static final int MAX_SPEED = 120;
	public static final int MIN_SPEED = 60;
	public static final double MIN_FUEL = 0.5;
	public static final double INITIAL_FUEL = 30;
	
	public static double fuel(){ return INITIAL_FUEL;}
	
	public static final String INC = "inc";
	public static final String DEC = "dec";
	public static final int DELTA_SPEED = 10;	
	public static final String SETSPEED = "setSpeed";
	public static final String START = "start";
	public static final String STOP = "stop";
	
	public static final double INITIAL_LATITUDE = 41.89;
	public static final double INITIAL_LONGITUDE = 12.492;
	
	public static final String MISSION_IN_FOLDER = "input_on_drone/";
	public static final String MISSION_OUT_FOLDER = "output_on_server/mission_";
	
	public static final String prefix = "output";	
 	public static String prefix(){ return prefix; }
 	
 	public static final int missionNotificationId= 1234;

	
}
