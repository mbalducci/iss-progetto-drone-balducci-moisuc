package it.unibo.iss.gruppo9.DroneServer.DispatchWithRegister.viewer;

import java.io.File;

public class Info {

    private File photo;

    private String speed, fuel, lat, longit, alt, dist;

    public void setPhoto(File photo) {
        this.photo = photo;
    }

    public File getPhoto() {
        return photo;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public void setDist(String dist) {
        this.dist = dist;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setLongit(String longit) {
        this.longit = longit;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getAlt() {
        return alt;
    }

    public String getDist() {
        return dist;
    }

    public String getFuel() {
        return fuel;
    }

    public String getLat() {
        return lat;
    }

    public String getLongit() {
        return longit;
    }

    public String getSpeed() {
        return speed;
    }

}
