# Drone mission #

[vedi specifiche - pdf](/mbalducci/iss-progetto-drone-balducci-moisuc/src/73422460a9281ddddc9321819ff091a421ba8bb2/TemaFinaleLMCE2012.pdf?at=master): file pdf fornito dal comittente

[vedi report finale - Pdf](https://bitbucket.org/mbalducci/iss-progetto-drone-balducci-moisuc/src/5b7e3d153ecf358b6150c52dc19b9014db612857/Report_Balducci_Moisuc.pdf) report finale da consegnare al committente


### Nota ###
L'applicazione Android necessita di una versione di Android API 11+.

### Altre risorse ###
[vedi report - GDocs](https://docs.google.com/document/d/1bZf4ptXYU4RM5VSmJseaAuXxIXsnDac9rToOCe8YeZQ/edit): versione del report usato nella fase di sviluppo

[vedi report finale - ShareLaTeX](https://it.sharelatex.com/project/542d4e944eb5b9c77255b87b): versione latex del report finale

### Autori ###

* Balducci Mattia
* Moisuc Cornel