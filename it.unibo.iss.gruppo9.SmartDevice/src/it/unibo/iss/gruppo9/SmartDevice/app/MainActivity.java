package it.unibo.iss.gruppo9.SmartDevice.app;


import it.unibo.contact.droneSystem.SmartDevicePlatform;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.iss.gruppo9.DroneServer.ApplParams;
import it.unibo.iss.gruppo9.SmartDevice.app.R;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	TextView myView;
	IOutputView myAndroidView;
	SmartDeviceDashboard gaugeDisplay;
	private SmartDevicePlatform droneSystem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dronelayout);

		// Customize SpeedometerView
		SpeedometerView speedometer = (SpeedometerView) findViewById(R.id.speedometer);

		// Add label converter
		speedometer.setLabelConverter(new SpeedometerView.LabelConverter() {
			@Override
			public String getLabelFor(double progress, double maxProgress) {
				return String.valueOf((int) Math.round(progress));
			}
		});

		// configure value range and ticks
		speedometer.setMaxSpeed(120);
		speedometer.setMajorTickStep(20);
		speedometer.setMinorTicks(9);

		// Configure value range colors
		speedometer.addColoredRange(82, 93, Color.GREEN);
		speedometer.addColoredRange(93, 110, Color.YELLOW);
		speedometer.addColoredRange(110, 120, Color.RED);

		ProgressBar fuelmeter = (ProgressBar) findViewById(R.id.fuelmeter);
		fuelmeter.setMax(100);

		myView = (TextView) findViewById(R.id.output);
		gaugeDisplay = new SmartDeviceDashboard(speedometer, fuelmeter,
				(TextView) findViewById(R.id.latitudeLabel),
				(TextView) findViewById(R.id.longitudeLabel),
				(TextView) findViewById(R.id.distanceLabel));
		myAndroidView = new AndroidOutputView(myView, gaugeDisplay, this);

		droneSystem = new SmartDevicePlatform(myAndroidView);
		droneSystem.doJob();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		myAndroidView.addOutput("Resumed");
		// Clear all notifications
		NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		nMgr.cancel(ApplParams.missionNotificationId);
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
		// permette di riaprire l'applicazione senza che si blocchi
		droneSystem.terminate();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
