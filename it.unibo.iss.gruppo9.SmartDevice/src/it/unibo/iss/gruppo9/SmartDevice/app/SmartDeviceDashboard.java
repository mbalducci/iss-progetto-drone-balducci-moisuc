package it.unibo.iss.gruppo9.SmartDevice.app;

import android.widget.ProgressBar;
import android.widget.TextView;

public class SmartDeviceDashboard {	
	SpeedometerView speedometer;
	ProgressBar fuelmeter;
	TextView latitudeLabel; 
	TextView longitudeLabel;
	TextView distanceLabel;
	double lastSpeed=0;
	
	public SmartDeviceDashboard(SpeedometerView speedometer, ProgressBar fuelmeter, TextView latitudeLabel,
			TextView longitudeLabel, TextView distanceLabel) {
		super();
		this.speedometer = speedometer;
		this.fuelmeter = fuelmeter;
		this.latitudeLabel = latitudeLabel;
		this.longitudeLabel = longitudeLabel;
		this.distanceLabel = distanceLabel;
	}

	public void setSpeed(double speed){
		if (speed!=lastSpeed){
			speedometer.setSpeed(speed, true);
			lastSpeed = speed;
		}		
	}
	
	public void setFuel(int fuel) {
		fuelmeter.setProgress(fuel);
	}

	public void setLatitudeLabel(String latitude) {
		latitudeLabel.setText(latitude);
	}

	public void setLongitudeLabel(String longitude) {
		longitudeLabel.setText(longitude);
	}

	public void setDistanceLabel(String distance) {
		distanceLabel.setText(distance);
	}	
}
