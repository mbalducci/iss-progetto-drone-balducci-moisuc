/*
*  Generated by AN Unibo
*/
package it.unibo.contact.droneSystem;
//Import generated by the contact spec
 	import  it.unibo.contact.platformuv.ContentSegmentable  ;
 	import  it.unibo.contact.platformuv.TcpOut  ;
 	import  it.unibo.is.interfaces.IContentSegmentable  ;
 	import  it.unibo.is.interfaces.platforms.ILindaLike  ;
 	import  it.unibo.is.interfaces.protocols.IConnInteraction  ;
 	import  java.io.FileInputStream  ;
 	import  org.eclipse.xtext.xbase.lib.StringExtensions  ;
//Other Import
import it.unibo.contact.platformuv.*;
import it.unibo.is.interfaces.*;
import it.unibo.is.interfaces.platforms.*;
//import org.eclipse.xtext.xbase.lib.*;
//import org.eclipse.xtext.xbase.lib.Functions.*;
import java.util.Vector;
import it.unibo.contact.platformuv.LindaLike;
import it.unibo.is.interfaces.protocols.IConnInteraction;
//import java.awt.Color;
//For Xbase code 
import org.eclipse.xtext.xbase.lib.Functions.*;
import org.eclipse.xtext.xbase.lib.*;

public abstract class DroneCtrlDashboardSupport extends Subject{
	private static DroneCtrlDashboard obj = null;
	private IMessage resCheckMsg;
	private boolean resCheck;
	/*
	* Factory method: returns a singleton
	*/
	public static DroneCtrlDashboard create(String name) throws Exception{
		if( obj == null ) obj = new DroneCtrlDashboard(name);
		return obj;
	}
	/* -------------------------------------
	* Local state of the subject
	* --------------------------------------
	*/
	protected int lastMsgNum = 0;
	
	
	//Constructor
	public DroneCtrlDashboardSupport(String name) throws Exception{
		super(name);
	 	isMultiInput=true;
	 	inputMessageList=new String[]{"updateGui", "endSelectInput"};
	 	initLastMsgRdMemo();  //put in initGui since the name must be set
		//Singleton
		if( obj != null ) return;
		 obj = (DroneCtrlDashboard)this;
	}
	
	/* -------------------------------------
	* Init
	* --------------------------------------
	*/
	//PREPARE INPUT THREADS
	public void initInputSupports() throws Exception{
	}
	
 	protected void initLastMsgRdMemo(){
 			lastMsgRdMemo.put("updateGui"+getName(),0);
 	}
	protected void initGui(){
		if( env != null ) view = env.getOutputView();
	    initLastMsgRdMemo(); //put here since the name must be set
	}
	
	/* -------------------------------------
	* State-based Behavior
	* -------------------------------------- 
	*/ 
	protected abstract void initDashGui() throws Exception;
	protected abstract void updateGui(java.lang.String values) throws Exception;
	/* --- USER DEFINED STATE ACTIONS --- */
	/* --- USER DEFINED TASKS --- */
	/* 
		Each state acquires some input and performs some action 
		Each state is mapped into a void method 
	*/
	//Variable behavior declarations
	
	protected boolean endStateControl = false;
	protected String curstate ="droneCtrlDashboardInit";
	protected void stateControl( ) throws Exception{
		boolean debugMode = System.getProperty("debugMode" ) != null;
	 		while( ! endStateControl ){
	 			//DEBUG 
	 			if(debugMode) debugNextState(); 
	 			//END DEBUG
			/* REQUIRES Java Compiler 1.7
			switch( curstate ){
				case "droneCtrlDashboardInit" : droneCtrlDashboardInit(); break; 
				case "droneCtrlDashboardWorking" : droneCtrlDashboardWorking(); break; 
				case "droneCtrlDashboardUpdating" : droneCtrlDashboardUpdating(); break; 
				case "droneCtrlDashboardEndNormally" : droneCtrlDashboardEndNormally(); break; 
			}//switch	
			*/
			if( curstate.equals("droneCtrlDashboardInit")){ droneCtrlDashboardInit(); }
			else if( curstate.equals("droneCtrlDashboardWorking")){ droneCtrlDashboardWorking(); }
			else if( curstate.equals("droneCtrlDashboardUpdating")){ droneCtrlDashboardUpdating(); }
			else if( curstate.equals("droneCtrlDashboardEndNormally")){ droneCtrlDashboardEndNormally(); }
		}//while
		//DEBUG 
		//if( synch != null ) synch.add(getName()+" reached the end of stateControl loop"  );
	 	}
	 	protected void selectInput(boolean mostRecent, Vector<String> tempList) throws Exception{
		Vector<IMessage> queries=comSup.prepareInput(mostRecent,getName(),
				SysKb.getSyskb(),tempList.toArray(),InteractPolicy.nopolicy() );
		//showMsg("*** queries" + queries);
		curInputMsg = selectOneInput(mostRecent,queries);	
		curInputMsgContent = curInputMsg.msgContent();	
	}
	
	protected void droneCtrlDashboardWorking()  throws Exception{
		
		/* --- TRANSITION TO NEXT STATE --- */
		Vector<String> tempList=new Vector<String>();
		tempList.add("updateGui");
		 		if( tempList.size()==0){
					resetCurVars();
					do_terminationState();
					endStateControl=true;
					return;
				}
		selectInput(false,tempList);
		if(curInputMsg.msgId().equals("updateGui")){ 
		curstate = "droneCtrlDashboardUpdating";
		return;
		}//if curInputMsg updateGui
	}
	protected void droneCtrlDashboardUpdating()  throws Exception{
		
		updateGui( curInputMsgContent );curstate = "droneCtrlDashboardWorking"; 
		//resetCurVars(); //leave the current values on
		return;
		/* --- TRANSITION TO NEXT STATE --- */
	}
	protected void droneCtrlDashboardEndNormally()  throws Exception{
		
		showMsg("GUI reset");
		curstate = "droneCtrlDashboardInit"; 
		//resetCurVars(); //leave the current values on
		return;
		/* --- TRANSITION TO NEXT STATE --- */
	}
	protected void droneCtrlDashboardInit()  throws Exception{
		
		initDashGui(  );curstate = "droneCtrlDashboardWorking"; 
		//resetCurVars(); //leave the current values on
		return;
		/* --- TRANSITION TO NEXT STATE --- */
	}
	
   	
 	/* -------------------------------------
	* COMMUNICATION CORE OPERATIONS for droneCtrlDashboard
	* --------------------------------------
	*/
 
	protected IAcquireOneReply hl_droneCtrlDashboard_demand_guiEvent_server( String M  ) throws Exception {
	//EXPERT for COMPOSED droneCtrlDashboard_demand_guiEvent_server isInput=false withAnswer=true applVisible=true
	M = MsgUtil.putInEnvelope(M);
	IAcquireOneReply answer = comSup.outIn(
	"server","guiEvent",getName(), 
	"server_guiEvent("+getName()+",guiEvent,"+M+","+msgNum+")","droneCtrlDashboard_server_guiEvent(server,guiEvent,M,"+msgNum+")" ); 
	msgNum++;return answer;
	
	}
	
	protected IMessage hl_droneCtrlDashboard_serve_updateGui(   ) throws Exception {
	IMessage m = new Message("droneCtrlDashboard_updateGui(ANYx1y2,updateGui,M,N)");
	IMessage inMsg = comSup.inOnly( getName() ,"updateGui", m );
	
		return inMsg;
	
	}
	
	
 	/* -------------------------------------
	* CONNECTION OPERATIONS for droneCtrlDashboard
	* --------------------------------------
	*/
	
	/* -------------------------------------
	* Local body of the subject
	* --------------------------------------
	*/
	
		public void doJob() throws Exception{ stateControl(); }
	 	//INSERTED FOR DEBUG
		protected boolean nextStep = false;
		protected String stateBreakpoint = null;
		protected Vector<String> synch;
		protected synchronized void debugNextState() throws Exception{
			if( stateBreakpoint != null && ! curstate.equals(stateBreakpoint) ) return;
			while( stateBreakpoint != null && curstate.equals(stateBreakpoint) ){
				showMsg(" stateBreakpoint reached "  +  stateBreakpoint);
				synch.add("stateBreakpoint reached " + stateBreakpoint);
				//showMsg("wait");
	 			wait();			
			}
	//		if( stateBreakpoint != null   ) { //resumed!
	// 	 	stateBreakpoint = null;
	//			return;
			}
	//		while( ! nextStep ) wait();
	//		if( stateBreakpoint != null ) debugNextState();
	//		else{
	//			showMsg("resume nextStep");
	//			synch.add("nextStep done");
	//			nextStep = false;
	//		}
	//	}
	//	public synchronized void nextStateStep(Vector<String> synch) throws Exception{
	//		showMsg("nextStateStep " + curstate );
	//		this.synch = synch;
	//		nextStep = true;
	//		notifyAll();
	//	}
		public synchronized void nextStateStep(String state, Vector<String> synch) throws Exception{
			this.synch = synch;
			stateBreakpoint = state;
			nextStep = true;
			showMsg("nextStateStep stateBreakpoint=" + stateBreakpoint );
	 		notifyAll();
		}
		//END INSERTED FOR DEBUG
			
		protected void do_terminationState() throws Exception {
			//showMsg(  " ---- END STATE LOOP ---- " );
		}
	
	protected IMessage acquire(String msgId) throws Exception{
	  //showMsg("acquire "  +  msgId ); 
	  IMessage m;
	  //USER MESSAGES
	  if( msgId.equals("updateGui")){
	  	m = hl_droneCtrlDashboard_serve_updateGui();
	  	return m;
	  }
	 if( msgId.equals("endSelectInput")){
	  String ms = MsgUtil.bm(MsgUtil.channelInWithPolicy(InteractPolicy.nopolicy(),getName(), "endSelectInput"), 
	    getName(), "endSelectInput", "ANYx1y2", "N");
	  //Serve the auto-dispatch
	  IMessage min = core.in(new Message(ms).toString());
	  return min;
	 }
	  throw new Exception("Wrong msgId:"+  msgId);
	}//acquire	
	
	/* -------------------------------------
	* Operations (from Java)
	* --------------------------------------
	*/
	
		
	/* -------------------------------------
	* Termination
	* --------------------------------------
	*/
	public void terminate() throws Exception{ //by EndSubjectConnections
		droneCtrlDashboardDemand_guiEvent_serverEnd();droneCtrlDashboardServe_updateGuiEnd();
	 			 //Auto-forward a dispatch to finish selectInput operations
	 		    String ms =
	 		      MsgUtil.bm(MsgUtil.channelInWithPolicy(InteractPolicy.nopolicy(),getName(), "endSelectInput"), 
	 		       getName(), "endSelectInput", "endSelectInput", "0");
	 		    core.out(ms);
	if( synch != null ){
		synch.add(getName()+" reached the end of loop"  );
	}
	obj = null;
	//System.out.println(getName() + " terminated");
	}	
	// Teminate operations
	protected void droneCtrlDashboardDemand_guiEvent_serverEnd() throws Exception{
	 		PlatformExpert.findOutSupportToEnd("droneCtrlDashboard","guiEvent",getName(),view );
		//showMsg("terminate droneCtrlDashboardDemand_guiEvent_server");
	}	
	protected void droneCtrlDashboardServe_updateGuiEnd() throws Exception{
	 		PlatformExpert.findInSupportToEnd(getName(),"updateGui",view );
		//showMsg("terminate droneCtrlDashboardServe_updateGui");
	}
}//DroneCtrlDashboardSupport
