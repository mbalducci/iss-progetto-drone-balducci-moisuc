package it.unibo.contact.droneSystem;

import it.unibo.is.interfaces.IOutputView;

public class SmartDevice extends SmartDeviceSupport {

	public SmartDevice(String name) throws Exception {
		super(name);
	}
	
	@Override
	protected void addOutput(String data) throws Exception {
		view.addOutput(data);
	}

	@Override
	protected void updateSDGui(String data) throws Exception {
		view.setOutput(data);
	}
	
	protected void showNotification(String data) throws Exception {
		view.setOutput(data);
	}

	public void setView(IOutputView newView){
		this.view = newView;
	}	
}
