package it.unibo.contact.droneSystem;

import it.unibo.iss.gruppo9.DroneServer.ApplParams;
import it.unibo.iss.gruppo9.DroneServer.InvalidSpeedCmdException;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;

public class Drone extends DroneSupport{
	
	long lastUpdate;
	long currentTime;
	double timeElapsed;	

	public Drone(String name) throws Exception {
		super(name);
	}

	@Override
	protected int getNFromMsg(String msg) throws Exception {
		Struct msgt = (Struct)Term.createTerm( msg );
		return Integer.parseInt(""+msgt.getArg(0));
	}

	@Override
	protected String getCmdFromMsg(String msg) throws Exception {
		Struct msgt = (Struct)Term.createTerm( msg );
		return msgt.getName();
	}

	// modifica la velocit� corrente a seconda che il comando ricevuto sia inc o dec
	@Override
	protected int modifySpeed(String cmd, int actualSpeed, int deltaSpeed)
			throws Exception {
		if(cmd.equals(ApplParams.INC)){
			actualSpeed += deltaSpeed;
			return actualSpeed;
		}
			if(cmd.equals(ApplParams.DEC)){
			actualSpeed -= deltaSpeed;
			return actualSpeed;
		}
		throw new InvalidSpeedCmdException(cmd);
	}
	
	// inizializza i parametri della simulazione
	@Override
	protected void setStartParams() throws Exception {
		lastUpdate=System.currentTimeMillis();
		fuel = ApplParams.INITIAL_FUEL;
		lat= ApplParams.INITIAL_LATITUDE;
		lon= ApplParams.INITIAL_LONGITUDE;
		alt= ApplParams.INITIAL_ALTITUDE;
		actualSpeed = targetSpeed;
		sensorDTF = ApplParams.SENSOR_SEND_INTERVAL;
	}
	
	// simula il funzionamento dei sensori calcolandone i valori
	@Override
	protected void calcSensors() throws Exception {
		currentTime = System.currentTimeMillis();
		timeElapsed=((double)(currentTime - lastUpdate))/1000/3600;
		fuel = fuel - (actualSpeed * 30 * timeElapsed);
		distance = distance + actualSpeed * timeElapsed;
		lon = lon + actualSpeed * timeElapsed / 111.319;
		lastUpdate=currentTime;
	}

	@Override
	protected void delay(int dt) throws Exception {
		Thread.sleep(dt);		
	}	
}
