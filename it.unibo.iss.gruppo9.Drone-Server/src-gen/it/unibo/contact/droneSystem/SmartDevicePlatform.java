package it.unibo.contact.droneSystem;

import it.unibo.is.interfaces.IOutputView;

public class SmartDevicePlatform extends CtxSmartDeviceMain {
	private IOutputView newView;

	public SmartDevicePlatform(IOutputView newView) {
		super();
		this.newView = newView;
	}

	@Override
	public void doJob() {
		initProperty();
		init();
		configure();
		setNewView();
		System.out.println("Da qui");
		start();
		System.out.println("A qui");
	}

	public void setNewView() {
		try {
			get_smartDevice().setView(newView);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
