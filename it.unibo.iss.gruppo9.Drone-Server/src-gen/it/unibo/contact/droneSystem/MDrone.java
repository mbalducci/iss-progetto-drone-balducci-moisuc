package it.unibo.contact.droneSystem;

import java.io.File;
import it.unibo.contact.platformuv.ContentSegmentable;
import it.unibo.is.interfaces.protocols.IConnInteraction;
import it.unibo.iss.gruppo9.DroneServer.ApplParams;
import it.unibo.iss.gruppo9.DroneServer.DroneCamera;

public class MDrone extends MDroneSupport {

	private DroneCamera camera = null;
	private long lastTime = 0;
	private long endTime;

	public MDrone(String name) throws Exception {
		super(name);		
	}

	// scatta una foto
	public void takePhoto(String infos){
		camera.takePhoto(infos);
	}
	
	@Override
	protected void delayM(int DTF) throws Exception {
		if (lastTime==0){
			lastTime = System.currentTimeMillis();
		}
		endTime = lastTime + DTF;
		if (System.currentTimeMillis() < endTime) {
			Thread.sleep(endTime - System.currentTimeMillis());
		}
		lastTime = System.currentTimeMillis();
	}

	// invia la foto scomposta in pacchetti
	protected void sendTheFileContent(ContentSegmentable segmentableSender,
			IConnInteraction supportRaw) throws Exception {
		byte[] segm = segmentableSender.nextSegment();
		while (segm != null) {
			showMsg("Source sending ... " + segm.length);
			supportRaw.sendRaw(segm);
			segm = segmentableSender.nextSegment();
		}
		segmentableSender.closeSending();
		Thread.sleep(250); 
	}

	//crea la cartella per le foto relativa alla missione corrente
	protected void createMissionInputFolder(String missionInputFolder)
			throws Exception {
		try {
			File f = new File(missionInputFolder); 
			f.mkdirs();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// inizializza i parametri di mDrone
	@Override
	protected void initMDroneParams() throws Exception {
		fileNameToSend = ApplParams.FILE_NAME_TO_SEND;
		fileFormatToSend = ApplParams.FILE_FORMAT_TO_SEND;
		DTF = ApplParams.PHOTO_SEND_INTERVAL;
		createMissionInputFolder(ApplParams.MISSION_IN_FOLDER);
		camera = new DroneCamera(missionInputFolder, fileNameToSend, fileFormatToSend);
	}
}
