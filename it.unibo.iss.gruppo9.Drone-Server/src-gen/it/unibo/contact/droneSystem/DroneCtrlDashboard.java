package it.unibo.contact.droneSystem;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.dronesystem.display.displays.InvalidInputError;
import it.unibo.dronesystem.display.displays.implementors.ICmdDisplayImplListener;
import it.unibo.iss.gruppo9.DroneServer.ApplParams;
import it.unibo.iss.gruppo9.DroneServer.DroneCtrlDashSwingImpl;

public class DroneCtrlDashboard extends DroneCtrlDashboardSupport implements ICmdDisplayImplListener{

	DroneCtrlDashSwingImpl serverGui;
	
	public DroneCtrlDashboard(String name) throws Exception {
		super(name);
	}
	
	@Override
	protected void initDashGui() {
		serverGui = new DroneCtrlDashSwingImpl("DroneCtrlDashboard");
		serverGui.initComponents();
		serverGui.setCmdDisplayListener(this);
		serverGui.start();
		System.out.println("Server GUI started.");		
	}
	
	/* data una stringa contenente tutti i valori ricevuti dal drone
	 * 		aggiorna i relativi gauges
	 */
	protected void updateGui(String values){
		Struct msgt = (Struct)Term.createTerm( values );
		String speed = ""+msgt.getArg(0);
		String fuel = ""+msgt.getArg(1);
		String lat = ""+msgt.getArg(2);
		String lon = ""+msgt.getArg(3);
		String alt = ""+msgt.getArg(4);
		String dist = ""+msgt.getArg(5);
		serverGui.updateSpeedometer(speed);
		serverGui.updateFuelometer(fuel);
		serverGui.updateLocTracker("["+lat+","+lon+"]");
		serverGui.updateOdometer(dist);		
	}

	// invia al server un messaggio contentente il comando inc
	//    attende la risposta con l'esito dell'operazione
	@Override
	public void onIncSpeed() {
		try {
			curAcquireOneReply=hl_droneCtrlDashboard_demand_guiEvent_server(
					ApplParams.INC+"("+ApplParams.DELTA_SPEED+")");
			curReply=curAcquireOneReply.acquireReply(500);
			showLabelMsg(curReply.msgContent());
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	// invia al server un messaggio contentente il comando dec
	//    attende la risposta con l'esito dell'operazione
	@Override
	public void onDecSpeed() {
		try {
			curAcquireOneReply=hl_droneCtrlDashboard_demand_guiEvent_server(
					ApplParams.DEC+"("+ApplParams.DELTA_SPEED+")");
			curReply=curAcquireOneReply.acquireReply(500);
			showLabelMsg(curReply.msgContent());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	// invia al server un messaggio contentente il comando setSpeed
	//    attende la risposta con l'esito dell'operazione
	@Override
	public void onSetSpeed(String speedValueDefRep) throws InvalidInputError {
		try {			
			curAcquireOneReply=hl_droneCtrlDashboard_demand_guiEvent_server(ApplParams.SETSPEED+"("+speedValueDefRep+")");
			curReply=curAcquireOneReply.acquireReply(500);
			showLabelMsg(curReply.msgContent());
		} catch (Exception e) {
			e.printStackTrace();
		}			
	}

	// invia al server un messaggio contentente il comando start
	//    attende la risposta con l'esito dell'operazione
	@Override
	public void onStart() {
		try {
			curAcquireOneReply=hl_droneCtrlDashboard_demand_guiEvent_server(ApplParams.START+"()");
			curReply=curAcquireOneReply.acquireReply(500);
			showLabelMsg(curReply.msgContent());
		} catch (Exception e) {
			e.printStackTrace();
		}			
	}

	// invia al server un messaggio contentente il comando stop
	//    attende la risposta con l'esito dell'operazione
	@Override
	public void onStop() {
		try {
			curAcquireOneReply=hl_droneCtrlDashboard_demand_guiEvent_server(ApplParams.STOP+"()");
			curReply=curAcquireOneReply.acquireReply(500);
			showLabelMsg(curReply.msgContent());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// mostra un messaggio sulla GUI
	private void showLabelMsg(String msg){
		if(msg.contains("Error")){
			serverGui.showErrorMsgLabel(msg);
		}
		else{
			serverGui.showMsgLabel(msg);
		}
	}
}
