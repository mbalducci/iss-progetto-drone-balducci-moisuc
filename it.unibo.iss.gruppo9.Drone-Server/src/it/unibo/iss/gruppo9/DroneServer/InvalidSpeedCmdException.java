package it.unibo.iss.gruppo9.DroneServer;

public class InvalidSpeedCmdException extends RuntimeException {

	/**
	 * Eccezione generata quando una richiesta di modifica velocit� non � ne una
	 * "inc" ne una "dec"
	 */
	private static final long serialVersionUID = 1L;

	public InvalidSpeedCmdException(String cmd) {
		super("Speed cmd ricevuto: " + cmd);
	}

}
