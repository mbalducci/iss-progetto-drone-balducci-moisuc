package it.unibo.iss.gruppo9.DroneServer;

import it.unibo.contact.droneSystem.CtxDroneMain;
import it.unibo.contact.droneSystem.CtxServerMain;
import it.unibo.contact.droneSystem.DroneSystemMain;
import it.unibo.contact.updaterServer.UpdaterServerMain;

public class RunAll {

	public static void main(String[] args) throws InterruptedException {
		Thread t = new Thread() {
			public void run() {
				try {
					UpdaterServerMain.main(null);
					System.out.println("Server Updater started.");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		t.start();
		t.join();
		Thread.sleep(1000);
		t = new Thread() {
			public void run() {
				try {
					DroneSystemMain.main(null);
					System.out.println("System started.");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		t.start();
		t.join();
		Thread.sleep(1000);
		t = new Thread() {
			public void run() {
				try {
					CtxServerMain.main(null);
					System.out.println("Server started.");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		t.start();
		t.join();
		Thread.sleep(1000);
		t = new Thread() {
			public void run() {
				try {
					CtxDroneMain.main(null);
					System.out.println("Drone started.");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		t.start();
		t.join();
		Thread.sleep(1000);
	}

}

class RunSystem {
	public static void main(String[] args) throws InterruptedException {
		new Thread() {
			public void run() {
				try {
					UpdaterServerMain.main(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
}

class RunDroneSystem {
	public static void main(String[] args) throws InterruptedException {
		new Thread() {
			public void run() {
				try {
					DroneSystemMain.main(null);
					System.out.println("System started.");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
}

class RunCtxServer {
	public static void main(String[] args) throws InterruptedException {
		new Thread() {
			public void run() {
				try {
					CtxServerMain.main(null);
					System.out.println("Server started.");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
}

class RunCtxDrone {
	public static void main(String[] args) throws InterruptedException {
		new Thread() {
			public void run() {
				try {
					CtxDroneMain.main(null);
					System.out.println("Drone started.");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
}