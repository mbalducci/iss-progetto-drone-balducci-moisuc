package it.unibo.iss.gruppo9.DroneServer.viewer;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;

public class DroneSystemViewer extends javax.swing.JFrame {

    public DroneSystemViewer() {
//        System.out.println("path: "+System.getProperty("user.dir"));
        initComponents();
//        Window.pack();
//        setSize(new java.awt.Dimension(725, 530));
        
//        setLocationRelativeTo(null);
//        setExtendedState(NORMAL);
//Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
//this.setLocation(100,100);//dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        jListMissionsModel = ((DefaultListModel) this.jListMissions.getModel());
        jListPhotosModel = ((DefaultListModel) this.jListPhotos.getModel());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();
        jPanel6 = new javax.swing.JPanel();
        inputJPanel = new javax.swing.JPanel();
        selectPathLabel = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListMissions = new javax.swing.JList();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListPhotos = new javax.swing.JList();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        outputJPanel = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        imageLabel = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        speedLabel = new javax.swing.JLabel();
        fuelLabel = new javax.swing.JLabel();
        latLabel = new javax.swing.JLabel();
        longitLabel = new javax.swing.JLabel();
        distLabel = new javax.swing.JLabel();

        jFileChooser1.setCurrentDirectory(new File(System.getProperty("user.dir")));
        jFileChooser1.setDialogTitle("Mission files path");
        jFileChooser1.setFileSelectionMode(javax.swing.JFileChooser.DIRECTORIES_ONLY);
        jFileChooser1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFileChooser1ActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Drone System - Mission viewer");

        inputJPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, " Input ", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(51, 51, 255))); // NOI18N

        selectPathLabel.setForeground(new java.awt.Color(51, 51, 255));
        selectPathLabel.setText("<-- Select path to mission root");

        jListMissions.setModel(new DefaultListModel<String>()
        );
        jListMissions.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jListMissions.setEnabled(false);
        jListMissions.setOpaque(false);
        jListMissions.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListMissionsValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(jListMissions);

        jListPhotos.setModel(new DefaultListModel<String>());
        jListPhotos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jListPhotos.setEnabled(false);
        jListPhotos.setOpaque(false);
        jListPhotos.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListPhotosValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jListPhotos);

        jLabel14.setText("Misson:");

        jLabel15.setText("Photo:");

        jLabel16.setText("->");

        jButton1.setText("Select...");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout inputJPanelLayout = new javax.swing.GroupLayout(inputJPanel);
        inputJPanel.setLayout(inputJPanelLayout);
        inputJPanelLayout.setHorizontalGroup(
            inputJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputJPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(inputJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(inputJPanelLayout.createSequentialGroup()
                        .addGroup(inputJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addGroup(inputJPanelLayout.createSequentialGroup()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel16)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(inputJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(inputJPanelLayout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(selectPathLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        inputJPanelLayout.setVerticalGroup(
            inputJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputJPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(inputJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(selectPathLabel)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(inputJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel14))
                .addGroup(inputJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(inputJPanelLayout.createSequentialGroup()
                        .addGap(130, 130, 130)
                        .addComponent(jLabel16)
                        .addGap(0, 230, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inputJPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inputJPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2)))
                .addContainerGap())
        );

        outputJPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, " Output ", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(51, 51, 255))); // NOI18N

        jScrollPane3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        imageLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jScrollPane3.setViewportView(imageLabel);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jPanel5.setLayout(new java.awt.GridLayout(5, 1));

        jLabel4.setText("Speed:");
        jPanel5.add(jLabel4);

        jLabel6.setText("Fuel:");
        jPanel5.add(jLabel6);

        jLabel8.setText("Latitude:");
        jPanel5.add(jLabel8);

        jLabel10.setText("Longitude:");
        jPanel5.add(jLabel10);

        jLabel12.setText("Distance:");
        jPanel5.add(jLabel12);

        jPanel2.add(jPanel5);

        jPanel4.setLayout(new java.awt.GridLayout(5, 1));

        speedLabel.setText("-");
        jPanel4.add(speedLabel);

        fuelLabel.setText("-");
        jPanel4.add(fuelLabel);

        latLabel.setText("-");
        jPanel4.add(latLabel);

        longitLabel.setText("-");
        jPanel4.add(longitLabel);

        distLabel.setText("-");
        jPanel4.add(distLabel);

        jPanel2.add(jPanel4);

        javax.swing.GroupLayout outputJPanelLayout = new javax.swing.GroupLayout(outputJPanel);
        outputJPanel.setLayout(outputJPanelLayout);
        outputJPanelLayout.setHorizontalGroup(
            outputJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(outputJPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(outputJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 385, Short.MAX_VALUE))
                .addContainerGap())
        );
        outputJPanelLayout.setVerticalGroup(
            outputJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, outputJPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(inputJPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(outputJPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(inputJPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(outputJPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        setSize(new java.awt.Dimension(770, 522));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jFileChooser1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFileChooser1ActionPerformed

    }//GEN-LAST:event_jFileChooser1ActionPerformed

    private void jListMissionsValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListMissionsValueChanged
        if (jListMissions.getSelectedValue() != null) {
            newMissionSelected();
        }
    }//GEN-LAST:event_jListMissionsValueChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        fileChooserResult = jFileChooser1.showOpenDialog(this);
        if (fileChooserResult == javax.swing.JFileChooser.APPROVE_OPTION) {
            String tempPathSelected = jFileChooser1.getSelectedFile().getPath();
            if (isMissionFolder(tempPathSelected)) {
                missionsFolder = tempPathSelected;
                goodMissionFolderSelected();
            } else {
                badMissionFolderSelected();
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jListPhotosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListPhotosValueChanged
        if (jListPhotos.getSelectedValue() != null) {
            newPhotoSelected();
        }
    }//GEN-LAST:event_jListPhotosValueChanged

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DroneSystemViewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DroneSystemViewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DroneSystemViewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DroneSystemViewer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new DroneSystemViewer().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel distLabel;
    private javax.swing.JLabel fuelLabel;
    private javax.swing.JLabel imageLabel;
    private javax.swing.JPanel inputJPanel;
    private javax.swing.JButton jButton1;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JList jListMissions;
    private javax.swing.JList jListPhotos;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel latLabel;
    private javax.swing.JLabel longitLabel;
    private javax.swing.JPanel outputJPanel;
    private javax.swing.JLabel selectPathLabel;
    private javax.swing.JLabel speedLabel;
    // End of variables declaration//GEN-END:variables

    private DefaultListModel jListMissionsModel;
    private DefaultListModel jListPhotosModel;
    private int fileChooserResult;
    private String missionsFolder = System.getProperty("user.dir");
    private ArrayList<File> allMissions;
    private ArrayList<Info> photosInfo;
    private BufferedReader br;

    private boolean isMissionFolder(String pathSelected) {
        allMissions = new ArrayList<>();
        File folder1 = new File(pathSelected);
        if (folder1.isDirectory()) {
            File[] files = folder1.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory() && file.getName().startsWith("mission_")) {
                        allMissions.add(file);
                    }
                }
            }
        }
        if (allMissions.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    private void badMissionFolderSelected() {
        this.selectPathLabel.setText("<-- Invalid path.");
        this.selectPathLabel.setForeground(Color.RED);
        this.resetImageAndData();
        this.resetPhotoList();
        this.resetMissionList();
    }

    private void goodMissionFolderSelected() {
        this.selectPathLabel.setText(" Valid path.");
        this.selectPathLabel.setForeground(Color.BLACK);
        this.resetImageAndData();
        this.resetPhotoList();
        this.resetMissionList();

        this.jListMissions.setEnabled(true);
        this.jListMissions.setOpaque(true);
        for (File file : allMissions) {
            jListMissionsModel.addElement(file.getName());
        }
    }

    private void newMissionSelected() {
        this.resetImageAndData();
        this.resetPhotoList();
        this.jListPhotos.setEnabled(true);
        this.jListPhotos.setOpaque(true);
        loadMissionLog();
        //get photo list from log instead of file list
//        File[] files = allMissions.get(jListMissions.getSelectedIndex()).listFiles();
//        if (files != null) {
//            for (File file : files) {
//                if (!file.isDirectory() && file.getName().contains("_output")) {
//                    list1Model.addElement(file.getName());
//                    Info info = new Info();
//                    info.setPhoto(file);
//                    photosInfo.add(info);
//                }
//            }
//        }
    }

    private void newPhotoSelected() {
        Info selectedInfo = photosInfo.get(jListPhotos.getSelectedIndex());

        // populate image label
        File selectedPhoto = selectedInfo.getPhoto();
        ImageIcon icon = new ImageIcon(selectedPhoto.getPath());
        this.imageLabel.setIcon(icon);

        // populate info fields
        this.speedLabel.setText(selectedInfo.getSpeed());
        this.fuelLabel.setText(selectedInfo.getFuel());
        this.latLabel.setText(selectedInfo.getLat());
        this.longitLabel.setText(selectedInfo.getLongit());
        this.distLabel.setText(selectedInfo.getDist());
    }

    private void resetMissionList() {
        this.jListMissions.setEnabled(false);
        this.jListMissions.setOpaque(false);
        jListMissionsModel.clear();
    }

    private void resetPhotoList() {
        this.jListPhotos.setEnabled(false);
        this.jListPhotos.setOpaque(false);
        jListPhotosModel.clear();
    }

    private void resetImageAndData() {
        this.imageLabel.setIcon(null);
        this.speedLabel.setText("-");
        this.fuelLabel.setText("-");
        this.latLabel.setText("-");
        this.longitLabel.setText("-");
        this.distLabel.setText("-");
        //reset any additional fields
    }

    private void loadMissionLog() {
        String missionFolderPath = allMissions.get(jListMissions.getSelectedIndex()).getPath();
        photosInfo = new ArrayList<>();
        Info info;
        String line, photoName;
        String[] lineParts, sensors;
        try {
            br = new BufferedReader(new FileReader(new File( missionFolderPath + "/log.txt")));
            line = "";
            while ((line = br.readLine()) != null) {
                lineParts = line.split("=");
                photoName = lineParts[0];
                sensors = lineParts[1].substring(6,lineParts[1].length()-1).split(",");
                info = new Info();
                info.setSpeed(sensors[0]);
                info.setFuel(sensors[1]);
                info.setLat(sensors[2]);
                info.setLongit(sensors[3]);
                info.setAlt(sensors[4]);
                info.setDist(sensors[5]);
                info.setPhoto(new File(missionFolderPath+"/"+photoName));
                photosInfo.add(info);
                jListPhotosModel.addElement(photoName);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DroneSystemViewer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DroneSystemViewer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
