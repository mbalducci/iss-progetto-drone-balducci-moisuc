package it.unibo.iss.gruppo9.DroneServer;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;

import javax.imageio.ImageIO;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;

public class DroneCamera {

	private String imgFormat = "";
	private File fileOutput = null;
	 public enum MAP_TYPES {roadmap, satellite, terrain, hybrid}

	public DroneCamera(String destRootPath, String imgName, String imgFormat) {
		this.imgFormat = imgFormat;
		fileOutput = new File(destRootPath + imgName + "." + imgFormat);
	}
	
	public void takePhoto(String infos) {
		/*
		 * basic info on:
		 * https://developers.google.com/maps/documentation/staticmaps
		 */
		
		//unwrap latitude and longitude
		DecimalFormat df = new DecimalFormat("#.000000");
		Struct msgt = (Struct)Term.createTerm( infos );
		String latitude = df.format(Float.parseFloat(""+msgt.getArg(2)));
		String longitude = df.format(Float.parseFloat(""+msgt.getArg(3)));

		System.out.println("latitude="+latitude+" longitude="+longitude);
		
		String url, baseUrl, center, zoom, scale, size;
		MAP_TYPES type;
		center = latitude + "," + longitude;
		zoom = "17";
		size = "400x300";
		type = MAP_TYPES.satellite;
		scale = "2";

		baseUrl = "https://maps.googleapis.com/maps/api/staticmap?";
		url = baseUrl + "center=" + center + "&zoom=" + zoom + "&size=" + size
				+ "&maptype=" + type + "&format=" + imgFormat + "&scale="
				+ scale;
		URL imageURL = null;
		RenderedImage img = null;
		try {
			imageURL = new URL(url);
			img = ImageIO.read(imageURL);
			ImageIO.write(img, imgFormat, fileOutput);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("image acquired");
		}
	}
}
