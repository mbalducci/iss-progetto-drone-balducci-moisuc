package it.unibo.contact.droneSystem;

import java.io.File;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;

public class Server extends ServerSupport {

	int i = 0;
	double fuel, lat, lon, alt, dist;
	int speed;

	String lastPhotoPath;
	File log;

	public Server(String name) throws Exception {
		super(name);
	}

	// Estrae il nome del comando ricevuto dal messaggio
	@Override
	protected String getCmdFromMsgS(String msg) throws Exception {
		Struct msgt = (Struct) Term.createTerm(msg);
		return msgt.getName().trim();
	}

	// Controlla che la velocit� impostata con SetSpeed sia compresa tra
	// MIN_SPEED e MAX_SPEED
	@Override
	protected boolean checkSpeed(String speedString) throws Exception {
		int speed = 0;
		speedString = speedString.substring(speedString.indexOf('(') + 1,
				speedString.indexOf(')'));
		try {
			speed = Integer.parseInt(speedString);
		} catch (NumberFormatException e) {
			return false;
		}
		if (speed >= 60 && speed <= 120) {
			return true;
		}
		return false;
	}

	// Verifica che il livello di carburante non sia sceso sotto il livello
	// minimo
	@Override
	protected boolean isLowFuel() throws Exception {
		if (fuel < 0.5) {
			return true;
		}
		return false;
	}

	// Salva una copia locale dei dati appena ricevuti dal drone
	@Override
	protected void updateDroneData(String data) throws Exception {
		Struct msgt = (Struct) Term.createTerm(data);
		speed = Integer.parseInt("" + msgt.getArg(0));
		fuel = Double.parseDouble("" + msgt.getArg(1));
		lat = Double.parseDouble("" + msgt.getArg(2));
		lon = Double.parseDouble("" + msgt.getArg(3));
		alt = Double.parseDouble("" + msgt.getArg(4));
		dist = Double.parseDouble("" + msgt.getArg(5));

	}

	// Verifica che non si sforino i bound della velocit� a seguito in un
	// incSpeed o decSpeed
	@Override
	protected boolean checkModifySpeed(String command) throws Exception {
		int deltaSpeed = Integer.parseInt(command.substring(
				command.indexOf('(') + 1, command.indexOf(')')));
		command = getCmdFromMsgS(command);
		if (command.equals("inc")
				&& speed + deltaSpeed <= 120) {
			speed += deltaSpeed;
			return true;
		}
		if (command.equals("dec")
				&& speed - deltaSpeed >= 60) {
			speed -= deltaSpeed;
			return true;
		}
		return false;
	}

	@Override
	protected void initServerParams() throws Exception {
		setSpeed = "setSpeed";
		inc = "inc";
		dec = "dec";
		start = "start";
		stop = "stop";

	}

	@Override
	protected void savePhotoAndData(String sensorData) {
		// I/O interaction
	}

	@Override
	protected String getInitialSpeedFromUser() throws Exception {
		return "setSpeed(61)";
	}

	@Override
	protected String getStartFromUser() throws Exception {
		return "start";
	}

	@Override
	protected String getCommandFromUser() throws Exception {
		Thread.sleep(700);
		i++;
		if (i % 2 == 0)
			return "inc(1)";
		if (i > 5)
			return "stop";
		return "dec(1)";
	}

}
