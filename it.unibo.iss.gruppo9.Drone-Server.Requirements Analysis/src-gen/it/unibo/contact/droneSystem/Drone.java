package it.unibo.contact.droneSystem;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;

public class Drone extends DroneSupport {

	long lastUpdate;
	long currentTime;
	double timeElapsed;

	public Drone(String name) throws Exception {
		super(name);
	}
	
	@Override
	protected void setStartParams() throws Exception {
		lastUpdate = System.currentTimeMillis();
		fuel = 30;
		lat = 41.89;
		lon = 12.492;
		alt = 100;
		actualSpeed = targetSpeed;
		DTF = 5000;
	}

	@Override
	protected int getNFromMsg(String msg) throws Exception {
		Struct msgt = (Struct) Term.createTerm(msg);
		return Integer.parseInt("" + msgt.getArg(0));
	}

	@Override
	protected String getCmdFromMsg(String msg) throws Exception {
		Struct msgt = (Struct) Term.createTerm(msg);
		return msgt.getName();
	}

	@Override
	protected int modifySpeed(String cmd, int actualSpeed, int deltaSpeed)
			throws Exception {
		if (cmd.equals("inc")) {
			actualSpeed += deltaSpeed;
			return actualSpeed;
		}
		if (cmd.equals("dec")) {
			actualSpeed -= deltaSpeed;
			return actualSpeed;
		}
		throw new Exception(cmd);
	}


	@Override
	protected void calcSensors() throws Exception {
		currentTime = System.currentTimeMillis();
		timeElapsed = ((double) (currentTime - lastUpdate)) / 1000 / 3600;
		fuel = fuel - (actualSpeed * 30 * timeElapsed);
		distance = distance + actualSpeed * timeElapsed;
		lon = lon + actualSpeed * timeElapsed / 111.319;
		lastUpdate = currentTime;
	}

	@Override
	protected void delay(int dt) throws Exception {
		Thread.sleep(dt);
	}

	@Override
	public String takePhoto(String infos) {
		return "photo for "+infos;
	}
}
